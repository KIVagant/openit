==============================================
Краткие сведения из теории операционных систем
==============================================

.. include:: ../../Include/os_termin.rst
.. include:: ../../Include/os_struct_sys.rst
.. include:: ../../Include/os_def.rst
.. include:: ../../Include/os_func.rst
.. include:: ../../Include/os_arch.rst
.. include:: ../../Include/os_type.rst
