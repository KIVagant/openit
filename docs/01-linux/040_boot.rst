===========
Загрузка ОС
===========

В настоящее время существуют два способа загрузки операционных систем для Intel-совместимых компьютеров:

- Система загрузки BIOS.
- Система загрузки UEFI.

Система загрузки BIOS
=====================
.. include:: ../../Include/boot_bios.rst

Система загрузки UEFI
=====================
.. include:: ../../Include/boot_uefi.rst

Установка и первичная настройка загрузчика UEFI
===============================================
.. include:: ../../Include/uefi.rst

Загрузчик GRUB и GRUB2
======================
.. include:: ../../Include/boot_grubx.rst

GRUB
----
.. include:: ../../Include/grub.rst

Особенности GRUB2
-----------------
.. include:: ../../Include/grub2.rst

Уровни загрузки (режимы работы)
===============================
.. include:: ../../Include/boot_init.rst

System V
--------
.. include:: ../../Include/boot_systemv.rst
.. include:: ../../Include/runlevel.rst

Systemd
-------
.. include:: ../../Include/boot_systemd.rst
.. include:: ../../Include/systemd_runlevel.rst

.. include:: ../../Include/boot.rst
