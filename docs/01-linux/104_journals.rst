========================
Протоколы работы системы
========================

RHEL 6: syslog
==============
.. include:: ../../Include/syslog.rst

RHEL 7: journalctl
==================
.. include:: ../../Include/systemd_journal.rst

Анализ протоколов
=================
.. include:: ../../Include/logwatch.rst
