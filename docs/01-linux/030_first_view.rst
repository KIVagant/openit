=====================
Знакомство с системой
=====================

Об архитектуре
==============
.. include:: ../../Include/first_view_arch.rst

Разделы жесткого диска и логические диски
=========================================
.. include:: ../../Include/first_view_part.rst

Файловая система
================
.. include:: ../../Include/first_view_fs.rst

Виртуальная память
==================
.. include:: ../../Include/first_view_vm.rst

«Демоны»
========
.. include:: ../../Include/first_view_daemon.rst

Графический интерфейс
=====================
.. include:: ../../Include/first_view_gui.rst

Консоль. Терминал. Виртуальный терминал
=======================================
.. include:: ../../Include/first_view_tui.rst

Пользователи и группы
=====================
.. include:: ../../Include/first_view_uid_gid.rst

Права доступа и маска доступа
=============================
.. include:: ../../Include/first_view_mask.rst
