=====================
Пользователи и группы
=====================

.. include:: ../../Include/user_uid_gid.rst

Переменные окружения
====================
.. include:: ../../Include/user_env.rst

Домашний каталог
================
.. include:: ../../Include/user_home.rst

Пользовательская маска
======================
.. include:: ../../Include/user_umask.rst

.. .. include:: ../../Include/umask.rst

Файлы настроек пользователя и скрипты инициализации
===================================================
.. include:: ../../Include/user_script.rst

Выполнение команд от имени другого пользователя
===============================================
.. include:: ../../Include/user_othe_user.rst

.. Команда su
   ----------
   .. include:: ../../Include/su_intro.rst 

   Команда sudo
   ------------
   .. include:: ../../Include/sudo_intro.rst 

   Сходство и отличия sudo и su
   ----------------------------
   .. include:: ../../Include/sudo_vs_su.rst 

Команды управления учетными записями
====================================
.. include:: ../../Include/user_ctrl.rst
