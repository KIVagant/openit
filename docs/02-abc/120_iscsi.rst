===============================
Сетевое хранилище на базе iSCSI
===============================

.. include:: ../../Include/def_iscsi.rst

Сервер (target)
===============
В дистрибутивах GNU/Linux семейства Red Hat существует два пакета для установки сервера ``iSCSI``:

- targetcli,
- scsi-target-utils. 

Первый пакет доступен в базовом репозитории CentOS 7, 
втрой -  в базовом репозиториив CentOS 6 и в репозитории ``EPEL`` для CentOS 7.

scsi-target-utils
-----------------
.. include:: ../../Include/iscsi_target_utils.rst

targetcli
---------
.. include:: ../../Include/iscsi_targetcli.rst

Клиент (initiator)
==================

.. include:: ../../Include/iscsi_initiator.rst
