==========================
DNS, DHCP, BOOTP/PXE, TFTP
==========================

.. include:: ../../Include/dns_dhcp_pxe_tftp.rst

.. include:: ../../Include/dnsmasq.rst
