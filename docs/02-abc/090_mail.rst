========================
Сервер электронной почты
========================

Основные термины
================
.. include:: ../../Include/mail_termins.rst

Маршрутизация почты в Internet
==============================
.. include:: ../../Include/mail_internet.rst

Формат заголовка почтового сообщения
====================================
.. include:: ../../Include/mail_header.rst

Способы хранения почты
======================
.. include:: ../../Include/mail_box.rst

Виртуальный почтовый домен
==========================
.. include:: ../../Include/mail_vmail.rst

Сервер SMTP (POSTFIX)
=====================
.. include:: ../../Include/mail_postfix.rst

Проверка SMTP сервера
=====================
.. include:: ../../Include/mail_postfix_check.rst

Сервер POP и IMAP (Dovecot)
===========================
.. include:: ../../Include/mail_dovecot.rst

Настройка общего доступа к папкам (ACL)
---------------------------------------
.. include:: ../../Include/mail_dovecot_acl.rst

Настройка скриптов фильтрации почты (Sieve)
-------------------------------------------
.. include:: ../../Include/mail_dovecot_sieve.rst

Проверка IMAP и POP сервера
===========================
.. include:: ../../Include/mail_dovecot_check.rst

.. Пример файла настроек для Dovecot 1.0

   ::

     protocols = imap imaps pop3 pop3s
     listen= [*]
     log_timestamp = “%d.%m.%Y %H:%M:%S ”
     syslog_facility = mail
     mail_privileged_group = mail
     first_valid_uid = 499
     last_valid_uid = 65535
     first_valid_gid = 499
     last_valid_gid = 65535
     mail_location = maildir:/var/vmail/%n/Maildir
     protocol imap {
       mail_plugins = acl
     }
     protocol lda {
       # для Sieve
       #mail_plugins = acl
       # для служебных сообщений
       postmaster_address = postmaster@mail.example
       #
       hostname = mail.mail.example
       # для авторизации postfix
       auth_socket_path = /var/run/dovecot/auth-master
     }
     lugin {

       # Без общих настроек ACLs:
       acl = vfile

       # Общие настройки ACLs в каталоге /etc/dovecot/acls
       #acl = vfile:/etc/dovecot/acls
     }

     disable_plaintext_auth = no
     ssl_disable = no
     ssl_cert_file = /etc/pki/dovecot/certs/dovecot.pem1
     ssl_key_file =  /etc/pki/dovecot/private/dovecot.pem1
     ssl_verify_client_cert = no
     ssl_parameters_regenerate = 168
     ssl_cipher_list = ALL:!LOW
     verbose_ssl = no
     auth default {
       mechanisms = plain login
       user = vmail
       passdb sql {
         args = /etc/dovecot-sql.conf
       }
       userdb sql {
         args = /etc/dovecot-sql.conf
       }
       userdb passwd {
         args = mail=mbox:/var/mail
       }
       passdb pam {
         args = dovecot
       }
       socket listen {
         master {
            path = /var/run/dovecot/auth-master
            mode = 0666
            user = vmail
            group = vmail
         }
         client {
            path = /var/spool/postfix/dovecot-auth
            mode = 0666
            user = postfix
            group = postfix
         }
       }
     }

