.. _os-label:

##############################
Операционная система GNU/Linux
##############################

..  toctree::
    :maxdepth: 2
    :glob:

    01-linux/*
