#######################################
Администрирование вычислительных систем
#######################################

.. include:: ../Include/000.rst

###############
:ref:`os-label`
###############

.. include:: ../Include/000_01-os.rst

.. toctree::
   :hidden:
   :maxdepth: 1

   index-01-linux.rst

----

################
:ref:`abc-label`
################

.. include:: ../Include/000_02-abc.rst

.. toctree::
   :hidden:
   :maxdepth: 1

   index-02-abc.rst

----

##################
:ref:`cloud-label`
##################

.. include:: ../Include/000_03-cloud.rst

.. toctree::
   :hidden:
   :maxdepth: 1

   index-03-cloud.rst

.. кракозябры
.. * :ref:`genindex`
