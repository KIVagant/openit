import sys, os
extensions = ['sphinx.ext.todo']
templates_path = ['.templates']
source_suffix = '.rst'
source_encoding = 'utf-8-sig'
master_doc = 'index'
project = u'ABC Unix'
copyright = u'Vladimir Balberin'
author = u'Балберин Владимир'
version = u'1.0'
release = u'1.0.01'
language = 'ru'
pygments_style = 'sphinx'
html_title = u'ABC Unix'
html_show_sourcelink = False
html_show_sphinx = False
htmlhelp_basename = 'abc-unixdoc'

import sphinx_rtd_theme
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme_options = {
   "typekit_id"          : "hiw1hhg",
   "analytics_id"        : "",
   "sticky_navigation"   : "False",
   "logo_only"           : "",
   "collapse_navigation" : "False",
   "display_version"     : "True"
}
html_favicon = '../images/logo.ico'
html_logo = '../images/logo_200.jpg'

latex_elements = {
'papersize': 'a4paper',
'preamble': '\\usepackage[utf8]{inputenc}',
'preamble':'\\setcounter{tocdepth}{1}',
'babel': '\\usepackage[russian]{babel}',
'cmappkg': '\\usepackage{cmap}',
'fontenc': '\usepackage[T1,T2A]{fontenc}',
'utf8extra':'\\DeclareUnicodeCharacter{00A0}{\\nobreakspace}',
}

#latex_documents = [
#  ('index', 'abc-unix.tex', u'abc-unix Documentation',
#   u'Балберин Владимир', 'manual'),
#]

#man_pages = [
#    ('index', 'abc-unix', u'abc-unix Documentation',
#     [u'Балберин Владимир'], 1)
#]

#texinfo_documents = [
#  ('index', 'abc-unix', u'abc-unix Documentation',
#   u'Балберин Владимир', 'abc-unix', 'One line description of project.',
#   'Miscellaneous'),
#]
