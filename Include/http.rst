.. ===========
   HTTP сервер
   ===========

Установка и первичная настройка
===============================

В Red Hat совместимых дистрибутивах пакет с сервером Apache называется httpd. 
Проверить установлен или нет HTTP сервер можно командой::

  rpm -q httpd

а установить сервер командой::

  yum install httpd

Основным конфигурационным файлом сервера является::

  /etc/httpd/conf/httpd.conf

В конфигурационный файл можно внести некоторые полезные изменения. 
Для этого необходимо найти, раскомментировать и исправить параметры::

  ServerAdmin master@example.com # почтовый адрес администратора
  ServerName example.com:80 # имя сервера

В параметре LanguagePriority рекомендуется поставить ru на первое место::

  LanguagePriority ru en ca cs da de el eo es et fr he hr it ja ko ltz nl nn no pl...

По умолчанию все файлы сайта размещаются в каталоге::

  /var/www

Для хранения страницы по умолчанию используется каталог::

  /var/www/html

а для хранения cgi-скриптов каталог::

  /var/www/cgi-bin

Дополнительные настройки
========================

Описание каталога для http-сервера
----------------------------------

По умолчанию, корневым каталогом для http-сервера является каталог ``/var/www``. Его параметры заданы в основном конфигурационном файле ``/etc/httpd/conf/httpd.conf``. Можно описать другие каталоги, доступные http-серверу с помощью директивы::

  <Directory <каталог_сервера>>

    Параметры доступа

  </Directory>

Атрибуты доступа к каталогу
~~~~~~~~~~~~~~~~~~~~~~~~~~~
Для каталога должны быть установлены соответствующие права. Сервис ``httpd`` выполняется от имени пользователя ``apache`` и группы ``apache``, поэтому каталог должен быть доступен хотя бы на чтение содержимого для упомянутого пользователя и группы.

Контекст ``selinux``
~~~~~~~~~~~~~~~~~~~~
Для каталога должны быть установлен соответствующий контекст ``selinux``:

- для доступа на чтение:

::

  semanage fcontext -a -t httpd_sys_content_t '<каталог>(/.*)?'
  restorecon -R -v <каталог>

- для доступа на запись:

::

  semanage fcontext -a -t httpd_sys_rw_content_t '<каталог>(/.*)?'
  restorecon -R -v <каталог>

Псевдоним для каталога
----------------------

Псевдоним позволяет обращаться к заданному каталогу http-сервера по имени (псевдониму)::

  Alias /<псевдоним> /<каталог сервера>/

Пример::

  Alias /webmail /usr/share/roundcubemail/

Перенаправление каталогов
-------------------------

Позволяет перенаправить запросы к каталогу данного сервера на корневой каталоги другого сервера::

  Redirect permanent /<виртуальный каталог> <URL без каталога>

Пример::

  Redirect permanent /abc-unix https://balberin.ru

Примеры ограничений доступа
---------------------------

В этом примере, все запросы запрещены.

- для ``apache 2.2``

::

  Order deny,allow
  Deny from all

- для ``apache 2.4``

::

  Require all denied

В этом примере все запросы разрешены.

- для ``apache 2.2``

::

  Order allow,deny
  Allow from all

- для ``apache 2.4``

::

  Require all granted

Следующий  пример разрешает доступ с домена example.doc, все остальные хосты запрещены:

- для ``apache 2.2``

::

  Order Deny,Allow
  Deny from all
  Allow from .example.doc

- для ``apache 2.4``

::

  Require host .example.doc

Авторизация
-----------

Apache поддерживает два вида аутентификации: 

- Basic (Базовая аутентификация),
- Digest (Дайджест аутентификация).

Базовая аутентификация
~~~~~~~~~~~~~~~~~~~~~~

Для настройки доступа к каталогам по пользователям и паролям нужно:

- создать файл с пользователями и паролями
  
  ::

    htpasswd /etc/httpd/passwd <ПОЛЬЗОВАТЕЛЬ>

  .. note::
     Если файл ``/etc/httpd/passwd`` отсутствует, то нужно использовать опцию ``-c``

- при необходимости, добавить пользователей с паролями

  ::

    htpasswd /etc/httpd/passwd <ПОЛЬЗОВАТЕЛЬ>
    New password:
    Re-type new password:
    Adding password for user <ПОЛЬЗОВАТЕЛЬ>

- создать файл для хранения групп ``/etc/httpd/group`` (если нужна авторизация по группам) вида::

    <ГРУППА>: <ПОЛЬЗОВАТЕЛЬ> <ПОЛЬЗОВАТЕЛЬ А> ... <ПОЛЬЗОВАТЕЛЬ Б>
	...
    <ГРУППА N>: <ПОЛЬЗОВАТЕЛЬ В> ... <ПОЛЬЗОВАТЕЛЬ Я>
 
- добавить в описание каталога опции авторизации::

    <Directory /var/www/html/<каталог>/>

      ...

      AuthType Basic
      AuthName Restricted

      # Авторизация по пользователям
      AuthUserFile /etc/httpd/passwd
      # разрешить всем авторизовавшимся пользователям 
      #Require valid-user
      # разрешить заданному пользователю после его авторизации 
      Require user <ПОЛЬЗОВАТЕЛЬ>

      # Авторизация по группам
	  #AuthGroupFile /etc/httpd/group
	  #Require group <ГРУППА>
      
      ...

    </Directory>

- или создать в защищаемом каталоге файл ``.htaccess`` примерно такого содержания::

    AuthName "Требуется авторизация!"
    AuthType Basic
    # Авторизация по пользователям
    AuthUserFile /etc/httpd/passwd
    # разрешить всем авторизовавшимся пользователям 
    #Require valid-user
    # разрешить заданному пользователю после его авторизации 
    Require user <ПОЛЬЗОВАТЕЛЬ>

    # Авторизация по группам
    #AuthGroupFile /etc/httpd/group
    #Require group <ГРУППА>
 
.. note::
   Если защищаемый каталог является подкаталогом корня сайта, необходимо добавить параметр ``AllowOvveride`` в основной конфигурационный файл ``/etc/httpd/conf/httpd.conf`` после директивы ``<Directory "/var/www/html">`` примерно так::
   
     <Directory "/var/www/html">
   
     ...
   
     Options Indexes FollowSymLinks

     #
     # AllowOverride controls what directives may be placed in .htaccess files.
     # It can be "All", "None", or any combination of the keywords:
     #
     # Options FileInfo AuthConfig Limit
     #
     AllowOverride Authconfig
     #
     # Controls who can get stuff from this server.
     #
   
     ...
   
     </Directory>

Дайджест аутентификация
~~~~~~~~~~~~~~~~~~~~~~~

Настройка этого типа аутентификации отличается только:

- Файл паролей создаётся при помощи утилиты htdigest::

    htdigest -c /etc/httpd/passwd <ИМЯ ОБЛАСТИ> <ПОЛЬЗОВАТЕЛЬ>

  Ключ ``-c`` указывается при необходимости создать новый файл, а аргумент <ИМЯ ОБЛАСТИ> будет указан в директиве ``AuthName``.

- Описание защищенного каталога содержит следующие директивы::

    AuthType Digest

    # Авторизация по пользователям
    AuthName <ИМЯ ОБЛАСТИ>
    AuthUserFile /etc/httpd/passwd
    # разрешить заданному пользователю после его авторизации 
    Require user <ПОЛЬЗОВАТЕЛЬ>
    # разрешить всем авторизовавшимся пользователям 
    #Require valid-user

    # Авторизация по группам
    #AuthGroupFile /etc/httpd/group
    #Require group <ГРУППА>

Создание дополнительного каталога сайта
---------------------------------------

Допустим, необходимо обеспечить доступ к некоторому каталогу на сайте по адресу ``http://example.com/<каталог>``.
Существуют два варианта решения этой задачи:

#. Найти корневой каталог сайта ``http://example.com`` и в этом каталоге создать подкаталог с именем ``<каталог>``.
#. Создать каталог в произвольном месте и присвоить ему псевдоним ``<каталог>``.

Первый способ описан выше, в процессе описания виртуального сайта. Для реализации второго способа необходимо:

#. Создать каталог на сервере;
#. Описать этот каталог как каталог доступный с помощью http протокола;
#. Присвоить этому каталогу http-название - псевдоним.

Допустим, необходимо обеспечить доступ к каталогу ``/var/www/README`` по адресу::

  http://<адрес или имя сервера>/readme

или по адресу::

  http://<адрес или имя сервера>/README

Создадим требуемый каталог из под пользователя ``root``::

  # mkdir /var/www/README

Установим для этого каталога контекст ``selinux`` (подробности смотри выше)::

  semanage fcontext -a -t httpd_sys_content_t '/var/www/README(/.*)?'
  restorecon -R -v /var/www/README

Дополнительные настройки не рекомендуется делать в основном настроечном файле ``/etc/httpd/conf/httpd.conf``, для подобных задач выделен специальный подкаталог::

  /etc/httpd/conf.d/

Любой файл в этом каталоге, имеющий расширение .conf вставляется в основной настроечный файл. 
Поэтому создадим отдельный файл настроек::

  /etc/httpd/conf.d/README.conf

со следующим содержанием::

  Alias /readme /var/www/README
  Alias /README /var/www/README

  <Directory /var/www/README>
    Options Indexes FollowSymLinks
    IndexOptions Charset=UTF-8 
    AllowOverride All
    AddDefaultCharset UTF-8
    IndexIgnore .htaccess .htaccess
  </Directory>

Настройка виртуальных сайтов (Virtual Hosts)
============================================
Допустим, на сервере должны располагаться несколько сайтов и необходимо, 
чтобы при переходе по имени сайта открывались соответствующие этому сайту страницы, 
а при переходе по IP-адресу или по любому другому имени, кроме избранных, 
открывался сайт-заглушка, например, со списком имеющихся сайтов или просто предупреждением. 
Все это можно организовать с помощью так называемых именованных виртуальных сайтов (в дальнейшем просто виртуальных сайтов).

Пусть, первый сайт должен быть доступен по http-адресам::

  http://example.com
  http://www.example.com

и располагаться в каталоге::

  /var/www/html/example.com

Второй сайт, по адресам::

  http://virtual.net
  http://www.virtual.net

и располагаться в каталоге::

  /var/www/html/virtual.net

Создадим каталоги для сайтов::

  mkdir /var/www/html/example.com ; mkdir /var/www/html/virtual.net

Настройки виртуальных сайтов не рекомендуется делать в основном настроечном файле, 
для подобных задач выделен специальный каталог::

  /etc/httpd/conf.d/

Любой файл в этом каталоге, имеющий расширение .conf вставляется в основной настроечный файл. 
Поэтому создадим отдельный файл настроек::

  /etc/httpd/conf.d/virtual.conf

со следующим содержанием::

  #
  # Основной сайт
  #
  NameVirtualHost *:80
  <VirtualHost *:80>
   ServerName default
  </VirtualHost>
  #
  # Сайт http://example.com
  #
  <VirtualHost *:80>
   ServerName example.com
   ServerAlias www.example.com
   DocumentRoot /var/www/html/example.com
   ErrorLog logs/example.com-error.log
  </VirtualHost>
  #
  # Сайт http://virtual.net
  #
  <VirtualHost *:80>
   ServerName virtual.net
   ServerAlias www.virtual.net
   DocumentRoot /var/www/html/virtual.net
   ErrorLog logs/virtual.net-error.log
  </VirtualHost>

Первая секция::

  <VirtualHost *:80>
    ServerName default
  </VirtualHost>

добавляется для того, чтобы все запросы с неправильными именами сайтов и обращения по IP адресам перенаправлялись в папку::

  /var/www/html

содержащую индексную страницу с соответствующим сообщением.
Для вступления всех изменений в силу необходимо перезапустить сервер командой::

  /etc/init.d/httpd restart

Настройка selinux
-----------------
Для каталогов виртуальных сайтов нужно установить контекст selinux::

  semanage fcontext -a -t httpd_sys_content_t '/var/www/html/example.com(/.*)?'
  semanage fcontext -a -t httpd_sys_content_t '/var/www/html/virtual.net(/.*)?'
  restorecon -R -v /var/www/html
