.. Команды управления учетными записями
   ====================================

Новые учетные записи создаются  командой::

  useradd <ОПЦИИ> <ИМЯ_ПОЛЬЗОВАТЕЛЯ>

Например::

  useradd student

Команда ``useradd`` без дополнительных опции, создает не только учетную запись пользователя (в данном случае ``student``), но и учетную запись основной группы с тем же именем (в данном случае ``student``).
Для изменения параметров существующей учетной используется команда::

  usermod <ОПЦИИ> <ИМЯ_ПОЛЬЗОВАТЕЛЯ>

Например, командой::

  usermod -G <ГРУППА1>,...<ГРУППАN> <ИМЯ_ПОЛЬЗОВАТЕЛЯ>
  
можно включить пользователя с указанным именем в дополнительные группы::

  id
  uid=1001(student) gid=1001(student) группы=1001(student)
  
  usermod -G users,wheel student
  id
  
  uid=1001(student) gid=1001(student) группы=100(users),10(wheel),1001(student)

Опция ``-G`` не добавляет, а заменяет дополнительные группы пользователя, поэтому в ней нужно указывать через запятую (без пробелов) все группы в которые должен входить пользователь.
Учетную запись нельзя использовать для входа в систему до тех пор пока ей хотя бы один раз не будет присвоен пароль командой::

  # passwd  <ИМЯ_ПОЛЬЗОВАТЕЛЯ>
  Changing password for user student.
  New UNIX password:
  Retype new UNIX password:
  passwd: all authentication tokens updated successfully.

Новый пароль вводится дважды, при вводе пароля символы не отображаются, курсор остается на месте. Пароль может состоять только из букв латинского алфавита, цифр и знаков препинания. Суперпользователь ``root`` может вводить и менять пароль любого пользователя, указав имя пользователя в качестве аргумента команды ``passwd``. Обычный пользователь может менять только свой пароль командой ``passwd`` без аргументов. При смене своего пароля обычный пользователь сначала должен ввести старый пароль, затем дважды новый пароль::

  $ passwd
  Changing password for user student.
  Changing password for student
  (current) UNIX password:
  New UNIX password:
  Retype new UNIX password:пароль[Enter]
  passwd: all authentication tokens updated successfully.

При смене своего пароля обычный пользователь должен ввести новый пароль удовлетворяющий заданным в настройках системы критериям сложности. По умолчанию требования к паролю следующие: 

1. длинна пароля должна быть не меньше 6 символов;
2. должны использоваться и большие и маленькие буквы латинского алфавита;
3. в пароле должна быть хотя бы одна цифра и хотя бы один из знаков препинания.

Только пользователь ``root`` может ввести пользователю пароль не удовлетворяющий критерию сложности. При этом система выводит предупреждение, но пароль все равно принимается::

  # passwd  <ИМЯ_ПОЛЬЗОВАТЕЛЯ>
  Changing password for user student.
  New UNIX password:пароль[Enter]
  BAD PASSWORD: it's WAY too short
  Retype new UNIX password:пароль[Enter]
  passwd: all authentication tokens updated successfully.

Использование опции ``-l`` в команде ``passwd`` приводит к блокированию (запрету) учетной записи пользователя без её удаления::

  passwd -l <ИМЯ_ПОЛЬЗОВАТЕЛЯ>

После выполнения этой команды вход в систему под пользователем ``<ИМЯ_ПОЛЬЗОВАТЕЛЯ>`` будет запрещен до тех пор пока учетная запись не будет разблокирована командой::

  passwd -u <ИМЯ_ПОЛЬЗОВАТЕЛЯ>

Для удаления учетной записи предназначена команда::

  userdel  <ИМЯ_ПОЛЬЗОВАТЕЛЯ>

.. note::
   Большинство команд выводят информацию на экран только если это необходимо для выполнения заложенных в них функции (например, команда id), или, если нарушен синтаксис их использования. Команды управления учетными записями useradd, usermod, userdel, при правильном использовании выполняются «молча» (не выводят ни каких сообщений).

Наряду с командами управления учетными записями пользователя система поддерживает аналогичные по назначению команды управления регистрационными записями групп. Создавать, изменять и удалять группы может только «суперпользователь». Для создания группы применяется команда::

  groupadd <ОПЦИИ> <ИМЯ_ГРУППЫ>

Например::

  groupadd education

Команда::

  groupmod <ОПЦИИ> <ИМЯ_ГРУППЫ>

изменяет параметры группы. 
Например, опция -g <gid> меняет идентификатор группы::

  groupmod -g 10000 education

Удалить группу можно командой::

  groupdel <ОПЦИИ> <ИМЯ_ГРУППЫ>

