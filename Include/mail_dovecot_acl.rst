.. ===============================================
   dovecot:Настройка общего доступа к папкам (ACL)
   ===============================================

Плагин acl позволяет пользователям предоставлять друг другу доступ к папкам в своих почтовых ящиках.
Эта возможность доступна только при использовании протокола IMAP.

Плагин подключается в файле ``/etc/dovecot/conf.d/10-mail.conf``::

  mail_plugins = acl

В файле ``/etc/dovecot/conf.d/20-imap.conf`` плагин добавляется для IMAP-сервере::

  protocol imap {
    mail_plugins = $mail_plugins imap_acl
  }

В файле ``/etc/dovecot/conf.d/10-mail.conf`` описываются два пространства имён:

- первое для хранения личной почты пользователя:

::

  namespace inbox {
    type = private
    separator = /
    prefix =
    inbox = yes
  }

второе для отображения каталогов других пользователей, к которым этот пользователь имеет доступ:

::

  namespace {
    type = shared
    separator = /
    prefix = Shared/%%u/
    location = maildir:/var/vmail/%d/mail/%%n/Maildir:INDEX=/var/vmail/%d/mail/%%n/Maildir/Shared/%%u
    subscriptions = yes
    list = children
  }

Параметр location задает каталог для хранения папок предоставленных в общее пользование.
Двойной символ ``%%`` означает, что макроподстановки (``%u`` и ``%n``) осуществляются относительно пользователя, который предоставил доступ.

.. ====================================== ====
   maildir:/var/vmail/%d/mail/%%n/Maildir место расположения чужого почтового ящика в формате Maildir,
   %%h                                    полный путь к Maildir-каталогу чужого ящика,
   INDEX=%h/shared/%%u                    каталог, в который монтируются каталоги чужой почты, к которым её владелец дал доступ,
   %h                                     путь к Maildir-каталогу ящика,
   %%u                                    имя другого пользователя в виде box@<ДОМЕН_ПОЧТЫ>
   ====================================== ====

   Например, если текущий пользователь A принадлежит домену D и ему пользователь B предоставил доступ, то ... ::

     location = maildir:/var/vmail/D/mail/B/Maildir:INDEX=/var/vmail/D/mail/B/Maildir/Shared/B@D

В файле ``/etc/dovecot/conf.d/90-acl.conf`` прописываются настройки плагина::

  plugin {
    acl = vfile
    acl_shared_dict = file:/var/vmail/%d/shared-mailboxes.db
  }

Значение vfile предписывает создавать внутри почтового ящика файл dovecot-acl, в котором и будут прописываться права доступа к нему со стороны других пользователей.

Значение acl_shared_dict указывает путь к файлу базы данных, которая позволит пользователям узнавать, к каким каталогам в чужих почтовых ящиках у них имеется доступ. 
В данном случае для каждого почтового домена будет создана отдельная база данных, расположенная в каталоге домена, на одном уровне с ящиками.

Должна быть разрешена запись в каталог, указанный параметром ``acl_shared_dict`` от имени пользователя под которым запускается процесс imap (обычно vmail).

.. Проблема с правами на каталог ``file:/home/vmail/%Ld/shared-mailboxes.db`` если imap работает от имени нескольких пользователей::
   
     imap(b2v): Error: file dict commit: file_dotlock_open(/srv/var/vmail/mailboxes.db) failed: Permission denied
   
   Если все работает от имени виртуального пользователя vmail, то должна быть разрешена запись в этот каталог от имени этого пользователя.
   
Selinux
-------

Для каталога, указанного в ``acl_shared_dict`` нужно добавить контекст::

  semanage fcontext -a -t mail_home_rw_t '<acl_shared_dict>(/.*)?'

и обновить его::

  restorecon -R -v <acl_shared_dict>



.. Заодно опишем в файле /etc/dovecot/conf.d/15-mailboxes.conf назначение различных каталогов внутри пространства имён, в котором хранится    личная почта пользователя:

  ::

  namespace inbox {
    mailbox Drafts {
      special_use = \Drafts
    }
    mailbox Junk {
      special_use = \Junk
    }
    mailbox Trash {
      special_use = \Trash
    }
    mailbox Sent {
      special_use = \Sent
    }
  }

  Drafts - каталог черновиков,
  Junk - каталог для спама,
  Trash - каталог для удалённых писем,
  Sent - каталог для отправленных писем.

Современные почтовые программы по протоколу IMAP определяют назначение каждого из специальных каталогов. 
Это полезно, если каталог имеет нестандартное название или название на языке пользователя ящика, например, "Входящие" или "Спам".

Чтобы настройки плагина acl вступили в силу, нужно перезапустить Dovecot::

  /etc/init.d/dovecot restart
