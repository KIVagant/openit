.. =============================
   Первоначальная настройка сети
   =============================

Настройки сетевых устройств хранятся в нескольких файлах, которые можно редактировать любым редактором текстов. Даже если для настройки сети используется графический интерфейс параметры сети все равно сохраняются в этих файлах.

Файл параметров сети
====================
Общие для всех сетевых интерфейсов параметры хранятся в файле /etc/sysconfig/network. Пример файла::

  NETWORKING=yes
  HOSTNAME=lsrv.example.com
  GATEWAY=172.16.27.2

Параметры в этом файле имеют следующие значения:

| ``NETWORKING=`` "yes' (включить поддержку сети и сетевых сервисов) или "no" (отключает поддержку сети и сетевых сервисов);
| ``HOSTNAME=`` полное сетевое имя компьютера (FQDN);
| ``GATEWAY=`` IP адрес маршрутизатора (если есть).

Изменения вступают в силу после перезагрузки сетевого сервиса::

  /etc/rc.d/init.d/network restart

Файлы параметров сетевых интерфейсов
====================================
Обычно Еthernet интерфейсы именуются как eth0, eth1 и т.д., модемные интерфейсы - ppp0, ppp1 и т.д., локальный интерфейс - lo (127.0.0.1), туннель в сети IP версии 6 - sit0, FDDI - fddi0, беспроводная сеть - wlan0. Имена интерфейсов не имеют прямого соответствия с именами устройств в каталоге /dev. Каждое Ethernet устройство имеет один MAC адрес и один или несколько IP адресов.

Параметры сетевых интерфейсов находятся в каталоге::

  /etc/sysconfig/network-scripts

Каждому интерфейсу соответствует файл вида::

  ifcfg-<имя интерфейса>

Ниже приведен пример файла::

  /etc/sysconfig/network-scripts/ifcfg-eth0

с параметрами для первого сетевого интерфейса::

  DEVICE=eth0
  ONBOOT=yes
  TYPE=Ethernet
  IPADDR=10.10.10.254
  NETMASK=255.255.255.0
  NETWORK=10.10.10.0
  BROADCAST=10.10.10.255
  BOOTPROTO=none
  USERCTL=no
  IPV6INIT=no

Для изменения параметров сетевого интерфейса достаточно отредактировать соответствующий файл, а для создания нового сетевого интерфейса нужно создать новый файл с соответствующим именем и параметрами. 

Основные параметры сетевого интерфейса:

============== ====
``DEVICE=``    имя сетевого устройства;
``IPADDR=``    IP адрес;
``NETMASK=``   сетевая маска;
``NETWORK=``   адрес сети;
``BROADCAST=`` широковещательный адрес;
============== ====

Дополнительные параметры сетевого интерфейса:

============== ====
``ONBOOT=``    включать (yes) или нет (no) управление сетевым интерфейсом с помощью службы ``nework``;
``BOOTPROTO=`` протокол времени загрузки:none (не использовать), bootp, dhcp;
``USERCTL=``   если ``no``, то только пользователь ``root`` может управлять интерфейсом, если ``yes`` - любой пользователь.
============== ====

.. note::
   Значение параметр ``ONBOOT=yes`` **приводит** к автоматической активации сетевого интерфейса сервисом ``network`` при загрузке системы, значение ``ONBOOT=no`` **не приводит** к автоматической активации сетевого интерфейса сервисом ``network`` при загрузке системы.

Для того, чтобы изменения вступили в силу необходимо перезапустить сетевой сервис::

  /etc/rc.d/init.d/network restart

