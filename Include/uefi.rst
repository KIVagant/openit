.. ===========================================
   UEFI: Unified Extensible Firmware Interface
   ===========================================

   :tags: UEFI,EFI,GPT,MBR,efibootmgr,UEFI-bootloader,systemd-boot,rEFind,Clover,VirtualBox

   .. PELICAN_END_SUMMARY

   .. contents:: Содержание

   По материалам `статьи <http://habrahabr.ru/post/185492/>`_

   Система загрузки BIOS была разработана фирмой IBM для производимых ею персональных компьютеров IBM PC.
   В 1998 году фирма Intel приступила к разработке новой системы загрузки Extensible Firmware Interface (EFI) для процессоров Itanium. 
   В 2005 году корпорация Intel передала свою разработку специально созданному консорциуму UEFI Forum,
   главными участниками которого стали: Intel, AMD, Apple, IBM, Microsoft и ряд других.

   UEFI представляет собой специализированную встроенную операционную систему предназначенную для загрузки других операционных систем.
   В то время как BIOS является фактически неизменным по содержанию кодом прошивки специального BIOS-чипа,
   физическое расположение UEFI может быть разнообразным - от микросхемы памяти или раздела на жестком диске 
   до внешнего сетевого хранилища.

Основные термины
----------------

EFI
  Старая версия UEFI (до 2.0)

GPT
  GUID Partition Table - новый способ разметки HDD.
  В отличие от MBR, GPT поддерживает диски размером более 2ТБ и неограниченное количество разделов. 
  UEFI по умолчанию может читать файлы FAT32 на GPT-разделах. 
  Доступа к MBR-разделам UEFI может осуществлять с помощью расширения Compatibility Support Module (CSM).

boot services
  обеспечивают взаимодействие с графическими и текстовыми терминалами, шинами, блочными устройствами и т.д.

runtime services
  обеспечивают взаимодействие с операционной системой. 
  Например, variable service обеспечивает запись значение в NVRAM. 
  В ОС Linux variable service используется для хранения "дампов" памяти, которые можно проанализировать после перезагрузки компьютера.

Модули
  Можно использовать свои приложения. 
  Можно загружать свои драйверы. 
  Спецификация UEFI предусматривает использование драйверов UEFI в ОС.
  Если ОС не поддерживает устройство, а UEFI поддерживает, то ОС может использовать такое устройство с помощью драйвера UEFI.

CSM BIOS
  Модуль совместимости с загрузчиком BIOS

Shell
  модуль, обеспечивающий итерактивный интерфейс для управления  UEFI.
  Например, с его помощью можно (до загрузки ОС) добавить модули поддержки файловых систем, UEFI-драйверы сетевых карт и т.д.

Встроенный менеджер загрузки
  Можно добавлять пункты меню загрузки, и они появятся в загрузочном меню UEFI. 
  Boot-сектор больше не используется.
  Загрузка в UEFI происходит быстрее.

ESP-раздел
----------
UEFI видит все разделы с файловыми системами, для которых в конкретной реализации прошивки имеются драйверы. 
ESP-раздел отличается от остальных разделов тем, что на разделе ESP осуществляется поиск загрузчиков и автоматическое создание переменной BootXXXX, если загрузчик нашелся.
На ESP-разделе содержаться папки (Boot, Microsoft, RedHat, Fedora, Ubuntu и т.д) с загрузчиками соответствующих ОС, в которых находятся файлы с расширением ``.efi``.
Большинство Linux-дистрибутивов монтируют ESP-раздел в каталог ``/boot/efi``.

Краткая инструкция по установке
-------------------------------

- Создать таблицу разделов GPT, создать FAT32-раздел
  В теории, EFI-раздел  должен иметь особый тип "EFI System" (ef00).
  На практике, годится первый раздел на GPT-диске, отформатированный в
  FAT32 и имеющий достаточно места, чтобы разместить загрузчик и
  вспомогательные файлы.

- Скачать любой UEFI-загрузчик. Поместить файл загрузчика на созданный раздел по адресу ``/EFI/Boot/bootx64.efi``
  Загрузчик — это просто исполняемый файл определенного формата. Для
  пакета systemd файл загрузчика находится по адресу
  ``/usr/lib/systemd/boot/efi/systemd-bootx64.efi``.

- Создать файл настройки (зависит от конкретной реализации загрузчика)

После перезагрузки должно появиться меню загрузчика.

Пример создания EFI-раздела с GPT-разметкой
-------------------------------------------

.. warning::
   После выполнения действий перечисленных ниже **ВСЕ ДАННЫЕ С НОСИТЕЛЯ БУДУТ УДАЛЕНЫ**

Запустить утилиту разметки диска (далее предполагается, что раздел для размещения загрузчика UEFI это ``/dev/sdc``)::

  gdisk /dev/sdc

Возможны несколько вариантов сообщений при запуске ``gdisk``:

- На носителе найдена разметка GPT::

    GPT fdisk (gdisk) version X.X.X

    Partition table scan:
      MBR: protective
      BSD: not present
      APM: not present
      GPT: present

    Found valid GPT with protective MBR; using GPT.

    Command (? for help): 

  Можно перейти к созданию раздела для загрузчика UEFI.

- На носителе нет никакой разметки::

    GPT fdisk (gdisk) version X.X.X

    Partition table scan:
      MBR: not present
      BSD: not present
      APM: not present
      GPT: not present

    Creating new GPT entries.

  Можно перейти к созданию раздела.

- На носителе найдена MBR разметка и попорченная GPT разметка::

    GPT fdisk (gdisk) version X.X.X
    Caution: invalid main GPT header, but valid backup; regenerating main header from backup!
    Warning! Main and backup partition tables differ! Use the 'c' and 'e' options
    on the recovery & transformation menu to examine the two tables.
    Warning! One or more CRCs don't match. You should repair the disk!

    Partition table scan:
      MBR: MBR only
      BSD: not present
      APM: not present
      GPT: damaged

    Found a valid MBR and corrupt GPT. Which do you want to use? (Using the
    GPT MAY permit recovery of GPT data.)
     1 - MBR
     2 - GPT
     3 - Create blank GPT

    Your answer:

  Необходимо ввести 3 и перейти к созданию раздела.

- На носителе найдена только MBR разметка::

    GPT fdisk (gdisk) version X.X.X

    Partition table scan:
      MBR: MBR only
      BSD: not present
      APM: not present
      GPT: not present

    ***************************************************************
    Found invalid GPT and valid MBR; converting MBR to GPT format.
    THIS OPERATION IS POTENTIALLY DESTRUCTIVE! Exit by typing 'q' if
    you don't want to convert your MBR parotitions to GPT format!
    ***************************************************************

    Command (? for help): 

  Необходимо:
  
  - выйти из программы разметки ``gdisk`` и очистить начальные сектора::

        dd if=/dev/zero of=/dev/sdc bs=1024000 count=10

  - повторно запустить программe разметки ``gdisk``.

.. Создание раздела EFI Filesystem
   -------------------------------

Для создания раздела нужно ввести команду ``n`` в ответ на приглашение::

  Command (? for help):n

Диалог создания раздела выглядит следующим образом::

  Command (? for help): n
  Partition number (1-128, default 1):<Enter>**
  First sector (34-4029406, default = 2048) or {+-}size{KMGTP}:<Enter
  Last sector (2048-4029406, default = 4029406) or {+-}size{KMGTP}: +250M
  Current type is 'Linux filesystem'
  Hex code or GUID (L to show codes, Enter = 8300): ef00
  Changed type of partition to 'EFI System'

  Command (? for help): w

  Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING
  PARTITIONS!!

  Do you want to proceed? (Y/N): y
  OK; writing new GUID partition table (GPT) to /dev/sdc.
  The operation has completed successfully.

Можно посмотреть как выглядит носитель после создания раздела::

  gdisk -l /dev/sdc

После создания раздела его необходимо отформотировать::

  mkdosfs /dev/sdc1

.. Удаление EFI-загрузки
   =====================
   ::

     fdisk /dev/sdX

   Создать раздел::

     gdisk /dev/sdX

     ...

     Found a valid MBR and corrupt GPT. Which do you want to use? (Using the
     GPT MAY permit recovery of GPT data.)
      1 - MBR
      2 - GPT
      3 - Create blank GPT

   Выбрать 1

efibootmgr
----------
Позволяет управлять пунктами загрузки из командной строки.

.. note::
   Запуск ``efibootmgr`` из под MS Windows делает загрузчик ``Windows Boot Manager`` загрузчиком по умолчанию (присваивает ему наивысший приоритет).

``efibootmgr`` опасная утилита, лучше использовать ``EFI Shell``. 
``efibootmgr`` изменяет напрямую NVRAM. В большинстве случае UEFI умеет восстанавливать испорченный NVRAM каждый раз проверяя его и перезаписывая, в случая неполадок. Однако некоторые недобросовестные производители (ноутбуки с UEFI на базе Phoenix SCT) выпускают прошивки, которые при восстановлении могут превратить компьютер в самый настоящий "кирпич".

Пример::

    efibootmgr -c -L "Archlinux (debug)" \
               -l '\EFI\archlinux\vmlinuz-linux' \
               -u "root=/dev/mapper/vg1-lvroot rw initrd=\EFI\archlinux\initramfs-linux.img systemd.log_level=debug systemd.log_target=kmsg log_buf_len=1M enforcing=0"

UEFI-bootloader
---------------
Если в настройках BIOS разрешен UEFI, то при загрузке прошивка UEFI сначала ищет EFI-раздел, 
а затем пытается исполнить файл ``/EFI/Boot/BOOTX64.EFI``.
Команда::

    bootctl install —path=/boot

копирует исполняемый файл загрузчика на EFI-раздел и добавляет свою загрузочную запись в прошивку.

systemd-boot
------------

Позволяет управлять пунктами меню загрузки с помощью простых настроечных файлов.
Неободимо в корне EFI-раздела создать каталог loader,  
в нём файл loader.conf с тремя строчками::

    default archlinux
    timeout 10
    editor 1

Параметр ``editor`` отвечает за возможность отредактировать пункт загрузочного меню перед запуском.
В том же каталоге, где находится файл \ ``loader.conf`` необходимо создать каталог ``entries``. 
Каждый файл в этом каталоге отвечает за одну загрузочную запись в boot-меню. Пример::

    title Arch Linux
    linux /efi/archlinux/vmlinuz-linuxinitrd /
    efi/archlinux/initramfs-linux.img
    options root=/dev/mapper/vg1-lvroot rw initrd=\EFI\archlinux\intel-ucode.img

.. note::
   Файлы **ядра** и **initramfs** должны находиться на файловой системе
   загрузчика (на EFI-разделе). Пути к ним отсчитываются от корня этой ФС.

UEFI в VirtualBox
-----------------

- Создать ВМ без дисков.
- Включить EFI.
- Включить ВМ

::

    UEFI Interactive Shell v2.0
    Copyright 2009-2011 Intel(r) Corportation. All rights reserved. Beta build 1.0
    UEFI v2.31 (EDK II, 0x00010000)

Файл с EFI загрузчиком в MacOSß::

  /Applications/VirtualBox.app/Contents/MacOS/VBoxEFI64.fd

Получение справки по командам с остановкой в конце каждой страницы::

  help -b

Просмотр текущих параметров загрузки::

  bcfg boot dump -v

Можно добавить EFI-загрузчик MacOS::

  bcfg boot add 3 fs1:\System\Library\CoreServices\boot.efi "10.6"

Другие загрузчики
-----------------

systemd-boot позволяет вручную настраивать очень простое чёрно-белое меню.

rEFind
~~~~~~

- поддерживает текстовый и графический интерфейсы;
- поддерживает TochPad;
- автоматически создает загрузочное меню, сканируя все распознаваемые разделы;
- поддерживает загрузку Linux, MacOS, MS Windows;
- автоматически запоминает последний пункт загрузки.

Документация и установочные пакеты `здесь <http://www.rodsbooks.com/refind/getting.html>`__.
Установливается в каталог ``EFI/refind/refind_x64.efi`` ESP-раздела.

Установить rEFInd повторно, если пакет уже установлен, можно с помощью команды::

  refind-install

Назначить наивысший приоритет ``rEFInd`` при загрузке можно с помощью команды::

  refind-mkdefault

Основной файл настроек - ``/boot/efi/EFI/refind/refind.conf``

``rEFInd`` является .efi-приложением, собранным средствами ``UEFI Shell``. 
``rEFInd`` не является загрузчиком. Это так называемый ``Boot Manager``, он вызвает другие efi-приложения, а также может направить ``UEFI`` на запуск ядра прямо с раздела ``/boot``. 
Другими словами систему загружает не он, а сам ``UEFI``.
В файле ``/boot/refind_linux.conf`` можно задать несколько вариантов с разными параметрами ядра. 
Также в папке ``EFI/tools`` можно разместить различные .efi-приложения (к примеру ``UEFI Shell`` или ``memtest86+``). 
rEFInd их автоматически подхватит и покажет в нижнем ряду как утилиты.

Clover
~~~~~~
Управляет разрешением экрана загрузки, 
имеет поддержку мыши на экране загрузки, 
поддерживает темы оформления. 
Настройки хранятся в ``xml`` формате.
