.. targetcli
   ^^^^^^^^^

Установка::

  yum install targetcli

Первоначальная очистка настроек::

  targetcli clearconfig confirm=true
  All configuration cleared

``targetcli`` является итерактивной оболочкой аналогичной ``bash``. 

.. note::
   При наборе команд работает автодополнение по <TAB>

Далее предполагается, что осуществлен вход в эту оболочку::

  targetcli
  Warning: Could not load preferences file /root/.targetcli/prefs.bin.
  targetcli shell version 2.1.fb35
  Copyright 2011-2013 by Datera, Inc and others.
  For help on commands, type 'help'.
   
  />

Просмотр существующих ресурсов::

  /> ls
  
  o- / .................................................................................   [...]
     o- backstores ......................................................................  [...]
     | o- block ........................................................... [Storage Objects: 0]
     | o- fileio .......................................................... [Storage Objects: 0]
     | o- pscsi ........................................................... [Storage Objects: 0]
     | o- ramdisk ......................................................... [Storage Objects: 0]
     o- iscsi ..................................................................... [Targets: 0]
     o- loopback .................................................................. [Targets: 0]
     o- vhost ..................................................................... [Targets: 0]

Смена "каталога" (ресурса)::

  /> cd <путь к ресурсу>

Подключение 2-х блочных устройств к iSCSI ресурсу::

  /> cd /backstores/block/
  /backstores/block> create <имя1> /dev/<устройство1>
  Created block storage object <имя1> using /dev/<устройство1>.
  /backstores/block> create <имя2> /dev/<устройство2>
  Created block storage object <имя2> using /dev/<устройство2>.
  /backstores/block> ls
  o- block ................................................................ [Storage Objects: 2]
    o- <имя1> ............................. [/dev/<устройство1> (X.XGiB) write-thru deactivated]
    o- <имя2> ............................. [/dev/<устройство2> (X.XGiB) write-thru deactivated]
  /backstores/block>

.. note::
   Отключение устройства::
   
     /> delete <имя>

Создание iSCSI ресурса (target)::

  /> cd /iscsi 
  /iscsi> create
  Created target <имя ресурса>.
  Created TPG 1.
  /iscsi> ls
  o- iscsi ........................................................................ [Targets: 1]
    o- <имя ресурса> ................................................................. [TPGs: 1]
      o- tpg1 ........................................................... [no-gen-acls, no-auth]
        o- acls ...................................................................... [ACLs: 0]
        o- luns ...................................................................... [LUNs: 0]
        o- portals ................................................................ [Portals: 1]
  /iscsi>

.. note::
   Для удаления ресурса::
   
     delete <имя ресурса> 

Создание портала::

  /iscsi/> cd <имя ресурса>tpg1/portals
  /iscsi/<имя ресурса>/tpg1/portals/> create
  Using default IP port 3260
  Binding to INADDR_ANY(0.0.0.0)
  Create network portal 0.0.0.0:3260
  /iscsi/<имя ресурса>/tpg1/portals/> cd /iscsi 
  /iscsi> ls
  o- iscsi ........................................................................ [Targets: 1]
    o- <имя ресурса> ................................................................. [TPGs: 1]
      o- tpg1 ........................................................... [no-gen-acls, no-auth]
        o- acls ...................................................................... [ACLs: 0]
        o- luns ...................................................................... [LUNs: 0]
        o- portals ................................................................ [Portals: 1]
          o- 0.0.0.0:3260 ................................................................. [OK]
  /iscsi>

Подключение устройств в качестве логических "юнитов" (LUNs)::

  /iscsi> cd <имя ресурса>/
  /iscsi/<имя ресурса>> cd tpg1/luns
  /iscsi/<имя ресурса>/tpg1/luns> create /backstores/block/<имя1>
  Created LUN 0.
  /iscsi/<имя ресурса>/tpg1/luns> create /backstores/block/<имя2>
  Created LUN 1.

Проверка::

  /iscsi/<имя ресурса>/tpg1/luns> ls
  o- luns ............................................................................ [LUNs: 2]
    o- lun0 ................................................ [block/<имя1> (/dev/<устройство1>)]
    o- lun1 ................................................ [block/<имя2> (/dev/<устройство2>)]

.. IQN (iSCSI qualified name) – имя длиной до 255 символов в следующем формате.
.. 
.. iqn.<year-mo>.<reversed_domain_name>:<unique_name>
.. <year-mo> - это год (year) и месяц (mo), когда был зарегистрирован домен;
.. <reversed_domain_name> - официальное имя домена, записанное в обратном порядке;
.. <unique_name> - это произвольное имя, например, имя сервера. 

Определение имён iSCSI-ресурсов (IQN) для узлов кластера:

- первый узел (node1)

::

  [root@node1 ~]# cat /etc/iscsi/initiatorname.iscsi
  InitiatorName=<iqn.ГГГГ-ММ-1>

- второй узел (node2)

::

  [root@node2 ~]# cat /etc/iscsi/initiatorname.iscsi
  InitiatorName=<iqn.ГГГГ-ММ-2>

Создание списка доступа (ACL) для узлов::

  /iscsi/<имя ресурса>/tpg1/luns> cd ../acls
  /iscsi/<имя ресурса>/tpg1/acls>
  /iscsi/<имя ресурса>/tpg1/acls> create <iqn.ГГГГ-ММ-1>
  Created Node ACL for <iqn.ГГГГ-ММ-1>
  Created mapped LUN 1.
  Created mapped LUN 0.
  /iscsi/iqn.20...998/tpg1/acls> create <iqn.ГГГГ-ММ-2>
  Created Node ACL for <iqn.ГГГГ-ММ-2>
  Created mapped LUN 1.
  Created mapped LUN 0.

Запись настроек::

  /iscsi/iqn.20.../tpg1/portals> cd /
  /> saveconfig
  Last 10 configs saved in /etc/target/backup.
  Configuration saved to /etc/target/saveconfig.json

Выход из оболочки::

  /> exit
  Global pref auto_save_on_exit=true
  Last 10 configs saved in /etc/target/backup.
  Configuration saved to /etc/target/saveconfig.json

Включение и запуск сервиса ``target``::
 
  [root@node0 ~]# systemctl enable target.service
  ln -s '/usr/lib/systemd/system/target.service' '/etc/systemd/system/multi-user.target.wants/target.service'
  [root@node0 ~]# systemctl start target.service
 
Правила сетевой фильтрации:

- открыть порт TCP 3260

::

  [root@node0 ~]# firewall-cmd --add-port=3260/tcp
  success
  [root@node0 ~]# firewall-cmd --permanent --add-port=3260/tcp
  success

- разрешить HA-сервис

::

  [root@node0 ~]# firewall-cmd --add-service=high-availability
  success
  [root@node0 ~]# firewall-cmd --permanent --add-service=high-availability
  success

Прсмотр правил сетевой фильтрации::

  [root@node0 ~]# firewall-cmd --list-ports 
  3260/tcp

  [root@node0 ~]# firewall-cmd --list-service
  dhcpv6-client high-availability ssh

Приведенные настройки сетевого фильтра необходимо повторить на всех узлах кластера.

.. Create the /var/target/pr directory so that persistent reservations work correctly:
.. 
.. [root@node0 ~]# mkdir -p /var/target/pr
.. 
.. Failure to do this will result in error messages being seen in dmesg, such as:
.. 
.. [ 6272.148841] filp_open(/var/target/pr/aptpl_fd9e668a-7d4b-47fe-acb0-a652dd005103) for APTPL metadata failed
.. [ 6272.148844] SPC-3 PR: Could not update APTPL
