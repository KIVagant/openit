.. ==========
   FTP сервер
   ==========

Протокол FTP является TCP-протоколом для передачи файлов между компьютерами. FTP работает по модели клиент-сервер. Серверная часть отслеживает запросы по FTP протоколу от удаленных клиентов. При получении запроса сервер устанавливает соединение. В рамках FTP сессии сервер выполняет посланные команды и возвращает результат клиенту.

Существуют два режима работы сервера FTP:

* анонимный
* авторизованный

В анонимном режиме, клиенты получают доступ к серверу, используя специальную учетную запись (``anonymous`` или ``ftp``), не передавая ни какого пароля или используя в качестве последнего свой адрес электронной почты. В этом случае, в качестве корневого каталога используется домашний каталог специального пользователя ``ftp``, что позволяет скрыть от анонимных пользователей остальную часть файловой системы.
В авторизованном режиме на сервер допускаются только те пользователи, которые имеют учетную запись. В этом случае, авторизованные пользователи получают доступ к файлам и каталогам в соответствии с правами учетной записи, использованной при входе на сервер.

Установка и настройка
=====================

В большинстве UNIX-систем в качестве сервера FTP используется vsftp сервер. 
Проверить установлен или нет vsftp сервер можно командой::

  rpm -q vsftpd

а установить сервер командой::

  sudo yum install vsftpd

После установки сервер готов к работе со следующими параметрами:

* разрешен анонимный доступ

::

  anonymous_enable=YES

* анонимный пользователь, получает доступ к каталогу ``/var/ftp`` только для чтения

::

  anon_upload_enable=NO

* разрешен авторизованный доступ для локальных пользователей

::

  local_enable=YES

*  локальные пользователи имеют право на запись в соответствии с правами своей учетной записи

::

  write_enable=YES

* для создаваемых файлов и каталогов устанавливается маска доступа “всем на все” (``0777``)

После установки, для запуска сервера с параметрами по умолчанию необходимо запустить сервис::

  sudo service vsftpd start

а также, включить запуск сервиса после перезагрузки::

  sudo chkconfig vsftpd on

.. note::
  Для корректного отображения русских имен каталогов и файлов необходимо настроить локализацию клиента. Русские имена отображаются в кодировке UTF-8. Многие клиенты настроены на кодировку Windows cp1251 или даже KOI-8.

Рекомендуемые параметры безопасности
====================================
Чтобы изменить настройки сервера нужно отредактировать файл настроек::

  /etc/vsftpd.conf

Для повышения безопасности сервера желательно выполнить следующие дополнительные настройки:

* изменить маску по умолчанию для создаваемых файлов так, чтобы владелец файла мог его читать и изменять, а группа и остальные пользователи только читать:

::

  local_umask=022

* скрыть реальных владельцев файлов и каталогов при просмотре FTP ресурса

::

  hide_ids=YES

После изменения настроек необходимо перезапустить сервис командой::

  sudo service vsftpd restart

.. note::
  Для получения дополнительной информации по изменению параметров можно обратиться к руководству ``man vsftpd.conf``

Настройка сетевого фильтра
==========================
В файл /etc/sysconfig/iptables необходимо добавить правила::

  # Разрешить FTP соединение
  -A INPUT  -p tcp --sport 21 -m state --state ESTABLISHED -j ACCEPT
  -A OUTPUT -p tcp --dport 21 -m state --state NEW,ESTABLISHED -j ACCEPT
  
  # Сохранять активное FTP соединение
  -A INPUT -p tcp --sport 20 -m state --state ESTABLISHED,RELATED -j ACCEPT
  -A OUTPUT -p tcp --dport 20 -m state --state ESTABLISHED -j ACCEPT
  
  # Разрешить пассивное FTP соединение
  -A INPUT -p tcp --sport 1024: --dport 1024:  -m state --state ESTABLISHED -j ACCEPT
  -A OUTPUT -p tcp --sport 1024: --dport 1024:  -m state --state ESTABLISHED,RELATED -j ACCEPT

.. Настройка selinux
.. """""""""""""""""
