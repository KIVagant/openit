..	========== ====
	Каталог    Назначение
	========== ====
	``/bin``   находятся исполняемые файлы (программы) большинства утилит системы. Здесь находятся программы, которые могут понадобиться пользователям.
	``/sbin``  находятся исполняемые файлы (программы) системных утилит. Здесь находятся программы, которые могут понадобиться системному администратору для управления системой и ее восстановления после сбоев.
	``/boot``  находятся файлы, необходимые для загрузки ядра и само ядро. Пользователю практически никогда не требуется непосредственно работать с этими файлами.
	``/dev``   находятся все имеющиеся в системе файлы устройств: файлы особого типа, предназначенные для обращения к различным системным ресурсам и устройствам (англ. devices - устройства). В файлах устройств в действительности не хранятся никакие данные, с их помощью осуществляется обмен между устройствами и ОС.
	``/etc``   содержит системные конфигурационные файлы, в которых  хранится информация о пользователях и настройках системы.
	``/home``  расположены каталоги, принадлежащие пользователям системы - домашние каталоги пользователей. По умолчанию, в GNU/Linux системах обычные пользователи могут изменять информацию только в своих домашних каталогах.
	``/lib``   содержатся библиотеки, необходимые для работы наиболее важных системных утилит, размещенных в каталогах /bin и /sbin. В том же каталоге находятся модули ядра (подкаталог modules), шрифты и раскладки клавиатуры, используемые в консольном режиме работы (подкаталог kbd).
	``/media`` предназначен для временного монтирования файловых систем, находящихся на съемных носителях (CD-ROM, USB-flash и др.).
	``/mnt``   предназначен для монтирования файловых систем, находящихся на стационарных носителях.
	``/proc``  все файлы "виртуальные" - они располагаются не на диске, а в оперативной памяти. В этих файлах содержится информация о программах (процессах), выполняемых в данный момент в системе.
	``/root``  домашний каталог системного администратора. 
	``/tmp``   предназначен для временных файлов: в таких файлах программы хранят необходимые для работы промежуточные данные. После завершения программы временные файлы теряют смысл и должны быть удалены. Обычно каталог /tmp очищается при каждой загрузке системы.
	``/var``   размещаются те данные, которые создаются в процессе работы разными программами и предназначены для передачи другим программам и системам (очереди печати, электронной почты и др.) или для сведения системного администратора (системные журналы, содержащие протоколы работы системы). В отличие от каталога /tmp сюда попадают те данные, которые могут понадобиться после того, как создавшая их программа завершила работу.
	``/usr``   во многом повторяет структуру корневого каталога. Здесь можно найти такие же подкаталоги ``bin``, ``sbin``, ``etc``, ``lib``, как и в корневом каталоге. Однако в корневой каталог попадают только утилиты, необходимые для загрузки и восстановления системы в аварийной ситуации - все остальные программы и данные располагаются в подкаталогах каталога /usr.
	``/opt``   предназначен для установки стороннего коммерческого программного обеспечения. Для каждого пакета создается свой подкаталог (имя подкаталога отражает название пакета), повторяющий структуру корневого каталога.
	========== ====

.. -----------------------------------------------------------------------------

+----------+-------------------------------------------------------------------+
|Каталог   |Назначение                                                         |
+==========+===================================================================+
|**/bin**  |Содержит исполняемые файлы (программы) большинства утилит системы. |
|          +-------------------------------------------------------------------+
|          | Здесь находятся программы, доступные обычному пользователю.       |
+----------+-------------------------------------------------------------------+
|**/sbin** |Содержит исполняемые файлы (программы) системных утилит,           |
|          +-------------------------------------------------------------------+
|          |преназначенных для управления системой и ее восстановления после   |
|          +-------------------------------------------------------------------+
|          |сбоев. Здесь находятся программы, доступные только системному      |
|          +-------------------------------------------------------------------+
|          |администратору.                                                    |
+----------+-------------------------------------------------------------------+
|**/boot** |Содержит файлы, необходимые для загрузки ядра и само ядро ОС.      |
+----------+-------------------------------------------------------------------+
|**/dev**  |Содержит файлы устройств, предназначенные для обращения к различным|
|          +-------------------------------------------------------------------+
|          |системным ресурсам и устройствам.                                  |
|          +-------------------------------------------------------------------+
|          |В файлах устройств в действительности не хранятся никакие данные, с|
|          +-------------------------------------------------------------------+
|          |их помощью осуществляется обмен между устройствами и ОС.           |
+----------+-------------------------------------------------------------------+
|**/etc**  |Содержит системные конфигурационные файлы, в которых хранится      |
|          +-------------------------------------------------------------------+
|          |информация о пользователях и настройках системы.                   |
+----------+-------------------------------------------------------------------+
|**/home** |Предназначен для хранения домашних каталогов пользователей. По     |
|          +-------------------------------------------------------------------+
|          |умолчанию, в GNU/Linux системах обычные пользователи могут изменять|
|          +-------------------------------------------------------------------+
|          |информацию только в своих домашних каталогах.                      |
+----------+-------------------------------------------------------------------+
|**/lib**  |Содержит библиотеки, необходимые для работы утилит, размещенных в  |
|          +-------------------------------------------------------------------+
|          |каталогах ``/bin`` и ``/sbin``.                                    |
|          +-------------------------------------------------------------------+
|          |Содержит модули ядра (подкаталог ``modules``), шрифты и раскладки  |
|          +-------------------------------------------------------------------+
|          |клавиатуры, используемые в консольном режиме работы (подкаталог    |
|          +-------------------------------------------------------------------+
|          |``kbd``).                                                          |
+----------+-------------------------------------------------------------------+
|**/media**|Предназначен для монтирования файловых систем, находящихся на      |
|          +-------------------------------------------------------------------+
|          |съемных носителях (CD-ROM, USB-flash и др.).                       |
+----------+-------------------------------------------------------------------+
|**/mnt**  |Предназначен для монтирования файловых систем, находящихся на      |
|          +-------------------------------------------------------------------+
|          |стационарных носителях.                                            |
+----------+-------------------------------------------------------------------+
|**/proc** |Все файлы в этом каталоге "виртуальные" - они располагаются не на  |
|          +-------------------------------------------------------------------+
|          |диске, а в оперативной памяти. В этих файлах содержится информация |
|          +-------------------------------------------------------------------+
|          |о программах (процессах), выполняемых в данный момент в системе.   |
+----------+-------------------------------------------------------------------+
|**/root** |Домашний каталог системного администратора.                        |
+----------+-------------------------------------------------------------------+
|**/tmp**  |Предназначен для временных файлов: в таких файлах программы хранят |
|          +-------------------------------------------------------------------+
|          |необходимые для работы промежуточные данные. Обычно каталог /tmp   |
|          +-------------------------------------------------------------------+
|          |очищается при каждой загрузке системы.                             |
+----------+-------------------------------------------------------------------+
|**/var**  |Размещаются данные, создаваемые в процессе работы системы и        |
|          +-------------------------------------------------------------------+
|          |пользователей: очереди печати, электронная почта, системные        |
|          +-------------------------------------------------------------------+
|          |журналы и т. д. В отличие от каталога ``/tmp`` сюда попадают те    |
|          +-------------------------------------------------------------------+
|          |данные, которые необходимы после того, как создавшая их программа  |
|          +-------------------------------------------------------------------+
|          |завершила работу.                                                  |
+----------+-------------------------------------------------------------------+
|**/usr**  |Во многом повторяет структуру корневого каталога. Здесь можно найти|
|          +-------------------------------------------------------------------+
|          |такие же подкаталоги ``bin``, ``sbin``, ``etc``, ``lib``, как и в  |
|          +-------------------------------------------------------------------+
|          |корневом каталоге. В корневой каталог попадают только утилиты,     |
|          +-------------------------------------------------------------------+
|          |необходимые для загрузки и восстановления системы в аварийной      |
|          +-------------------------------------------------------------------+
|          |ситуации - все остальные программы и данные располагаются в        |
|          +-------------------------------------------------------------------+
|          |подкаталогах каталога /usr.                                        |
+----------+-------------------------------------------------------------------+
|**/opt**  |Предназначен для установки стороннего, коммерческого программного  |
|          +-------------------------------------------------------------------+
|          |обеспечения. Для каждого пакета создается свой подкаталог (имя     |
|          +-------------------------------------------------------------------+
|          |подкаталога отражает название пакета), повторяющий структуру       |
|          +-------------------------------------------------------------------+
|          |корневого каталога.                                                |
+----------+-------------------------------------------------------------------+
