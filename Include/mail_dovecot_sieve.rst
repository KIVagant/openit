.. dovecot:Настройка скриптов фильтрации почты (Sieve)
   ---------------------------------------------------

Дополнение (плагин) Sieve позволяет выполнять скрипты фильтрации почты агентом локальной доставки (LDA) в момент получения письма от почтового сервера (MTA). 
С помощью плагина можно на основании любого поля заголовка или содержимого тела письма:

- раскладывать письма по папкам,
- удалять письма,
- переслать на другой ящик,
- отправлять уведомления отправителю.

Все операции выполняются на строне сервера поэтому пользователю не нужно настраивать правила фильтрации в каждом из используемых им почтовых клиентов.

Установка плагина Sieve::

  yum install dovecot-pigeonhole

.. /etc/dovecot/conf.d/20-managesieve.conf
   /etc/dovecot/conf.d/90-sieve-extprograms.conf
   /etc/dovecot/conf.d/90-sieve.conf

Подключение плагина в файле ``/etc/dovecot/conf.d/15-lda.conf``::

  protocol lda {
    mail_plugins = $mail_plugins sieve
  }

Настройки плагина в файле /etc/dovecot/conf.d/90-sieve.conf::

  plugin {
    sieve = /var/vmail/%d/mail/%n/sieve/active.sieve # Расположение активного скрипта
    sieve_dir = /var/vmail/%d/mail/%n/sieve          # Каталог для скриптов
    sieve_max_script_size = 1M                       # Максимальный размер одного скрипта
    sieve_quota_max_scripts = 50                     # Максимальное количество скриптов
    sieve_quota_max_storage = 1M                     # Максимальный общий объём скриптов
  }

У каждого пользователя должен быть свой каталог для набора Sieve-скриптов (параметр ``sieve_dir``), 
из которых в любой момент времени может быть активен только один (параметр ``sieve`` указывает на активный скрипт).

Selinux
-------

Для каталога, указанного в ``sieve_dir`` нужно добавить контекст::

  semanage fcontext -a -t mail_home_rw_t '<sieve_dir>(/.*)?'

и обновить его::

  restorecon -R -v <sieve_dir>

После настройки плагина нужно перезапустить Dovecot, чтобы изменения вступили в силу::

  /etc/init.d/dovecot restart

.. note::
   Подробнее о написании скриптов Sieve можно почитать в `статье <http://ru.wikipedia.org/wiki/Sieve>`__.

Управление фильтрами Sieve (ManageSieve)
----------------------------------------
Для управления скриптами Sieve из почтового клиента предназначен сервис ManageSieve.
Он ожидает подключений на TCP-порту 4190.
Для управления скриптами используются учётные данные использованные для входа почтовую программу.

Установка сервиса ManageSieve::

  yum install dovecot-pigeonhole

После установки сервис готов к работе.
