.. =========================================
   NIS и автомонтирование домашних каталогов
   =========================================

Настройка сервера
=================

Установить сервер NIS::

  yum install ypserv

Установить клиента NIS::

  yum install ypbind

Запустить сервер NIS::

  service ypserv start

Запустить клиента NIS::

  service ypbind start

Добавить имя домена NIS в настройки сети (/etc/sysconfig/network)::

  NISDOMAIN=yp.nisdomain.name

.. note::
  Рекомендуется не делать одинаковые DNS и NIS домены.

Отредактировать настройки базы данных NIS (/var/yp/Makefaile)::

  MERGE_PASSWD=false
  MERGE_GROUP=false
  all: passwd shadow group hosts rpc services netid protocols 

.. note::
  Если не добавить shadow, то для каждого пользователя нужно задавать пароль с помощью yppasswd, а на сервере нужно запускать службу yppasswdd

Обновить базу данных NIS::

  cd /var/yp
  make

.. note::
  Нужно выполнять каждый раз когда изменяются централизованные параметры.

Настроить клиента (/etc/yp.conf)::

  domain yp.nisdomainl.name server ypserver.dnsdomainl.name

.. note::
  Рекомендуется не делать одинаковые DNS и NIS домены

Проверить::

  yptest

Настройка клиента
=================

Установить клиента NIS::

  yum install ypbind

Запустить клиента NIS::

  service ypbind start
 
Добавить имя домена NIS в настройки сети (/etc/sysconfig/network)::

  NISDOMAIN=yp.nisdomain.name
 
Настроить клиента (/etc/yp.conf)::

  domain yp.nisdomainl.name server ypserver.dnsdomainl.name

.. note::
  Рекомендуется не делать одинаковые DNS и NIS домены

Проверить::

  yptest

nsswitch!!!

pam

Автомонтирования домашних каталогов
===================================

/etc/auto.home::

  #beth	fileserver.example.com:/export/home/beth
  #joe	fileserver.example.com:/export/home/joe
  *	fileserver.example.com:/export/home/&

/var/yp/Makefaile::

  all: passwd shadow group hosts auto.home rpc services netid protocols 

Обновить базу данных NIS::

  cd /var/yp
  make

Добавить в /etc/auto.master::

  /home auto.home
