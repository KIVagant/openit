.. Кластер высокой доступности (HA-cluster) на базе ricci и cman
   =============================================================

Классический HA-cluster с переключением не кластерной ФС между узлами кластера

Перед настройкой кластера нужно установить и настроить сервер синхранизации на обеих узлах::

  yum -y install ntp
  service ntpd start
  ntpq -p
  chkconfig ntpd on
  
Обеспечить распознование узлов по имени.

.. Next, for lab purposes, disable iptables and ip6tables, and switch SELinux into permissive mode::
.. 
..   # service iptables stop
..   # chkconfig iptables off
..   # service ip6tables stop
..   # chkconfig ip6tables off
..   # vi /etc/selinux/config
..   # grep '^SELINUX=' /etc/selinux/config
..   SELINUX=permissive
..   # setenforce permissive

На обеих узлах устанавливаем пакеты необходимые для построения кластера::

  yum -y install ricci cman rgmanager ccs

На обеих узлах устанавливаем пароль для пользователя ``ricci``, от имени которого работает одноименный сервис, обеспечивающий синхронизацию настроек узлов кластера::

  passwd ricci
  
  Changing password for user ricci.
  New password:
  Retype new password:
  passwd: all authentication tokens updated successfully.

Запускаем сервис ``ricci``::

  service ricci start

  Starting system message bus:                               [  OK  ]
  Starting oddjobd:                                          [  OK  ]
  generating SSL certificates...  done
  Generating NSS database...  done
  Starting ricci:                                            [  OK  ]

  service rgmanager start
  Starting Cluster Service Manager:                          [  OK  ]

На обеих узлах включаем сервисы::

  chkconfig ricci on
  chkconfig rgmanager on

Далее, нужно установить утилиту ``omping``, которая обеспечивает тестирование узлов с помощью вспомогательныч широковещательных запросов::

  yum -y install omping

Проверяем доступ к узлам с помощью установленной утилиты::

  [root@node3 ~]# omping 10.1.1.104
  10.1.1.104 : waiting for response msg
  10.1.1.104 : joined (S,G) = (*, 232.43.211.234), pinging
  10.1.1.104 :   unicast, seq=1, size=69 bytes, dist=0, time=0.497ms
  10.1.1.104 : multicast, seq=1, size=69 bytes, dist=0, time=0.507ms
  [root@node4 ~]# 10.1.1.104
  10.1.1.103 : waiting for response msg
  10.1.1.103 : joined (S,G) = (*, 232.43.211.234), pinging
  10.1.1.103 :   unicast, seq=1, size=69 bytes, dist=0, time=0.201ms
  10.1.1.103 : multicast, seq=1, size=69 bytes, dist=0, time=0.281ms

.. note::
   Если указать адрес узла на котором выполняется команда - ругается.

Теперь можно приступить к настройке кластера.

Команды выполняются на одном из узлов (``node3``). После окончания настройки с помощью утилиты ``ccs`` можно синхронизировать настройки на второй узел.
Создадим кластер с именем ``testcluster``::

  [root@node3 ~]# ccs --createcluster testcluster

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="1" name="lnp">
	<fence_daemon/>
	<clusternodes/>
	<cman/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Добавим оба узла с одинаковым ``quorum vote``::

  [root@node3 ~]# ccs --addnode node3 --votes=1 --nodeid=1
  Node node3 added.
  [root@node3 ~]# ccs --addnode node4 --votes=1 --nodeid=2
  Node node4 added.

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="3" name="lnp">
	<fence_daemon/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1"/>
		<clusternode name="gw2" nodeid="2" votes="1"/>
	</clusternodes>
	<cman/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Установим параметры ``fence daemon``::

  [root@node3 ~]# ccs  --setfencedaemon post_fail_delay=0 post_join_delay=30

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="4" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1"/>
		<clusternode name="gw2" nodeid="2" votes="1"/>
	</clusternodes>
	<cman/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Параметр ``post_fail_delay`` задает задержку в секундах для изоляции вышедшего из строя узла.
Параметр ``post_join_delay`` задает задержку в секундах для подключения узла к кластеру.

Установим корректные параметры для сервиса ``cman``. 
Для двух узлов в кластере требуется установить параметр ``two_node`` в 1, и ``expected_votes`` в 1::

  [root@node3 ~]# ccs --setcman two_node=1 expected_votes=1

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="5" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1"/>
		<clusternode name="gw2" nodeid="2" votes="1"/>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Далее, добавим fencing-метод для SCSI-устройств для обеиз узлов::

  [root@node3 ~]# ccs --addmethod scsi node3
  Method scsi added to node3.
  [root@node3 ~]# ccs --addmethod scsi node4
  Method scsi added to node4.

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="7" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1">
			<fence>
				<method name="scsi"/>
			</fence>
		</clusternode>
		<clusternode name="gw2" nodeid="2" votes="1">
			<fence>
				<method name="scsi"/>
			</fence>
		</clusternode>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Создадим ``fence`` устройство на ``/dev/sdc``::

  [root@node3 ~]# ccs --addfencedev scsi_dev agent=fence_scsi devices=/dev/sdc logfile=/var/log/cluster/fence_scsi.log aptpl=1

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="8" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1">
			<fence>
				<method name="scsi"/>
			</fence>
		</clusternode>
		<clusternode name="gw2" nodeid="2" votes="1">
			<fence>
				<method name="scsi"/>
			</fence>
		</clusternode>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices>
		<fencedevice agent="fence_scsi" aptpl="1" devices="/dev/sdb" logfile="/var/log/cluster/fence_scsi.log" name="scsi_dev"/>
	</fencedevices>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

.. todo
   APTPL (Activate Persist Through Power Loss) позволяет управлять вводом-выводом, но и обеспечивает logging для fence агента.

Далле добавим ``fence`` правило в кластер. ``unfence`` правило создается автоматически::

  [root@node3 ~]# ccs --addfenceinst scsi_dev node3 scsi key=1
  Note: Automatically adding unfence action... (use --nounfence to prevent this)
  [root@node3 ~]# ccs --addfenceinst scsi_dev node4 scsi key=2
  Note: Automatically adding unfence action... (use --nounfence to prevent this)

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="12" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1">
			<fence>
				<method name="scsi">
					<device key="1" name="scsi_dev"/>
				</method>
			</fence>
			<unfence>
				<device action="on" key="1" name="scsi_dev"/>
			</unfence>
		</clusternode>
		<clusternode name="gw2" nodeid="2" votes="1">
			<fence>
				<method name="scsi">
					<device key="2" name="scsi_dev"/>
				</method>
			</fence>
			<unfence>
				<device action="on" key="2" name="scsi_dev"/>
			</unfence>
		</clusternode>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices>
		<fencedevice agent="fence_scsi" aptpl="1" devices="/dev/sdb" logfile="/var/log/cluster/fence_scsi.log" name="scsi_dev"/>
	</fencedevices>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

Создадим отказоустойчивый домен::

  [root@node3 ~]# ccs --addfailoverdomain fs-failover ordered=1 nofailback=1

Добавим оба узла в отказоустойчивый домен::

  [root@node3 ~]# ccs --addfailoverdomainnode fs-failover node3 1
  [root@node3 ~]# ccs --addfailoverdomainnode fs-failover node4 2

Добавляем сервисы в отказоустойчивый домен::

  [root@node3 ~]# ccs --addservice fs domain=fs-failover recovery=relocate autostart=1

Далее добавляем файловые ресурсы::

  [root@node3 ~]# ccs --addresource fs name=failover_fs device=/dev/sdb1 mountpoint=/mnt fstype=ext4

.. Finally, add a subservice tying the filesystem resource back to the service. These are used more for when resource ordering is required, but I’ll add it anyway::

::

  [root@node3 ~]# ccs --addsubservice fs fs ref=failover_fs

::

  cat /etc/cluster/cluster.conf 
  <?xml version="1.0"?>
  <cluster config_version="18" name="lnp">
	<fence_daemon post_fail_delay="0" post_join_delay="30"/>
	<clusternodes>
		<clusternode name="gw1" nodeid="1" votes="1">
			<fence>
				<method name="scsi">
					<device key="1" name="scsi_dev"/>
				</method>
			</fence>
			<unfence>
				<device action="on" key="1" name="scsi_dev"/>
			</unfence>
		</clusternode>
		<clusternode name="gw2" nodeid="2" votes="1">
			<fence>
				<method name="scsi">
					<device key="2" name="scsi_dev"/>
				</method>
			</fence>
			<unfence>
				<device action="on" key="2" name="scsi_dev"/>
			</unfence>
		</clusternode>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices>
		<fencedevice agent="fence_scsi" aptpl="1" devices="/dev/sdb" logfile="/var/log/cluster/fence_scsi.log" name="scsi_dev"/>
	</fencedevices>
	<rm>
		<failoverdomains>
			<failoverdomain name="fs-failover" nofailback="0" ordered="0" restricted="0">
				<failoverdomainnode name="gw1" priority="1"/>
				<failoverdomainnode name="gw2" priority="2"/>
			</failoverdomain>
		</failoverdomains>
		<resources>
			<fs device="/dev/sdb1" fstype="ext4" mountpoint="/srv" name="failover_fs"/>
		</resources>
		<service autostart="1" domain="fs-failover" name="fs" recovery="relocate">
			<fs ref="failover_fs"/>
		</service>
	</rm>
  </cluster>

Клонируем настройки на второй узел ``node4``::

  [root@node3 ~]# ccs --sync --activate

Запустим сервис ``cman`` на обеих узлах::

  [root@node3 ~]# service cman start

::

  Starting cluster: 
   Checking if cluster has been disabled at boot...        [  OK  ]
   Checking Network Manager...                             [  OK  ]
   Global setup...                                         [  OK  ]
   Loading kernel modules...                               [  OK  ]
   Mounting configfs...                                    [  OK  ]
   Starting cman...                                        [  OK  ]
   Waiting for quorum...                                   [  OK  ]
   Starting fenced...                                      [  OK  ]
   Starting dlm_controld...                                [  OK  ]
   Tuning DLM kernel config...                             [  OK  ]
   Starting gfs_controld...                                [  OK  ]
   
   !!!
   Unfencing self... unfence gw1 failed
                                                           [СБОЙ ]
   !!!

  Stopping cluster: 
   Leaving fence domain...                                 [  OK  ]
   Stopping gfs_controld...                                [  OK  ]
   Stopping dlm_controld...                                [  OK  ]
   Stopping fenced...                                      [  OK  ]
   Stopping cman...                                        [  OK  ]
   Waiting for corosync to shutdown:                       [  OK  ]
   Unloading kernel modules...                             [  OK  ]
   Unmounting configfs...                                  [  OK  ]


Запускаем все сервисы::

  [root@node3 ~]# ccs --startall
  Started node3
  Started node4
  
Проверяем состояние::

  [root@node3 ~]# clustat
  Cluster Status for testcluster @ Thu Jul 17 20:10:44 2014
  Member Status: Quorate
  
   Member Name                                     ID   Status
   ------ ----                                     ---- ------
   node3                                            1 Online, Local, rgmanager
   node4                                            2 Online, rgmanager
   
   Service Name                           Owner (Last)                        State
   ------- ----                           ----- ------                        -----
   service:fs                             node3                               started

Проверяем смонтированную файловую систему на ``node3``::

  [root@node3 ~]# df -h
  Filesystem                       Size  Used Avail Use% Mounted on
  /dev/mapper/vg_node3-lv_root   18G  1.1G   16G   7% /
  tmpfs                            495M   23M  473M   5% /dev/shm
  /dev/sda1                        485M   33M  427M   8% /boot
  /dev/sdb1                        7.9G  146M  7.8G   2% /mnt

В данный момент раздел ``/dev/sdb1`` смонтирован в каталог ``/mnt``. 
При перезагрузке ``node3`` файловая системы должна переключиться на узел ``node4``::

  [root@node3 ~]# reboot
   
  Broadcast message from root@node3
          (/dev/pts/0) at 20:11 ...
  
  The system is going down for reboot NOW!
  
  [root@node4 ~]# df -h
  Filesystem                       Size  Used Avail Use% Mounted on
  /dev/mapper/vg_node4-lv_root   18G  1.1G   16G   7% /
  tmpfs                            495M   23M  473M   5% /dev/shm
  /dev/sda1                        485M   33M  427M   8% /boot
  /dev/sdb1                        7.9G  146M  7.8G   2% /mnt

Все работает. Следующие команды позволяют анализировать состояние кластера::

  [root@node3 ~]# ccs --lsnodes
  node3: votes=1, nodeid=1
  node4: votes=1, nodeid=2
  [root@node3 ~]# ccs --lsfencedev
  scsi_dev: logfile=/var/log/cluster/fence_scsi.log, aptpl=1, devices=/dev/sdc, agent=fence_scsi
  [root@node3 ~]# ccs --lsfailoverdomain
  fs-failover: restricted=0, ordered=0, nofailback=0
    node3: priority=1
    node4: priority=2
  [root@node3 ~]# ccs --lsservices
  service: name=fs, domain=fs-failover, autostart=1, recovery=relocate
    fs: ref=failover_fs
  resources:
    fs: name=failover_fs, device=/dev/sdb1, mountpoint=/mnt, fstype=ext4
