.. Аудит состояния системы
   =======================

Основным инструментом аудита Red Hat совместимых дистрибутивов GNU/Linux является система `Auditd <http://people.redhat.com/sgrubb/audit/>`__. 

Архитектура и принцип работы
----------------------------

Подсистема аудита предназначена для отслеживания критичных с точки зрения безопасности системных событий. 
В качестве примеров таких событий можно привести следующие (список далеко не полный):

- запуск и завершение работы системы;
- чтение, запись и изменение прав доступа к файлам;
- инициация сетевых соединений;
- попытки неудачной авторизации в системе;
- изменение сетевых настроек;
- изменение информации о пользователях и группах;
- запуск и остановка приложений;
- выполнение системных вызовов.

Отслеживание происходит с помощью перехвата соответствующих событиям системных вызовов на уровне ядра системы.

Установка
---------

В дистрибутивах GNU/Linux, основанных на Red Hat (Fedora, CentOS и т.
д.), сервис auditd установлен и запускается по умолчанию.

Проверка состояния сервиса::

    sudo systemctl status auditd.service

Запуск сервиса::

    sudo systemctl start auditd.service

Включение сервиса в загрузку::

    sudo systemctl enable auditd.service

Состав системы
--------------
============ ====
**auditd**   основеой сервис
**auditctl** утилита для управления правилами и анализа текущего состояния системы аудита
**aureport** утилита для генерации отчётов о работе системы аудита
**autrace**  утилита для аудита событий, порождаемых процессами (аналог strace)
**ausearch** утилита для поиска событий в журналах аудита
============ ====

Настройка
---------

Основной файд конфигурации `/etc/audit/auditd.conf` содержит в числе прочих следующие параметры:

log_file            
  файл, в котором будут храниться протоколы подсистемы аудита.

log_format
  формат, в котором будет сохранены протоколы.

freq
  максимальное число записей протокола, которые могут храниться в буфере.

flush
  режим синхронизации буфера с диском:
  
  - none — ничего не делать,
  - incremental — переносить данные из буфера на диск с частотой, указанной в значении параметра freq,
  - data — синхронизировать немедленно,
  - sync — синхронизировать как данные, так и метаданные файла при записи на диск.

max_log_file
  максимальный размер файла протокола в мегабайтах.

max_log_file_action
  действие при превышении максимального размера файла протокола.

space_left
  минимум свободного пространства в мегабайтах, по достижении которого должно быть осуществлено действие, указанное в следующем параметре.

space_left_admin
  указывает, что делать, когда на диске недостаточно свободного места:
  
  - ignore — ничего не делать,
  - syslog — отправлять в syslog, 
  - email — отправлять уведомление по почте,
  - suspend — прекратить запись протоколов на диск,
  - single — перейти в однопользовательский режим,
  - halt — выключить машину.

disk_full_action
  действие, которое нужно осуществить при переполнении диска (этот параметр может принимать те же значения, что и space_left_admin).

Генератор отчетов aureport
--------------------------

В дистрибутивах GNU/Linux, основанных на Red Hat (Fedora, CentOS и т.
д.), утилита aureport установлена по умолчанию.

Справка по ключам::

    aureport --help

	-a,--avc			Avc report
	-au,--auth			Authentication report
	--comm				Commands run report
	-c,--config			Config change report
	-cr,--crypto			Crypto report
	-e,--event			Event report
	-f,--file			File name report
	--failed			only failed events in report
	-h,--host			Remote Host name report
	--help				help
	-i,--interpret			Interpretive mode
	-if,--input <Input File name>	use this file as input
	--input-logs			Use the logs even if stdin is a pipe
	--integrity			Integrity event report
	-l,--login			Login report
	-k,--key			Key report
	-m,--mods			Modification to accounts report
	-ma,--mac			Mandatory Access Control (MAC) report
	-n,--anomaly			aNomaly report
	-nc,--no-config			Don't include config events
	--node <node name>		Only events from a specific node
	-p,--pid			Pid report
	-r,--response			Response to anomaly report
	-s,--syscall			Syscall report
	--success			only success events in report
	--summary			sorted totals for main object in report
	-t,--log			Log time range report
	-te,--end [end date] [end time]	ending date & time for reports
	-tm,--terminal			TerMinal name report
	-ts,--start [start date] [start time]	starting data & time for reports
	--tty				Report about tty keystrokes
	-u,--user			User name report
	-v,--version			Version
	--virt				Virtualization report
	-x,--executable			eXecutable name report

Отчет об отслеживаемых событиях::

    sudo aureport

    Summary Report
    ======================
    Range of time in logs: 18/05/16 09:47:34.453 - 22/05/16 11:28:03.168
    Selected time for report: 18/05/16 09:47:34 - 22/05/16 11:28:03.168
    Number of changes in configuration: 195
    Number of changes to accounts, groups, or roles: 30
    Number of logins: 5
    Number of failed logins: 0
    Number of authentications: 136
    Number of failed authentications: 9
    Number of users: 4
    Number of terminals: 12
    Number of host names: 2
    Number of executables: 13

.. note::
   Большоое значение параметра::

     Number of failed authentications

   может говорить о том, что была сделана попытка подбора пароля одного из
   пользователей с целью получить не санкционированный доступ к системе.

Команда::

    sudo aureport -au

выводит дату, время, имя пользователя и результаты проверки
аутентификации (опция ``-au``) для всех пользователей::

    Authentication Report
    ============================================
    # date time acct host term exe success event
    ============================================
    1. 18/05/16 09:47:56 sddm ? ? /usr/lib/sddm/sddm-helper yes 187
    2. 18/05/16 09:48:09 paul ? ? /usr/lib/sddm/sddm-helper yes 199
    3. 18/05/16 09:41:29 root ? pts/1 /usr/bin/su yes 227
    4. 18/05/16 09:57:16 root ? pts/2 /usr/bin/su yes 231
    5. 18/05/16 10:01:57 root ? ? /usr/sbin/groupadd yes 235

С помощью фильтра grep можно получить список неудачных попыток
аутентификации::

    sudo aureport -au | grep no

    37. 18/05/16 12:18:24 root ? pts/0 /usr/bin/su no 217
    38. 18/05/16 12:18:38 root ? pts/0 /usr/bin/su no 218
    47. 18/05/16 12:41:15 root ? pts/1 /usr/bin/su no 262
    66. 18/05/16 14:09:55 root ? pts/4 /usr/bin/su no 220
    67. 18/05/16 14:10:05 root ? pts/4 /usr/bin/su no 221
    102. 20/05/16 12:37:07 root ? pts/5 /usr/bin/su no 191
    117. 21/05/16 12:10:39 root ? pts/0 /usr/bin/su no 229
    129. 21/05/16 17:59:08 root ? pts/1 /usr/bin/su no 208
    134. 21/05/16 18:32:05 root ? pts/0 /usr/bin/su no 248

Отчет по пользователям (опция ``-u``) с опцией ``-i`` отображает имена пользователей вместо их
идентификаторов::

    sudo aureport -u -i

    User ID Report
    ====================================
    # date time auid term host exe event
    ====================================
    1. 18/05/16 09:47:35 unset ? ? /usr/lib/systemd/systemd 136
    2. 18/05/16 09:47:35 unset ? ? /usr/lib/systemd/systemd-update-utmp 137
    3. 18/05/16 09:47:35 unset ? ? /usr/lib/systemd/systemd 138
    4. 18/05/16 09:47:45 unset ? ? /usr/lib/systemd/systemd 139
    5. 18/05/16 09:47:45 unset ? ? /usr/lib/systemd/systemd 140

C помощью цепочки команд::

    accessdate=`aureport -au | grep no | tail -1 | cut -d ' ' -f 2,3`; \
    aureport -u -i | grep "$accessdate"; \
    unset accessdate

можно получить информацию о последнем неудачном входе в систему::

    2324. 21/05/16 18:32:05 paul pts/3 ? /usr/bin/su 201

C помощью цепочки команд::

    aureport -au | grep no | cut -d ' ' -f 2,3 > accessdates.log; \
    aureport -u -i | grep -f accessdates.log; \
    rm accessdates.log

можно получить информацию о всех неудачных попытках аутентификации::

    986. 18/05/16 14:09:55 paul pts/4 ? /usr/bin/su 220
    987. 18/05/16 14:10:05 paul pts/4 ? /usr/bin/su 221
    1577. 20/05/16 12:37:07 paul pts/5 ? /usr/bin/su 191
    1785. 21/05/16 12:10:39 paul pts/0 ? /usr/bin/su 229
    2050. 21/05/16 17:59:08 paul pts/1 ? /usr/bin/su 208
    2090. 21/05/16 18:32:05 paul pts/0 ? /usr/bin/su 248

Поддерживается фильтрация по дате и времени::

    sudo aureport -s --start 07/31/15 12:00 --end 07/31/15 13:00

Можно указывать как конкретные время и дату, так и специальные конструкции::

    now       - текущий момент;
    yesterday - вчерашнее сутки;
    recent    - 10 минут назад;
    this      - week (или this-month, this-year) — текущая неделя (месяц, год).

С помощью aureport можно просмотреть информацию о действиях любого пользователя системы по его ``uid``.
Для этого нужно сначала узнать ``uid`` этого пользователя::

	id user
	uid=1000(user) gid=1000(andrei) groups=1000(andrei),27(sudo)

а затем выполнить команду::

	sudo ausearch -ui 1000 --interpret

Поиск и анализ событий с помощью ausearch
-----------------------------------------

Для просмотра детальной информации о событии по его номеру (идентификатору) используется команда::

  sudo ausearch -a <номер события>

Пример вывода команды::

	type=SYSCALL msg=audit(1364481363.243:24287): arch=c000003e syscall=2 success=no exit=-13 a0=7fffd19c5592 a1=0 a2=7fffd19c4b50 a3=a items=1 ppid=2686 pid=3538 auid=500 uid=500 gid=500 euid=500 suid=500 fsuid=500 egid=500 sgid=500 fsgid=500 tty=pts0 ses=1 comm="cat" exe="/bin/cat" subj=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 key="sshd_config"

.. note::
   Чтобы информация выводилась в более понятном виде, можно воспользоваться опцией -i или −−interpret.

type 
  указывает на тип записи; type = syscall означает, что запись была сделана после выполнения системного вызова. 

msg 
  указывает на время события и уникальный идентификационный номер события.

arch 
  содержи информация об используемой архитектуре системы (c000003e означает x86_84) в закодированном шестнадцатеричном формате 

syscall 
  указывает на тип системного вызова — в данном случае это 2, то есть вызов open. 

success
  сообщает, был ли вызов обработан успешно или нет. В примере вызов был обработан неудачно (success = no).

В зависимости от критерия поиска в выводе команды могут отображаться индивидуальные параметры щ которых можно получить информацию в официальном руководстве.

Вывести информацию о любом параметре в более читаемом виде можно при помощи упомянутой выше опции -i или −−interpret, например::

  sudo ausearch --interpet --exit  -13

Опция ``-sc`` позволяет включать в список события, относящиеся к указанному системному вызову, например::

  sudo ausearch -sc ptrace

Опция ``-ui`` служит для поиска событий по идентификатору пользователя::

  ausearch -ui 33

Поиск по именам процессов осуществляется с помощью опции ``-tm``::

  ausearch -x -tm cron

Для поиска нужных событий можно также использовать ключи, например::

  sudo auditctl -k root-actions

Приведённая команда выведет список всех действий, совершённых от имени root-пользователя. Поддерживается также фильтрация по дате и времени, аналогичная той, что была описана выше. 

Управление правилами с помощью auditctl
---------------------------------------
Список опций auditctl::

    -l — вывести список имеющихся правил;
    -а — добавить новое правило;
    -d — удалить правило из списка;
    -D — удалить все имеющиеся правила.

Чтобы создать новое правило, нужно выполнить команду вида::

  auditctl -a <список>, <действие> -S <имя системного вызова> -F <фильтры>

После опции ``-а`` указывается список, в который нужно добавить правило. 
Всего существует 5 таких списков событий.

task
  создание новых процессов.
entry
  перед выполнением системного вызова.
exit
  после системного вызова.
user
  события, использующие параметры пользовательского пространства.
exclude
  список исключенных событий.

Затем указывается действие, которое нужно выполнить после наступления события. 
Здесь возможны два варианта: 

- always - событие записывается в журнал
- never - событие не записывается в журнал.

После опции -S идёт имя системного вызова, при котором событие нужно перехватить (open, close и т.п.).

После опции -F указываются дополнительные параметры фильтрации. 
Например, если требуется вести аудит обращений к файлам из каталога ``/etc``, правило будет выглядеть так::

  auditctl -a exit,always -S open -F path =/etc/

Можно установить и дополнительный фильтр::

  auditctl -a exit,always -S open -F path =/etc/ -F perm = aw

Фильтр ``-F perm = aw`` ограничивает протоколирование действий над файлами каталога ``/etc`` операциями изменения атрибутов (а — attribute change) и записи  (w — write).

При настройке слежения за отдельными файлами можно опустить опцию -S, например::

  auditctl -a exit,always -F path =/etc/ -F perm = aw

Файлы правил
------------
Правила храняться в файле ``/etc/audit/audit.rules``.
Файл начинается с так называемых метаправил, в которых задаются общие настройки журналирования::

	# удаляем все ранее созданные правила
	-D

	# задаём количество буферов, в которых будут храниться сообщения
	-b 320

	# указываем, что делать в критической ситуации (например, при переполнении буферов):
	0 - ничего не делать; 1 - отправлять сообщение в dmesg, 2 -  отправлять ядро в панику
	-f 1

Далее следуют пользовательские правила. Каждому правилу соответствует строка с перечислением опции команды auditctl.

Пример типового файла правил::

	# отслеживать системные вызовы unlink () и rmdir()
	-a exit,always -S unlink -S rmdir

	# отслеживать системные вызовы open () от пользователя с UID 1001
	-a exit,always -S open -F loginuid=1001

	# отслеживать доступ к файлам паролей и групп и попытки их изменения:
	-w /etc/group -p wa
	-w /etc/passwd -p wa
	-w /etc/shadow -p wa
	-w /etc/sudoers -p wa

	# отслеживать доступ к следующей директории:
	-w /etc/my_directory -p r

	# закрыть доступ к конфигурационному файлу для предотвращения изменений
	-e 2

После изменения правил нужно перезапустить сервис::

  sudo service auditd restart

Централизованное хранение протоколов
------------------------------------

Для отправки протоколов в централизованное хранилище используется плагин audisp-remote. 
Он входит в пакет audisp-plugins, который нужно устанавливать дополнительно::

  sudo yum install audisp-plugins

Конфигурационные файлы всех плагинов хранятся в директории::

  /etc/audisp/plugins.d

Настройки удалённого протоколирования прописываются в конфигурационном файле:: 

  /etc/audisp/plugins.d/audisp-remote.conf

По умолчанию этот файл выглядит так::

	active = no
	direction = out
	path = /sbin/audisp-remote
	type = always
	#args =
	format = string

Чтобы активировать отправку протоколов в удалённое хранилище нужно заменить значение параметра active на yes. 
Затем в файле etc/audisp/audisp-remote.conf в качестве значения параметра remote_server нужно указать IP-адрес cервера, на котором будут храниться протоколы.

Чтобы сервер принимал протоколы с других узлов в файле /etc/audit/auditd.conf нужно прописать следующие параметры::

	tcp_listen_port = 60
	tcp_listen_queue = 5
	tcp_max_per_addr = 1
	##tcp_client_ports = 1024-65535 #optional
	tcp_client_max_idle = 0
