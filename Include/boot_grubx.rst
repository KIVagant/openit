.. Загрузчик GRUB и GRUB2
   ======================

Загрузчик ОС, который разрабатывается в рамках одноименного открытого проекта. Загрузчик GRUB может быть установлен в большинстве ОС семейства UNIX, a также в некоторых ОС семейства MS Windows. При установке GRUB в MBR записывается нестандартный загрузочный код, который позволяет активизировать системные загрузчики разделов с помощью меню. Предусмотрена возможность передачи управления системному загрузчику расположенному как в загрузочном секторе, так и непосредственно в файловой системе. В первом случае загрузчик GRUB ведет себя как стандартный внесистемный загрузчик и позволяет загружать любые ОС (в частности, ОС семейства MS Windows). Во втором случае, могут загружаться только те ОС, файловые системы которых, поддерживаются загрузчиком GRUB.

На настоящий момент существуют две версии загрузчика - GRUB и GRUB2.
