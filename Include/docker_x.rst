.. Запуск графических приложений в Docker
   ======================================

Для того чтобы графические приложения могли работать в контейнере они должны подключаться к X-серверу хоста. 

Монтирование устройств
----------------------

Для подключения к X-серверу можно воспользоваться доменными сокетами Unix для X11, которые, обычно, находятся в каталоге ``/tmp/.X11-unix``. 
Получить доступ к сокетам можно смонтировав эту директорию с помощью оции ``-v``. 
Также необходимо настроить переменную окружения `DISPLAY <http://www.x.org/releases/X11R7.7/doc/man/man7/X.7.xhtml#heading5>`__,
которая указывает приложениям экран для вывода графики. 
Так как выводить нужно на X-сервер хост-машины, то значение DISPLAY может быть ``:0.0`` или просто ``:0``. 
Пустое имя хоста (перед двоеточием) `подразумевает <http://www.x.org/releases/X11R7.7/doc/man/man1/Xorg.1.xhtml#heading5>`__
локальное соединение с использованием самого эффективного транспорта,
что в большинстве случаев означает доменные сокеты Unix::

    $ docker run \
             -e DISPLAY=unix$DISPLAY \
             -v /tmp/.X11-unix:/tmp/.X11-unix \
             <image>

Приставка «unix» к DISPLAY явно указывает на использование unix-сокетов, но чаще всего в этом нет необходимости.

Ошибка авторизации
^^^^^^^^^^^^^^^^^^

При запуске можно столкнуться с ошибкой вида::

    No protocol specified
    Error: cannot open display: unix:0.0

Такое происходит, когда расширение `Xsecurity <http://www.x.org/releases/X11R7.7/doc/man/man7/Xsecurity.7.xhtml>`__
блокирует неавторизованные подключения к X-серверу. Решить эту проблему
можно, например, разрешив все локальные несетевые подключения::

    $ xhost +local:

Или ограничиться разрешением только для root-пользователя::

    $ xhost +si:localuser:root

В большинстве случаев этого должно быть достаточно.

Подключение звуковых устройств
------------------------------

В версиях Docker до 1.2 звуковые устройства можно смонтировать с помощью параметра ``-v``,
при этом контейнер должен быть запущен в привилегированном режиме::

    $ docker run -v /dev/snd:/dev/snd --privileged <image>

В Docker 1.2 был добавлен специальный параметр ``--device`` для подключения устройств. 
К сожалению, на данный момент (версия 1.2), ``--device`` в качестве значения может принимать только одно устройство за раз, а значит придется явно перечислять их все. 
Например::

    $ docker run \
             --device=/dev/snd/controlC0 \
             --device=/dev/snd/pcmC0D0p \
             --device=/dev/snd/seq \
             --device=/dev/snd/timer \
             <image>

Возможно, функция обработки всех устройств в директории через ``--device`` будет добавлена в будущих релизах (на github есть соответствующий
`запрос <https://github.com/docker/docker/issues/7721>`__).

В итоге
-------

Суммируя, команда для запуска контейнера с графическим приложением
выглядит примерно так::

    $ docker run \
             -v /tmp/.X11-unix:/tmp/.X11-unix \
             -e DISPLAY=unix$DISPLAY \
             --device=/dev/snd/controlC0 \
             --device=/dev/snd/pcmC0D0p \
             --device=/dev/snd/seq \
             --device=/dev/snd/timer \
             <image>

Или, для Docker версии ниже 1.2:

::

    $ docker run \
             -v /tmp/.X11-unix:/tmp/.X11-unix \
             -e DISPLAY=unix$DISPLAY \
             -v /dev/snd:/dev/snd \
             --privileged \
             <image>

Запуск графического аудиоплеера deadbeef
----------------------------------------

Dockerfile для сборки образа с плеером::

    FROM debian:wheezy

    ENV DEBIAN_FRONTEND noninteractive

    RUN apt-get update

    RUN apt-get install -yq wget

    # установка deadbeef
    RUN wget -P /tmp \
      'http://sourceforge.net/projects/deadbeef/files/debian/0.6.2/deadbeef-static_0.6.2-2_amd64.deb' \
      && dpkg -i /tmp/deadbeef-static_0.6.2-2_amd64.deb || true \
      && apt-get install -fyq --no-install-recommends \
      && ln -s /opt/deadbeef/bin/deadbeef /usr/local/bin/deadbeef \
      && rm /tmp/deadbeef-static_0.6.2-2_amd64.deb

    # будем запускать проигрыватель вместе с контейнером
    ENTRYPOINT ["/opt/deadbeef/bin/deadbeef"]

Сборка образа::

    $ docker build -t deadbeef .

Запуск (плеер запустится на полной громкости)::

    $ docker run --rm \
             -v /tmp/.X11-unix:/tmp/.X11-unix \
             -e DISPLAY=unix$DISPLAY \
             --device=/dev/snd/controlC0 \
             --device=/dev/snd/pcmC0D0p \
             --device=/dev/snd/seq \
             --device=/dev/snd/timer \
             deadbeef http://94.25.53.133/ultra-128.mp3

Запуск графических приложений с помощью ssh
-------------------------------------------

Параметр ``-X`` при создании ssh соединения включает перенаправление X11, 
что позволяет отображать на локальной машине графическое приложение, запущенное на удаленной машине.
В роли удаленной машины может выступать docker-контейнер.

В контейнере при этом должен быть установлен и запущен ssh-сервер.
Также следует удостовериться, что в настройках сервера разрешено
перенаправление X11. 
Проверить это можно заглянув в ``/etc/ssh/sshd_config`` и поискав параметр ``X11Forwarding`` (или его
синонимы: ``ForwardX11``, ``AllowX11Forwarding``), который должен быть
установлен в ``yes``::

    X11Forwarding yes

В ssh нет опции для перенаправления звука. 
Но настроить его можно с помощью звукового сервера `PulseAudio <http://www.freedesktop.org/wiki/Software/PulseAudio/>`__,
для которого можно разрешить клиентский доступ из контейнера. 
Проще всего это сделать через `paprefs <http://freedesktop.org/software/pulseaudio/paprefs/>`__.
Установив ``paprefs`` надо зайти в настройки PulseAudio и на вкладке
«Network Settings» поставить галочку напротив «Enable network access to
local sound devices» (включение сетевого доступа к локальным звуковым
устройствам). Там же можно найти опцию «Don't require authentication»
(не требовать авторизации). Включение этой опции разрешит
неавторизованный доступ к серверу, что может упростить настройку
docker-контейнеров. Для авторизованного же доступа необходимо
скопировать в контейнер файл ``~/.pulse-cookie``.

После изменения настроек PulseAudio следует перезапустить::

    $ sudo service pulseaudio restart

или::

    $ pulseaudio -k && pulseaudio --start

В некоторых случаях может потребоваться перезагрузка. Проверить
принялись ли настройки можно с помощью команды ``pax11publish``, вывод
которой должен выглядеть примерно так::

    Server: <...>unix:/run/user/1000/pulse/native tcp:<hostname>:4713 tcp6:<hostname>:4713
    Cookie: <...>

Здесь можно увидеть, что аудиосервер «слушает» на unix-сокете
(unix:/run/user/1000/pulse/native), 4713-м TCP и TCP6 портах
(стандартный порт для PulseAudio).

Для того, чтобы X-приложения в контейнере подключались к pulse-серверу нужно указать его адрес в переменной окружения ``PULSE_SERVER``::

    $ PULSE_SERVER=tcp:172.17.42.1:4713

Здесь ``172.17.42.1`` адрес docker-хоста. Узнать этот адрес можно, например, так::

    $ ip route | awk '/docker/ { print $NF }'
    172.17.42.1

Строка ``tcp:172.17.42.1:4713`` говорит о том, что подключиться к
pulse-серверу нужно по IP-адресу 172.17.42.1 к TCP-порт 4713.

----

При использовании вышеописанного метода весь звук будет передаваться в
открытом виде (нешифрованном), что в случае использования контейнера на
локальном компьютере не имеет особого значения. Но при желании этот
трафик можно и зашифровать. Для этого ``PULSE_SERVER`` следует настроить на
любой свободный порт на localhost (например: ``tcp:localhost:64713``). 
В результате аудиопоток будет идти на локальный порт 64713, который в свою
очередь можно пробросить на 4713-й порт хостовой машины с помощью::

    $ ssh -X -R 64713:localhost:4713 <user>@<hostname>

Аудиоданные при этом будут передаваться по зашифрованному каналу.

Запуск графического аудиоплеера deadbeef с помощью ssh
------------------------------------------------------

Скрипт entrypoint.sh для «точки входа», который будет выполняться каждый раз при
запуске контейнера будет создавать пользователя со случайным
паролем, настраивать переменную окружения ``PULSE_SERVER`` и запускать
ssh-сервер.

entrypoint.sh::

    #!/bin/bash

    # формируем переменную окружения PULSE_SERVER
    PA_PORT=${PA_PORT:-4713}
    PA_HOST=${PA_HOST:-localhost}
    PA_SERVER="tcp:$PA_HOST:$PA_PORT"

    # имя пользователя для подключения
    DOCKER_USER=dockerx

    # генерируем пароль пользователя
    DOCKER_PASSWORD=$(pwgen -c -n -1 12)
    DOCKER_ENCRYPTED_PASSWORD=$(perl -e 'print crypt('"$DOCKER_PASSWORD"', "aa")')

    # выводим имя и пароль пользователя,
    # чтобы их можно было увидеть с помощью docker logs
    echo User: $DOCKER_USER
    echo Password: $DOCKER_PASSWORD

    # создаем пользователя
    useradd --create-home \
            --home-dir /home/$DOCKER_USER \
            --password $DOCKER_ENCRYPTED_PASSWORD \
            --shell /bin/bash \
            --user-group $DOCKER_USER

    # добавляем инициализацию и экспорт PULSE_SERVER в ~/.profile для нового пользователя,
    # чтобы она была доступна во время его сессии
    echo "PULSE_SERVER=$PA_SERVER; export PULSE_SERVER" >> /home/$DOCKER_USER/.profile

    # запускаем ssh-сервер
    exec /usr/sbin/sshd -D

Dockerfile::

    FROM debian:wheezy

    ENV DEBIAN_FRONTEND noninteractive

    RUN apt-get update

    RUN apt-get install -yq wget

    # установка deadbeef
    RUN wget -P \
     /tmp 'http://sourceforge.net/projects/deadbeef/files/debian/0.6.2/deadbeef-static_0.6.2-2_amd64.deb' \
     && dpkg -i /tmp/deadbeef-static_0.6.2-2_amd64.deb || true \
     && apt-get install -fyq --no-install-recommends \
     && ln -s /opt/deadbeef/bin/deadbeef /usr/local/bin/deadbeef \
     && rm /tmp/deadbeef-static_0.6.2-2_amd64.deb

    # минимальная установка pulseaudio
    RUN apt-get install -yq --no-install-recommends pulseaudio

    RUN apt-get install -yq \
          pwgen \
          openssh-server

    # создаем директорию необходимую для запуска ssh-сервера
    RUN mkdir -p /var/run/sshd

    ADD entrypoint.sh /entrypoint.sh
    RUN chmod +x /entrypoint.sh

    EXPOSE 22

    ENTRYPOINT ["/entrypoint.sh"]

Порт и адрес pulse-сервера будем передавать в качестве параметров при
запуске контейнера, чтобы иметь возможность поменять их на нестандартные
(не забыв о значениях по умолчанию).

Сборка образа::

    $ docker build -t deadbeef:ssh .

Запуск контейнера с указатанием хоста PulseAudio (порт стандартный)::

    $ docker run -d -p 2222:22 -e PA_HOST="172.17.42.1" --name=dead_player deadbeef:ssh

Узнать пароль пользователя для подключения можно с помощью команды ``docker logs``::

    $ docker logs dead_player
    User: dockerx
    Password: vai0ay7OuNga

Для подключения по ssh можно использовать адрес как самого
контейнера, так и адрес docker-хоста (при этом порт подключения будет
отличаться от стандартного 22-го; в данном случае это будет 2222 — тот,
который указан при запуске контейнера). Узнать IP контейнера можно с
помощью команды ``docker inspect``::

    $ docker inspect --format '{{ .NetworkSettings.IPAddress }}' dead_player
    172.17.0.69

Подключение к контейнеру::

    $ ssh -X dockerx@172.17.0.69

Или через docker-шлюз::

    $ ssh -X -p 2222 dockerx@172.17.42.1

Наконец, после авторизации, можно послушать музыку::

    dockerx@5e3add235060:~$ deadbeef

----

Subuser
-------

`Subuser <http://subuser.org/>`__ позволяет запускать программы в
изолированных контейнерах docker, беря на себя всю работу, связанную с
созданием и настройкой контейнеров, поэтому пользоваться им могут даже
люди, ничего не знающие о docker. Во всяком случае, такова идея проекта.
Для каждого приложения-контейнера при этом устанавливаются ограничения в
зависимости от его назначения — ограниченный доступ к директориям хоста,
к сети, звуковым устройствам и т.д. По сути, subuser реализует удобную
обертку над описанным здесь первым способом запуска графических
приложений, так как запуск осуществляется с помощью монтирования нужных
директорий, устройств и т.д.

К каждому создаваемому образу прилагается файл permissions.json,
который определяет настройки доступа для приложения. Так, например,
выглядит этот файл для образа vim::

    {
      "description"                : "Simple universal text editor."
      ,"maintainer"                : "Timothy Hobbs <timothyhobbs (at) seznam dot cz>"
      // Путь к исполняемому файлу внутри контейнера
      ,"executable"                : "/usr/bin/vim"
      // Список директорий, к которым программа должна иметь доступ с правами на чтение/запись
      // Пути задаются относительно вашей домашней директории. Например: "Downloads" преобразуется в "$HOME/Downloads"
      ,"user-dirs"                 : [ 'Downloads', 'Documents' ]  // По умолчанию: []
      // Разрешить программе создавать X11 окна
      ,"x11"                       : true       // По умолчанию: false
      // Разрешить программе использовать вашу звуковую карту и устройства записи
      ,"sound-card"                : true       // По умолчанию: false
      // Дать программе доступ к директории, из которой она была запущена с правами на чтение/запись
      ,"access-working-directory"  : true       // По умолчанию: false
      // Дать программе доступ к интернету
      ,"allow-network-access"      : true       // По умолчанию: false
    }

Subuser имеет
`репозиторий <https://github.com/subuser-security/subuser-default-repository>`__
(на данный момент — небольшой) готовых приложений, список которых можно
увидеть с помощью команды::

    $ subuser list available

Добавить приложение из репозитория можно так::

    $ subuser subuser add firefox-flash firefox-flash@default

Эта команда установит приложение, назвав его firefox-flash,
основанное на одноименном образе из репозитория по умолчанию.

Запуск приложения выглядит так:::

    $ subuser run firefox-flash

Кроме использования стандартного репозитория, можно создавать и
добавлять свои собственные subuser-приложения.

Проект довольно молодой и пока выглядит сыроватым, но свою задачу он
выполняет. Код проекта можно найти на github:
`subuser-security/subuser <https://github.com/subuser-security/subuser>`__

Пример №3
---------

Создадим, subuser-приложение для все того же DeaDBeef. Для
демонстрации создадим локальный репозиторий subuser (ни что иное как
git-репозиторий). Директория ``~/.subuser-repo`` сойдет. В ней следует
инициализировать git-репозиторий::

    $ mkdir ~/.subuser-repo
    $ cd ~/.subuser-repo
    $ git init

Здесь же создадим директорию для DeaDBeef::

    $ mkdir deadbeef

Структура директории для subuser-образа выглядит следующим образом::

    image-name/
      docker-image/
        SubuserImagefile
        docker-build-context...
      permissions.json

SubuserImagefile — это тот же Dockerfile. Разница лишь в возможности
использовать инструкцию ``FROM-SUBUSER-IMAGE``, которая принимает
идентификатор существующего subuser-образа в качестве аргумента. Список
доступных базовых образов можно посмотреть здесь:
`SubuserBaseImages <https://github.com/subuser-security/subuser-examples/tree/master/SubuserBaseImages>`__.

Итак, подготовив структуру каталога, остается создать два файла:
SubuserImagefile и permissions.json.

SubuserImagefile практически ничем не будет отличаться от
приведенного ранее Dockerfile::

    FROM debian:wheezy

    ENV DEBIAN_FRONTEND noninteractive

    RUN apt-get update

    RUN apt-get install -yq wget

    # установка deadbeef
    RUN wget -P /tmp 'http://sourceforge.net/projects/deadbeef/files/debian/0.6.2/deadbeef-static_0.6.2-2_amd64.deb' \
     && dpkg -i /tmp/deadbeef-static_0.6.2-2_amd64.deb || true \
     && apt-get install -fyq --no-install-recommends \
     && ln -s /opt/deadbeef/bin/deadbeef /usr/local/bin/deadbeef \
     && rm /tmp/deadbeef-static_0.6.2-2_amd64.deb

В permissions.json опишем параметры доступа для нашего плеера. Нам
понадобиться экран, звук и интернет::

    {
     "description": "Ultimate Music Player For GNU/Linux",
     "maintainer": "Humble Me",
     "executable": "/opt/deadbeef/bin/deadbeef",
     "sound-card": true,
     "x11": true,
     "user-dirs": [
      "Music"
     ],
     "allow-network-access": true,
     "as-root": true
    }

Параметр ``as-root`` позволяет запускать приложения в контейнере от
имени root. По умолчанию subuser запускает контейнер с параметром
``--user``, присваивая ему идентификатор текущего пользователя. Но
deadbeef при этом отказывается запускаться (не может создать сокет-файл
в домашней директории, которой не существует).

Закоммитим изменения в нашем импровизированном subuser-репозитории::

    ~/.subuser-repo $ git add . && git commit -m 'initial commit'

Теперь можно установить плеер из локального репозитория::

    $ subuser subuser add deadbeef deadbeef@file:////home/silentvick/.subuser-repo

Наконец, запустить его можно с помощью следующей команды::

    $ subuser run deadbeef

Удаленный рабочий стол
----------------------

Для доступа к удаленному рабочему столу можно использовать: 
`TightVNC <http://www.tightvnc.com/>`__,
`Xpra <https://www.xpra.org/>`__, 
`X2Go <http://wiki.x2go.org>`__ и т.п.

Такой вариант выглядит более накладным, так как требует установки
дополнительного софта — как в контейнере, так и на локальном компьютере.
Но у него есть и преимущества, например:

-  высокая изоляция контейнера, что увеличивает безопасность при запуске
   `подозрительных <https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%BA%D1%80%D1%8B%D1%82%D1%8B%D0%B9_%D0%B8%D1%81%D1%85%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%B4>`__
   программ
-  шифрование траффика
-  оптимизация передачи данных по сети
-  мультиплатформенность
-  а также возможность запуска полноценного графического окружения

Пример №4
---------

В качестве примера воспользуемся X2Go поскольку он поддержывает трансляцию звука. 
Для подключения к X2Go-серверу используется специальная программа ``x2goclient``. 
Подробнее об установке сервера и клиента можно найти на `официальном сайте <http://wiki.x2go.org>`__.

В контейнере кроме x2go-сервера также понадобится ssh-сервер. Поэтому
код будет во многом похож на приведенный в примере №2. В той части, где
ставится плеер, openssh-сервер, и сервер pulseaudio. То есть останется
только добавить x2go-сервер и openbox:

Dockerfile::

    FROM debian:wheezy
    ENV DEBIAN_FRONTEND noninteractive
    RUN apt-get update
    RUN apt-get install -yq wget

    # установка deadbeef
    RUN wget -P /tmp 'http://sourceforge.net/projects/deadbeef/files/debian/0.6.2/deadbeef-static_0.6.2-2_amd64.deb' \
     && dpkg -i /tmp/deadbeef-static_0.6.2-2_amd64.deb || true \
     && apt-get install -fyq --no-install-recommends \
     && ln -s /opt/deadbeef/bin/deadbeef /usr/local/bin/deadbeef \
     && rm /tmp/deadbeef-static_0.6.2-2_amd64.deb

    # минимальная установка pulseaudio
    RUN apt-get install -yq --no-install-recommends pulseaudio

    # устанавливаем ssh-сервер и утилиту pwgen для генерации пароля
    RUN apt-get install -yq \
          pwgen \
          openssh-server

    # создаем директорию, необходимую для запуска ssh-сервера
    RUN mkdir -p /var/run/sshd

    # установка x2go-сервера
    RUN echo 'deb http://packages.x2go.org/debian wheezy main' >> /etc/apt/sources.list \
     && echo 'deb-src http://packages.x2go.org/debian wheezy main' >> /etc/apt/sources.list \
     && apt-key adv --recv-keys --keyserver keys.gnupg.net E1F958385BFE2B6E \
     && apt-get update && apt-get install -yq x2go-keyring \
     && apt-get update && apt-get install -yq \
          x2goserver \
          x2goserver-xsession

    # установка openbox
    RUN apt-get install -yq openbox

    # Добавляем пункт запуска DeaDBeeF в меню OpenBox.
    #
    # Для демонстрации сгодится и такой прямолинейный метод,
    # но, в общем случае, лучше создать свой файл menu.xml и добавлять его через ADD
    RUN sed -i '/<.*id="root-menu".*>/a <item label="DeaDBeeF"><action name="Execute"><execute>deadbeef</execute></action></item>' \
          /etc/xdg/openbox/menu.xml

    ADD entrypoint.sh /entrypoint.sh
    RUN chmod +x /entrypoint.sh

    EXPOSE 22

    ENTRYPOINT ["/entrypoint.sh"]

Скрипт entrypoint.sh::

    #!/bin/bash

    # имя пользователя для подключения
    DOCKER_USER=dockerx
    X2GO_GROUP=x2gouser

    # генерируем пароль пользователя
    DOCKER_PASSWORD=$(pwgen -c -n -1 12)
    DOCKER_ENCRYPTED_PASSWORD=$(perl -e 'print crypt('"$DOCKER_PASSWORD"', "aa")')

    # выводим имя и пароль пользователя,
    # чтобы их можно было увидеть с помощью docker logs
    echo User: $DOCKER_USER
    echo Password: $DOCKER_PASSWORD

    # создаем пользователя
    useradd --create-home \
            --home-dir /home/$DOCKER_USER \
            --password $DOCKER_ENCRYPTED_PASSWORD \
            --shell /bin/bash \
            --groups $X2GO_GROUP \
            --user-group $DOCKER_USER

    # запускаем ssh-сервер
    exec /usr/sbin/sshd -D

Соберем образ::

    $ docker build -t deadbeef:x2go .

Запустим контейнер в режиме демона::

    $ docker run -d -p 2222:22 --name=dead_player deadbeef:x2go

Теперь, когда контейнер работает, к нему можно подключиться с помощью
x2goclient. 
В настройках подключения в качестве хоста следует указать адрес либо
самого контейнера, либо docker-хоста (в этом случае стоит также учесть
нестандартный порт подключения ssh). Узнать логин и пароль для
авторизации можно с помощью команды ``docker logs``, как
показано в примере №2. Для запуска openbox-сесcии в настройках «Session
type» следует выбрать «Custom desktop» и в поле «Command» прописать
«openbox-session».

Добавлю еще, что X2Go позволяет запускать одиночные приложения, так,
как если бы они запускались на локальной машине. Для этого надо в
клиенте в настройках «Session type» выбрать «Single application» и в
«Command» прописать путь к исполняемому файлу. При этом в контейнере
даже нет необходимости устанавливать графическую среду — достаточно
иметь X2Go-сервер и желаемое приложение.
