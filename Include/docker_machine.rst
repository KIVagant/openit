.. docker-machine: Управление виртуальными машинами
   ================================================
   :tags: docker-machine,VirtualBox,Mac OS,MS Windows,CoreOS,rancherOS

   .. contents:: Содержание:

   .. PELICAN_END_SUMMARY

Установка
---------

Docker Toolbox для Mac OS и MS Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Для ``Mac OS`` и ``MS Windows`` утилита ``docker-machine`` входит в состав пакета ``Docker Toolbox``.

Ниже приведен конспект инструкции с официальной `страницы <https://docs.docker.com/machine/install-machine/>`__ проекта.

GNU/Linux, Mac OS
^^^^^^^^^^^^^^^^^
::

  $ sudo curl -L https://github.com/docker/machine/releases/download/<ВЕРСИЯ>/docker-machine-`uname -s`-`uname -m` > \
  /usr/local/bin/docker-machine
  $ sudo chmod +x /usr/local/bin/docker-machine

MS Windows
^^^^^^^^^^
::

  $ if [[ ! -d "$HOME/bin" ]]; then mkdir -p "$HOME/bin"; fi && \
  curl -L https://github.com/docker/machine/releases/download/<ВЕРСИЯ>/docker-machine-Windows-x86_64.exe > "$HOME/bin/docker-machine.exe" && \
  chmod +x "$HOME/bin/docker-machine.exe"

Проверка
--------
::

  $ docker-machine version
  docker-machine version X.X.X, build xxxxxxx

Основные команды
----------------
Остановка
^^^^^^^^^
::

  docker-machine stop <ИМЯ_ВМ>

Запуск
^^^^^^
::

  docker-machine start <ИМЯ_ВМ>

Рестарт
^^^^^^^
::

  docker-machine restart <ИМЯ_ВМ>

Список
^^^^^^
::

  docker-machine ls

Удаление
^^^^^^^^
::

  docker-machine rm <ИМЯ_ВМ>

Обновление
^^^^^^^^^^
::

  docker-machine upgrade <ИМЯ_ВМ>

Соединение по SSH
^^^^^^^^^^^^^^^^^
::

  docker-machine ssh <ИМЯ_ВМ>

Определение IP адреса
^^^^^^^^^^^^^^^^^^^^^
::

  docker-machine ip <ИМЯ_ВМ>

Активизация ВМ для Docker
^^^^^^^^^^^^^^^^^^^^^^^^^
Для каждого окна терминала (сессии) одну из запущенных ВМ можно сделать активной.
Отличие активной ВМ от остальных: команда ``docker`` выполненная в окне терминала хост-системы передается в активную ВМ.

.. note::
   Можно открыть несколько терминалов в каждом из которых активизировать свою ВМ.
   Это может быть удобно для тестирования Swarm режима (роя контейнеров). 

Определить активную ВМ для текущего терминала можно по колонке ``ACTIVE`` в выводе команды::

    $ docker-machine ls
    NAME      ACTIVE   DRIVER       STATE     URL                         SWARM
    <ИМЯ_ВМ>           virtualbox   Running   tcp://192.168.99.101:2376

Если ВМ активна то в этой колонке должен стоять символ ``*``. 

Чтобы сделать ВМ активной нужно установить соответствующие ей переменные окружения. 
Просмотреть эти переменные можно командой::

    docker-machine env <ИМЯ_ВМ>
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_HOST="tcp://192.168.99.101:2376"
    export DOCKER_CERT_PATH="/Users/b2v/.docker/machine/machines/<ИМЯ_ВМ>"
    export DOCKER_MACHINE_NAME="<ИМЯ_ВМ>"
    # Run this command to configure your shell: 
    # eval $(docker-machine env <ИМЯ_ВМ>)

Последняя строка вывода содержит команду с помощью которой данную ВМ можно сделать активной::

    $ eval "$(docker-machine env <ИМЯ_ВМ>)"
 
После выполнения рекомендуемой команды в столбце ``ACTIVE`` напротив ВМ появляется звездочка:: 

    $ docker-machine ls
    NAME      ACTIVE   DRIVER       STATE     URL                         SWARM
    <ИМЯ_ВМ>  *        virtualbox   Running   tcp://192.168.99.101:2376

Пример создания ВМ RancherOS с Docker
-------------------------------------
::

  docker-machine create -d virtualbox \
  --virtualbox-boot2docker-url https://releases.rancher.com/os/latest/rancheros.iso \
  rancheros

После создания, виртуальная машина запущена и готова к работе::

    $ docker-machine ls
	NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
	rancheros   -        virtualbox   Running   tcp://192.168.99.100:2376           v1.12.6       

Пример создания ВМ CoreOS с Docker
----------------------------------
::

  docker-machine create -d virtualbox testenv

После создания, виртуальная машина запущена и готова к работе::

    $ docker-machine ls
	NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
	rancheros   -        virtualbox   Running   tcp://192.168.99.100:2376           v1.12.6       
	testenv     -        virtualbox   Running   tcp://192.168.99.101:2376           v17.03.0-ce   

Активизируем ВМ для текущей сессии::

    $ eval "$(docker-machine env testenv)"
 
Проверяем:: 

    $ docker-machine ls
	NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
	rancheros   -        virtualbox   Running   tcp://192.168.99.100:2376           v1.12.6       
	testenv     *        virtualbox   Running   tcp://192.168.99.101:2376           v17.03.0-ce   

Соединение с ВМ
^^^^^^^^^^^^^^^
::

    $ docker-machine ssh testenv
	                        ##         .
	                  ## ## ##        ==
	               ## ## ## ## ##    ===
	           /"""""""""""""""""\___/ ===
	      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
	           \______ o           __/
	             \    \         __/
	              \____\_______/
	 _                 _   ____     _            _
	| |__   ___   ___ | |_|___ \ __| | ___   ___| | _____ _ __
	| '_ \ / _ \ / _ \| __| __) / _` |/ _ \ / __| |/ / _ \ '__|
	| |_) | (_) | (_) | |_ / __/ (_| | (_) | (__|   <  __/ |
	|_.__/ \___/ \___/ \__|_____\__,_|\___/ \___|_|\_\___|_|
	Boot2Docker version 17.03.0-ce, build HEAD : f11a204 - Thu Mar  2 00:14:47 UTC 2017
	Docker version 17.03.0-ce, build 3a232c8

Доступ к ФС хост-системы из ВМ 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Посмотрим на смонтированные разделы::

    docker@testenv:~$ mount
	tmpfs on / type tmpfs (rw,relatime,size=917696k)
	proc on /proc type proc (rw,relatime)
	sysfs on /sys type sysfs (rw,relatime)
	devpts on /dev/pts type devpts (rw,relatime,mode=600,ptmxmode=000)
	tmpfs on /dev/shm type tmpfs (rw,relatime)
	fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
	/dev/sda1 on /mnt/sda1 type ext4 (rw,relatime,data=ordered)
	cgroup on /sys/fs/cgroup type tmpfs (rw,relatime,mode=755)
	cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,relatime,cpuset)
	cgroup on /sys/fs/cgroup/cpu type cgroup (rw,relatime,cpu)
	cgroup on /sys/fs/cgroup/cpuacct type cgroup (rw,relatime,cpuacct)
	cgroup on /sys/fs/cgroup/blkio type cgroup (rw,relatime,blkio)
	cgroup on /sys/fs/cgroup/memory type cgroup (rw,relatime,memory)
	cgroup on /sys/fs/cgroup/devices type cgroup (rw,relatime,devices)
	cgroup on /sys/fs/cgroup/freezer type cgroup (rw,relatime,freezer)
	cgroup on /sys/fs/cgroup/net_cls type cgroup (rw,relatime,net_cls)
	cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,relatime,perf_event)
	cgroup on /sys/fs/cgroup/net_prio type cgroup (rw,relatime,net_prio)
	cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,relatime,hugetlb)
	cgroup on /sys/fs/cgroup/pids type cgroup (rw,relatime,pids)

::

	Users on /Users type vboxsf (rw,nodev,relatime)

::

	/dev/sda1 on /mnt/sda1/var/lib/docker/aufs type ext4 (rw,relatime,data=ordered)
    docker@testenv:~$

В ВМ смонтирован каталог ``/Users`` (аналог ``/home`` в Linux). 
Это позволяет прозрачно работать с файлами каталога ``/Users`` на host-системе. 

.. note::
   В данном случае используется ``Mac OS``. В MS Windows монтируются другие каталоги.
