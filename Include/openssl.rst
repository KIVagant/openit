.. ================================
   Сертификаты и шифрование SSL/TLS
   ================================

Использование SSL сертификата в Apache
======================================

Создание **самоподписанного** сертификата (/etc/pki/tls/certs/apache.crt) и соответствующего ему ключа для сервера (/etc/pki/tls/private/apache.key) на год (365 дней)::

  openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
          -keyout /etc/pki/tls/private/apache.key \
          -out /etc/pki/tls/certs/apache.crt

Диалог при созданиии выглядит следующим образом::

  Country Name (2 letter code) [AU]:US
  State or Province Name (full name) [Some-State]:New York
  Locality Name (eg, city) []:NYC
  Organization Name (eg, company) [Internet Widgits Pty Ltd]:Awesome Inc
  Organizational Unit Name (eg, section) []:Dept of Merriment
  Common Name (e.g. server FQDN or YOUR name) []:example.com                  
  Email Address []:[email protected]
  Step Four—Set Up the Certificate

.. note::
  Самый важный параметр "Common Name". Он должен соответствовать имени домена закрепленного за IP адресом.

.. note::
  Для присвоения пустого значения вводится ``.``

Установка модуля для apache::

  yum install mod_ssl

Изменение конфигурационного файла::

  /etc/httpd/conf.d/ssl.conf

Найти секцию ``<VirtualHost _default_:443>`` и внести следующие изменеия::

  ServerName example.com:443
  SSLEngine on
  SSLCertificateFile /etc/pki/tls/certs/apache.crt
  SSLCertificateKeyFile /etc/pki/tls/private/apache.key 

Перезапустить службу::

  /etc/init.d/httpd restart

Шифрование TLS для Postfix
==========================
Само-подписанный сертификат создается командой::

  openssl req -new -x509 -days 365 -nodes \
          -keyout /etc/ssl/private/postfixkey.pem \
          -out /etc/ssl/certs/postfixcert.pem

Приведенная выше команда создает новый сертификат X.509 который будет действовать в течение 365 дней. 
Дополнительный параметр -nodes указывает, что закрытый ключ не должен быть зашифрован. 
Результирующий файл сертификата будет сохранен как::

  /etc/ssl/certs/postfixcert.pem
 
а файл с ключом как::

  /etc/ssl/private/postfixkey.pem.

Во время выполнения команды необходимо ввести ряд праметров::

  Country Name (2 letter code) [AU]:RU 
  State or Province Name (full name) [Some-State]:Russia 
  Locality Name (eg, city) []:SPb 
  Organization Name (eg, company) [Internet Widgits Pty Ltd]:Company
  Organizational Unit Name (eg, section) []:Deportament
  Example.tst Common Name (e.g. server FQDN or YOUR name) []:mail.example.com 
  Email Address []:sarmed@example.com 

Затем, в конфигурационном файле postfix::

  /etc/postfix/main.cf 

добавляются/изменяются параметры::
  
  ### STARTTLS is enabled ###
  smtpd_tls_security_level = may 
  smtpd_tls_received_header = yes 
  smtpd_tls_auth_only = yes 
  
  ### loglevel 3 should be used while troubleshooting ###
  smtpd_tls_loglevel = 1
  
  ### path to certificate and key file
  smtpd_tls_cert_file = /etc/ssl/certs/postfixcert.pem 
  smtpd_tls_key_file = /etc/ssl/private/postfixkey.pem 
  smtpd_use_tls=yes 

В заключении, для того, чтобы включить TLS, перезапускается сервис postfix::

  service postfix restart 

Шифрование SSL для Dovecot
==========================

Создается само-подписанный сертификат::

  openssl req -new -x509 -days 365 -nodes \
          -keyout /etc/ssl/private/dovecotkey.pem \
          -out /etc/ssl/certs/dovecotcert.pem 

Приведенная выше команда создает новый сертификат X.509, который действителен в течение 365 дней. Параметр -nodes является необязательным параметром, который определяет, что хранимый секретный ключ не должен быть зашифрован. 

Результирующим файлом сертификата будет файл::

  /etc/ssl/certs/dovecotcert.pem

а выходным файлом с ключом будет файл::

  /etc/ssl/private/dovecotkey.pem

Во время выполнения команды необходимо ввести ряд праметров::

  Country Name (2 letter code) [AU]:RU 
  State or Province Name (full name) [Some-State]:Russia 
  Locality Name (eg, city) []:SPb 
  Organization Name (eg, company) [Internet Widgits Pty Ltd]:Company
  Organizational Unit Name (eg, section) []:Deportament
  Example.tst Common Name (e.g. server FQDN or YOUR name) []:mail.example.com 
  Email Address []:sarmed@example.com 

Затем в конфигурационном файле dovecot::

  /etc/dovecot/conf.d/10-ssl.conf

добавляются/изменяются параметры::

  ssl_cert = /etc/ssl/certs/dovecot
  cert.pemssl_key = /etc/ssl/private/dovecotkey.pem

Процедуру можно значительно упростить, если воспользоваться скриптом::

  /usr/share/doc/dovecot-<версия>/mkcert.sh

Для корректной работы последнего нужно:

- отредактировать

::

  /etc/pki/dovecot/dovecot-openssl.conf

- переименовать или удалить

::

  /etc/pki/dovecot/certs/dovecot.pem
  /etc/pki/dovecot/private/dovecot.pem

В заключении, для того, чтобы включить SSL, перезапускается сервис dovecot::

  service dovecot restart

Настройка почтового клиента
===========================

Установить параметры в соответствии с таблицей:

======== ==== ======== ====
Протокол Порт SSL      Аутентификация
======== ==== ======== ====
IMAP     993  SSL/TLS  Normal password
SMTP     25   STARTTLS No authentication
======== ==== ======== ====

Экспорт в формат `PKCS#12`
==========================

::

  openssl pkcs12 -export \
          -name "Описание" \
          -out <имя>.p12 \
          -in /etc/pki/tls/certs/<имя>.crt \
          -inkey /etc/pki/tls/private/<имя>.key

Формат PKCS#12 используется многими приложениями.

Шифрование сообщений и файлов с помощью OpenSSL
===============================================
C помощью OpenSSL можно:

- создавать приватный ключ RSA или CSR (Certificate Signing Request);
- тестировать производительность компьютера;
- шифровать файлы и сообщения.

Описанные ниже команды предсталяет собой только введение в возможности OpenSSL. Узнать больше о различных методах шифрования, используемых OpenSSL, можно в руководстве::

   man openssl

Шифровка и дешифровка сообщений
-------------------------------
Команда::

   echo "Привет мир" | openssl enc -base64

шифрует методом **Base64** строку символов "Привет мир" и выводит результат на терминал::

   0J/RgNC40LLQtdGCINC80LjRgAo=

Для расшифровки используется та же команда с опцией ``-d``::

   echo "0J/RgNC40LLQtdGCINC80LjRgAo=" | openssl enc -base64 -d

выводящая результат на терминал::

   Привет мир

Описанные выше команды не позволяют задать пароля для расшифровки. 
Для создания зашифрованного сообщения с паролем используется команда::

   echo "OpenSSL" | openssl enc -aes-256-cbc

предлагающая ввести пароль для дальнейшей расшифровки::

   enter aes-256-cbc encryption password:
   Verifying - enter aes-256-cbc encryption password:

Результат шифрования выводится на терминал::

   U2FsdGVkX185E3H2me2D+qmCfkEsXDTn8nCn/4sblr8=

Для записи зашифрованного сообщения в файл, нужно перенаправить стандартный поток вывода с помощью ``>``::

   echo "OpenSSL" | openssl enc -aes-256-cbc > openssl.dat

Команда::

   openssl enc -aes-256-cbc -d -in openssl.dat 

используется для расшифровки файла openssl.dat. 

Шифровка и дешифровка файлов
----------------------------
Для шифрования файлов используется опция `-in`, которая задает исходный файл для шифрования, и опция `-out`, которая задает выходной, зашифрованный файл::

   openssl enc -aes-256-cbc -in /etc/services -out services.dat

При расшифровке используется опция ``-d``::

   openssl enc -aes-256-cbc -d -in services.dat

Шифровка и дешифровка каталогов
-------------------------------
Если необходимо зашифровать каталог, сначала необходимо его "затарить" (сохранить в виде файла с помощью команды tar), а затем зашифровать полученный файл::

   tar c /etc | openssl enc -aes-256-cbc -out etc.tar.gz.dat

Для дешифровки и извлечения содержимого в текущий каталог используется команда::

   openssl enc -aes-256-cbc -d -in etc.tar.gz.dat | tar x

Описанные команды можно использовать для организации автоматического резервная с шифрованием резервных копий.
