.. Кластерная файловая система Oracle (OCFS2)
   ==========================================

.. note::
   "Из коробки" работает только с ядром UEK от Oracle Linux

.. note::
   Работает на OL 6.3

.. note::
   На OL 6.7 не работает

Oracle Cluster Filesystem 2 (OCFS2) — файловая система, предназначенная для совместного использования двумя или более системами.

Файловая система может быть использована как обычная локальная файловая система, поскольку не требует никакой специальной модификации программного обеспечения, использующего её.

В настоящий момент файловая система OCFS2 используется в продукте компании Oracle — Real Application Cluster. Кроме этого, её часто применяют при построении масштабируемых Web-серверов, файловых, почтовых систем, а также для хранения образов виртуальных машин.

Файловая система работает поверх разделяемого хранилища, доступ к которому может осуществляться при помощи таких протоколов как iSCSI,AoE или DRBD.

Возможности файловой системы OCFS2:

-  Блоки переменного размера
-  Гибкое выделение пространства (экстенты, разреженные файлы (с версии 2.6.22), незаписанные экстенты с возможностью создания дырок)
-  Журналирование (поддерживаются режимы ordered и writeback)
-  В системах различных аппаратных платформ работает одинаково (x86, x86_64, ia64 и ppc64)
-  Поддержка встроенного Clusterstack с распределённой системой управления блокировками (Distributed Lock Manager)
-  Поддержка буферизованного, прямого, асинхронного, `splice() <http://en.wikipedia.org/wiki/Splice_%28system_call%29>`_ ввода/вывода, а также возможность отображения памяти на файловую систему (Memory Mapped I/Os)
-  Разнообразные утилиты, обеспечивающий всестороннюю поддержку файловой системы.

Установка::

  yum install ocfs2-tools 

.. note::
   Устанавливается клиентская часть X-сервера для запуска qt-приложений 

Программы

=========== ====
mkfs.ocfs2  создание файловой системы
mount.ocfs2 монтирование
fsck.ocfs2  проверка
o2cb        запуск, остановка, настройка кластера
=========== ====

Настройка

Firewall::

  iptables -I INPUT -s subnet_addr/prefix_length -p tcp \
           -m state --state NEW -m tcp --dport 7777 -j ACCEPT
  iptables -I INPUT -s subnet_addr/prefix_length -p udp \
           -m udp --dport 7777 -j ACCEPT
  service iptables save


Создание файла настроек с помощью команды o2cb::

  o2cb add-cluster <ИМЯ_КЛАСТЕРА> 

Команда создает файл /etc/ocfs2/cluster.conf (если файл не существует) с описанием кластера.

Добавление узла::

  o2cb add-node <ИМЯ_КЛАСТЕРА> <ИМЯ_УЗЛА> --ip <IP_АДРЕС_УЗЛА>

Имя узла должно совпадать с именем компьютера, заданным в файле /etc/sysconfig/network. 

.. warning::
   Еси **<ИМЯ_УЗЛА> несовпадает** с именем в файле /etc/sysconfig/network, узел будет оставаться в состоянии **Ofline**

В качестве IP адреса желательно использовать адрес в изолированной сети, предназначенной для связи элементов кластера.


.. Добавление механизма "сердцебиения" (heartbeat)ЖЖ

     o2cb add-heartbeat <ИМЯ_КЛАСТЕРА> /dev/<ISCSI_РАЗДЕЛ>
     o2cb heartbeat-mode <ИМЯ_КЛАСТЕРА> global

   .. note::
      You must configure global heartbeat to use whole disk devices. You cannot configure a global heartbeat device on a disk partition.

    For example, to use /dev/sdd, /dev/sdg, and /dev/sdj as global heartbeat devices:

    # o2cb add-heartbeat mycluster /dev/sdd
    # o2cb add-heartbeat mycluster /dev/sdg
    # o2cb add-heartbeat mycluster /dev/sdj
    # o2cb heartbeat-mode mycluster global

    Copy the cluster configuration file /etc/ocfs2/cluster.conf to each node in the cluster.

    .. note::
       Any changes that you make to the cluster configuration file do not take effect until you restart the cluster stack.

Далее нужно клонировать файл /etc/ocfs2/cluster.conf на все узлы кластера.
На всех узлах выполнить команду::

  service o2cb configure

Во время выполнения этой команды нужно ответить на несколько вопросов:

::

  Load O2CB driver on boot (y/n) [n]: y <Enter>

Если ответить ``y``, то при загрузке узла будут загружаться все необходимые модули ядра. В противном случае перед запуском кластера надо выполнить загрузку модулей ядра командой::

  service o2cb load

Выбирать ``n`` имеет смысл в том случае, если все необходимые модули загружаются вместе с ядром.

::

  Cluster stack backing O2CB [o2cb]: <Enter>

Имя системы для поддержки механизмов кластера. По умолчанию встроенная утилита ``o2cb``.

::

  Cluster to start on boot (Enter "none" to clear) [ocfs]: <ИМЯ_КЛАСТЕРА> <Enter>

Нужно ввести имя кластера, выбранное при создании файла ``/etc/ocfs2/cluster.conf``.

Остальные параметры (цитата из документации Oracle):

::

  Specify heartbeat dead threshold (>=7)

  The number of 2-second heartbeats that must elapse without response before a node is considered dead. To calculate the value to enter, divide the required threshold time period by 2 and add 1. For example, to set the threshold time period to 120 seconds, enter a value of 61. The default value is 31, which corresponds to a threshold time period of 60 seconds.

.. note::
   If your system uses multipathed storage, the recommended value is 61 or greater.

::

  Specify network idle timeout in ms (>=5000)
    	

    The time in milliseconds that must elapse before a network connection is considered dead. The default value is 30,000 milliseconds.
    Note

    For bonded network interfaces, the recommended value is 30,000 milliseconds or greater.

    Specify network keepalive delay in ms (>=1000)
    	

    The maximum delay in milliseconds between sending keepalive packets to another node. The default and recommended value is 2,000 milliseconds.

    Specify network reconnect delay in ms (>=2000)
    	

    The minimum delay in milliseconds between reconnection attempts if a network connection goes down. The default and recommended value is 2,000 milliseconds.

    To verify the settings for the cluster stack, enter the service o2cb status command:

    # service o2cb status
    Driver for "configfs": Loaded
    Filesystem "configfs": Mounted
    Stack glue driver: Loaded
    Stack plugin "o2cb": Loaded
    Driver for "ocfs2_dlmfs": Loaded
    Filesystem "ocfs2_dlmfs": Mounted
    Checking O2CB cluster "mycluster": Online
      Heartbeat dead threshold: 61
      Network idle timeout: 30000
      Network keepalive delay: 2000
      Network reconnect delay: 2000
      Heartbeat mode: Local
    Checking O2CB heartbeat: Active

    In this example, the cluster is online and is using local heartbeat mode. If no volumes have been configured, the O2CB heartbeat is shown as Not active rather than Active.

    The next example shows the command output for an online cluster that is using three global heartbeat devices:

    # service o2cb status
    Driver for "configfs": Loaded
    Filesystem "configfs": Mounted
    Stack glue driver: Loaded
    Stack plugin "o2cb": Loaded
    Driver for "ocfs2_dlmfs": Loaded
    Filesystem "ocfs2_dlmfs": Mounted
    Checking O2CB cluster "mycluster": Online
      Heartbeat dead threshold: 61
      Network idle timeout: 30000
      Network keepalive delay: 2000
      Network reconnect delay: 2000
      Heartbeat mode: Global
    Checking O2CB heartbeat: Active
      7DA5015346C245E6A41AA85E2E7EA3CF /dev/sdd
      4F9FBB0D9B6341729F21A8891B9A05BD /dev/sdg
      B423C7EEE9FC426790FC411972C91CC3 /dev/sdj

    Configure the o2cb and ocfs2 services so that they start at boot time after networking is enabled:

    # chkconfig o2cb on
    # chkconfig ocfs2 on

    These settings allow the node to mount OCFS2 volumes automatically when the system starts. 

Проверка состояния обеих узлов::

  service o2cb status
  Driver for "configfs": Loaded
  Filesystem "configfs": Mounted
  Stack glue driver: Loaded
  Stack plugin "o2cb": Loaded
  Driver for "ocfs2_dlmfs": Loaded
  Filesystem "ocfs2_dlmfs": Mounted
  Checking O2CB cluster "lnp": Online
    Heartbeat dead threshold: 31
    Network idle timeout: 30000
    Network keepalive delay: 2000
    Network reconnect delay: 2000
    Heartbeat mode: Local
  Checking O2CB heartbeat: Not active
  Debug file system at /sys/kernel/debug: mounted


При проблемах с ядром (цитата из документации Oracle):

::

  7.2.6 Configuring the Kernel for Cluster Operation

  For the correct operation of the cluster, you must configure the kernel settings shown in the following:

  Specifies the number of seconds after a panic before a system will automatically reset itself.

  If the value is 0, the system hangs, which allows you to collect detailed information about the panic for troubleshooting. This is the default value.

  To enable automatic reset, set a non-zero value. If you require a memory image (vmcore), allow enough time for Kdump to create this image. The suggested value is 30 seconds, although large systems will require a longer time.

  panic_on_oops
	
  Specifies that a system must panic if a kernel oops occurs. If a kernel thread required for cluster operation crashes, the system must reset itself. Otherwise, another node might not be able to tell whether a node is slow to respond or unable to respond, causing cluster operations to hang.

  On each node, enter the following commands to set the recommended values for panic and panic_on_oops::

    sysctl kernel.panic = 30
    sysctl kernel.panic_on_oops = 1

  To make the change persist across reboots, add the following entries to the /etc/sysctl.conf file::

    # Define panic and panic_on_oops for cluster operation
    kernel.panic = 30
    kernel.panic_on_oops = 1


Конфигурационный файл::

  /etc/ocfs2/cluster.conf

Пример для двух узлов::

  node:
    ip_port = 7777
    ip_address = XXX.XXX.XXX.YYY
    number = 0
    name = <ИМЯ_УЗЛА_1>           # имя должно распознаваться (лучше прописать в /etc/hosts)
    cluster = ocfs
  node:
    ip_port = 7777
    ip_address = XXX.XXX.XXX.ZZZ
    number = 1
    name = <ИМЯ_УЗЛА_2>           # имя должно распознаваться (лучше прописать в /etc/hosts)
    cluster = ocfs
  cluster:
    node_count = 2
    name = ocfs

На одном из элементов кластера выполнить форматирование раздела iSCSI::

  mkfs.ocfs2 /dev/<ISCSI_РАЗДЕЛ>

где:

/dev/<ISCSI_РАЗДЕЛ> - ISCSI раздел

Проверить возможность монтирования::

  mount /dev/sdb1 /mnt/sdb1

Прописать монтирование в /etc/fstab::

  /dev/sdb1 /mnt/sdb1 ocfs2  defaults 0 0

Запусить сервис::

  /etc/init.d/o2cb online ocfs2

.. note::
  При запуске служба выдает ошибку:
    mount.ocfs2: Invalid name for a cluster while trying to join the group

.. note::
  После запуска при перезапуске выдает ошибку:
    mount.ocfs2: Insufficient permissions to access cluster service while trying to join the group

Обычное монтирование::

  mount -at ocfs2

выполняется без ошибок и все работает нормально

.. note::
  При перезагрузке (если ocfs смонтированы на 2-х компьютерах) система зависает. Последнее сообщение - размонтирование блочных устройств

Временное решение перечисленных проблем: отключить SELinux

.. note::
  При попытке отформатировать lv под ocfs2 - ошибка
