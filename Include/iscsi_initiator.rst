.. Клиент (initiator)
   ~~~~~~~~~~~~~~~~~~

Имея сетевую карту Gigabit Ethernet клиента можно подключить к сети хранения данных SAN по IP-протоколу. На стороне клиента необходимо выполнить последовательность команд.

Если не установлена утилита iscsi-initiator-utils, установить::

  yum install iscsi-initiator-utils

Определить доступные iSCSI устройства::

  iscsiadm -m discovery -t sendtargets -p [IP-адрес или имя хранилища]

.. Подключить
..
..   iscsiadm -m node -T [идентификатор хранилища] -p [адрес или имя хранилища]

В файле::

  /etc/iscsi/initiatorname.iscsi

отредактировать идентификатор устройства в соответствии с результатами выполнения предыдущей команды::

  InitiatorName=[идентификатор хранилища]

Проверить уровни запуска::

  chkconfig iscsi --list

Если выключена, включить::

  chkconfig iscsi on

Активизировать::

  /etc/init.d/iscsi start

.. В CentOS установленной на LVM при первом подключении использует устройство sda

Просмотреть::

  iscsiadm -m discovery -P 1
  iscsiadm -m node -P 1

.. note::
   Проверка подключения с помощью targetcli

   ::

     targetcli sessions
     alias: node1    sid: 16 type: Normal session-state: LOGGED_IN
     alias: node2    sid: 15 type: Normal session-state: LOGGED_IN
     Verify that the new LUNs are visible on the cluster nodes:

Удалить::

  iscsiadm -m node -o delete -T [идентификатор хранилища] -p [адрес или имя хранилища]

.. note::
  Чисто программная реализация iSCSI значительно снижает производительность. Для снижения вычислительных затрат на создание и обработку SCSI-команд был создан TCP/IP offload engine (TOE). Для достижения наилучшей производительности рекомендуется использовать iSCSI-адаптеры, в которых кроме TOE аппаратно реализован и уровень iSCSI, например, QLogic iSCSI HBAs QLA4010 или Adaptec iSCSI HBAs 7211. Эти адаптеры имеют специальные чипы iSCSI и TCP/IP, что позволяет достигать высоких скоростей обработки пакетов iSCSI при минимальной нагрузке на процессор.

