.. ================================================
   Управление виртуальными машинами с помощью virsh
   ================================================

`Источник <http://docs.fedoraproject.org/ru-RU/Fedora/12/html/Virtualization_Guide/chap-Virtualization_Guide-Managing_guests_with_virsh.html>`_

Утилита ``virsh`` пакета программ ``libvirt`` предназначена для управления системами виртуализации (KVM, XEN, VmWare, OpenVZ, LXC) из командной строки и служит альтернативой графическому менеджеру виртуальных машин ``virt-manager``.

Обычные пользователи могут работать только в режиме просмотра. 

``virsh`` с одной стороны может запускать командную оболочку аналогичную bash, с другой стороны, может управлять виртуальными машинами прямо из командной строки.

Для запуска командной оболочки нужно просто ввести имя команды и нажать <Enter>::

  virsh
  # <команда>
  ...
  # <команда>
  ...
  # quit

Для использования в качестве команды управления виртуальными машинами используется следующий синтаксис::

  virsh <команда> <опции>

Подключение к удаленному гипервизору
====================================
``virsh`` можно использовать как локально так и удаленно. Для подключения к удаленному гипервизору используется следующие форматы команды:

* запуск удаленной командной оболочки:

::

  virsh --connect <адрес_гипервизора>

* удаленное выполнение команд:

::

  virsh --connect <адрес_гипервизора> <команда> <опция>

..  virsh --connect qemu+ssh://<пользователь>@<компьютер>/system list --all
..  root@nuc.home.net's password: 
..   ID    Имя                         Статус
..  ----------------------------------------------------
..   1     F19                            работает
  
Обзор команд
============

Команды управления виртуальными машинами:

================ ====
Команда		 Описание
================ ====
help		 Краткая справка.
list		 Просмотр всех виртуальных машин.
dumpxml		 Вывести файл конфигурации XML для заданной виртуальной машины.
create		 Создать виртуальную машину из файла конфигурации XML и ее запуск.
start		 Запустить неактивную виртуальную машину.
destroy		 Принудительно остановить работу виртуальной машины.
define		 Определяет файл конфигурации XML для заданной виртуальной машины.
domid		 Просмотр идентификатора виртуальной машины.
domuuid		 Просмотр UUID виртуальной машины.
dominfo		 Просмотр сведений о виртуальной машине.
domname		 Просмотр имени виртуальной машины.
domstate	 Просмотр состояния виртуальной машины.
quit		 Закрыть интерактивный терминал.
reboot		 Перезагрузить виртуальную машину.
restore		 Восстановить сохраненную в файле виртуальную машину.
resume		 Возобновить работу приостановленной виртуальной машины.
save		 Сохранить состояние виртуальной машины в файл.
shutdown	 Корректно завершить работу виртуальной машины.
suspend		 Приостановить работу виртуальной машины.
undefine	 Удалить все файлы виртуальной машины.
migrate		 Перенести виртуальную машину на другой узел.
================ ====

Команды управления ресурсами и гипервизором:

================ ====
Команда		 Описание
================ ====
setmem		 Определяет размер выделенной виртуальной машине памяти.
setmaxmem 	 Ограничивает максимально доступный гипервизору объем памяти.
setvcpus 	 Изменяет число предоставленных гостю виртуальных процессоров.
vcpuinfo 	 Просмотр информации о виртуальных процессорах.
vcpupin 	 Настройка соответствий виртуальных процессоров.
domblkstat 	 Просмотр статистики блочных устройств для работающей виртуальной машины.
domifstat 	 Просмотр статистики сетевых интерфейсов для работающей виртуальной машины.
attach-device 	 Подключить определенное в XML-файле устройство к гостю.
attach-disk 	 Подключить новое дисковое устройство к гостю
attach-interface Подключить новый сетевой интерфейс к гостю
detach-device 	 Отключить устройство от гостя (принимает те же определения XML, что и attach-device).
detach-disk 	 Отключить дисковое устройство от гостя.
detach-interface Отключить сетевой интерфейс от гостя.
================ ====

Другие команды:

================ ====
Команда		 Описание
================ ====
version 	 Просмотр версии virsh.
nodeinfo 	 Просмотр информации о гипервизоре.
================ ====

Создание виртуальной машины
===========================

.. rubric :: Файла конфигурации виртуальной машины

Для создания виртуальной машины необходим файл конфигурации в формате XML.

Пример файла конфигурации::

  <domain type='xen' id='13'>
     <name>r5b2-mySQL01</name>
     <uuid>4a4c59a7ee3fc78196e4288f2862f011</uuid>
     <bootloader>/usr/bin/pygrub</bootloader>
     <os>
        <type>linux</type>
        <kernel>/var/lib/libvirt/vmlinuz.2dgnU_</kernel>
        <initrd>/var/lib/libvirt/initrd.UQafMw</initrd>
        <cmdline>ro root=/dev/VolGroup00/LogVol00 rhgb quiet</cmdline>
     </os>
     <memory>512000</memory>
     <vcpu>1</vcpu>
     <on_poweroff>destroy</on_poweroff>
     <on_reboot>restart</on_reboot>
     <on_crash>restart</on_crash>
     <devices>
        <interface type='bridge'>
            <source bridge='xenbr0'/>
            <mac address='00:16:3e:49:1d:11'/>
            <script path='vif-bridge'/>
        </interface>
        <graphics type='vnc' port='5900'/>
        <console tty='/dev/pts/4'/>
     </devices>
  </domain>

Виртуальная машина создается командой::

  virsh define <имя_файла_конфигурации>.xml

.. note::
  Описание виртуальной машины становится доступно в virt-manager

Команда::

  virsh create <имя_файла_конфигурации>.xml

создает и запускает виртуальную машину.

.. note::
  После выключения описание виртуальной машины удаляется из virt-manager

.. # virsh dumpxml {domain-id, domain-name or domain-uuid}
  
   This command outputs the guest's XML configuration file to standard out (stdout). You can save the data by piping the output to a file. An example of piping the output to a file called guest.xml:
  
  # virsh dumpxml GuestID > guest.xml
  
   This file guest.xml can recreate the guest (refer to Редактирование файла конфигурации виртуальной машины. You can edit this XML configuration file to configure additional devices or to deploy additional guests. Refer to Раздел 18.1, «Использование файлов конфигурации с помощью virsh» for more information on modifying files created with virsh dumpxml.

.. rubric :: Редактирование файла конфигурации

.. Instead of using the dumpxml option (refer to Создание XML-файла конфигурации виртуальной машины) guests can be edited either while they run or while they are offline. The virsh edit command provides this functionality. For example, to edit the guest named softwaretesting:

::

  virsh edit <виртуальная_машина>

где <виртуальная_машина> - это имя виртуальной машины или ее идентификатор.

.. {domain-id, domain-name or domain-uuid}

Откроется окно текстового редактора, заданного переменной оболочки $EDITOR (по умолчанию используется ``vi``).

Управление состоянием виртуальной машины
========================================

Приостановка виртуальной машины::

  virsh suspend <виртуальная_машина>

.. When a guest is in a suspended state, it consumes system RAM but not processor resources. Disk and network I/O does not occur while the guest is suspended. This operation is immediate and the guest can be restarted with the resume (Возобновление работы виртуальной машины) option.

Возобновление работы виртуальной машины::

  virsh resume <виртуальная_машина>

Работа машины будет возобновлена немедленно. Параметры будут сохраняться между циклами suspend и resume.

.. Сохранение виртуальной машины::

.. virsh save <виртуальная_машина>

.. This stops the guest you specify and saves the data to a file, which may take some time given the amount of memory in use by your guest. You can restore the state of the guest with the restore (Восстановление виртуальной машины) option. Save is similar to pause, instead of just pausing a guest the present state of the guest is saved.

.. Восстановление виртуальной машины::

..  virsh restore <имя_файла_состояния>

.. Сохраненная машина будет восстановлена из файла и перезапущена, что может занять некоторое время. Имя и идентификатор UUID виртуальной машины останутся неизменными, но будет предоставлен новый идентификатор домена.

Завершение работы виртуальной машины::

  virsh shutdown <виртуальная_машина>

Поведение выключаемого гостя можно контролировать с помощью параметра on_shutdown в его файле конфигурации.

Перезагрузка виртуальной машины::

  virsh reboot <виртуальная_машина>

Поведение перезагружаемого гостя можно контролировать с помощью параметра on_reboot в его файле конфигурации.

Принудительная остановка виртуальной машины::

  virsh destroy <виртуальная_машина>

.. This command does an immediate ungraceful shutdown and stops the specified guest. Using virsh destroy can corrupt guest file systems . Use the destroy option only when the guest is unresponsive. For para-virtualized guests, use the shutdown option(Завершение работы виртуальной машины) instead.

Определение идентификатора домена::

  virsh domid <имя_машина>

Определение имени домена::

  virsh domname {domain-id or domain-uuid}

Определение UUID::

  virsh domuuid {domain-id or domain-name}

Пример вывода virsh domuuid::

  # virsh domuuid r5b2-mySQL01
  4a4c59a7-ee3f-c781-96e4-288f2862f011

Получение информации о виртуальной машине
=========================================

Команда для получения информации::

  virsh dominfo <имя_машина>

Пример вывода virsh dominfo::

  # virsh dominfo r5b2-mySQL01
  id:             13
  name:           r5b2-mysql01
  uuid:           4a4c59a7-ee3f-c781-96e4-288f2862f011
  os type:        linux
  state:          blocked
  cpu(s):         1
  cpu time:       11.0s
  max memory:     512000 kb
  used memory:    512000 kb

Получение информации об узле
============================

Команда получения информации об узле::

  virsh nodeinfo

Пример вывода virsh nodeinfo::

  # virsh nodeinfo
  CPU model                    x86_64
  CPU (s)                      8
  CPU frequency                2895 Mhz
  CPU socket(s)                2      
  Core(s) per socket           2
  Threads per core:            2
  Numa cell(s)                 1
  Memory size:                 1046528 kb

Вывод содержит информацию об узле и машинах, поддерживающих виртуализацию.

Просмотр списка виртуальных машин::

  virsh list

Можно добавить аргументы:

| ``--inactive`` покажет список неактивных доменов (неактивным считается тот домен, который был определен, но в настоящий момент не является активным).
| ``--all`` покажет все виртуальные машины независимо от их состояния. 

Пример::

  # virsh list --all
  Id Name                State
  ----------------------------------
  0 Domain-0             running
  1 Domain202            paused
  2 Domain010            inactive
  3 Domain9600           crashed

Столбец «Status» может содержать следующие значения:

======== ====
running  работающие виртуальные машины, то есть те машины, которые используют ресурсы процессора в момент выполнения команды.
blocked  заблокированные, неработающие машины. Такой статус может быть вызван ожиданием ввода/вывода или пребыванием машины в спящем режиме.
paused   приостановленные домены. В это состояние они переходят, если администратор нажал кнопку паузы в окне менеджера виртуальных машин или выполнил команду xm pause или virsh suspend. В приостановленном состоянии гость продолжает потреблять ресурсы, но не может занимать больше процессорных ресурсов.
shutdown виртуальные машины, завершающие свою работу. При получении виртуальной машиной сигнала завершения работы, она начнет завершать все процессы. Стоит отметить, что некоторые операционные системы не отвечают на такие сигналы.
dying    сбойные домены и домены, которые не смогли корректно завершить свою работу.
crashed  сбойные домены, работа которых была прервана. В этом состоянии домены находятся, если не была настроена их перезагрузка в случае сбоя.
======== ====

Получение информации о виртуальных процессорах
==============================================

Команда получения информации о виртуальных процессорах::

  virsh vcpuinfo {domain-id, domain-name or domain-uuid}

Пример вывода::

  # virsh vcpuinfo r5b2-mySQL01
  VCPU:           0
  CPU:            0
  State:          blocked
  CPU time:       0.0s
  CPU Affinity:   yy

Настройка соответствий виртуальных процессоров::

  virsh vcpupin {domain-id, domain-name or domain-uuid} vcpu, список_cpu

где:

| vcpu -- номер виртуального процессора, 
| список_cpu — сопоставляемые ему физические процессоры.

Изменение числа виртуальных процессоров::

  virsh setvcpus {domain-name, domain-id or domain-uuid} count

.. note::

  Заданное число не может превышать значение, определенное при создании ВМ.

Изменение выделенного объема памяти::

  virsh setmem {domain-id or domain-name} count

Объем памяти, определяемый заданным числом, должен быть указан в килобайтах. 

.. note::

  Объем не может превышать значение, определенное при создании ВМ, но в то же время не должен быть меньше 64 мегабайт. 

Изменение максимального объема памяти может оказать влияние на функциональность только в том случае, если указанный размер меньше исходного.

Получение информации о блочных устройствах работающей виртуальной машины::

  virsh domblkstat GuestName block-device

Получение информации о сетевых устройствах работающей виртуальной машины::

  virsh domifstat GuestName interface-device 

Миграция виртуальных машин
==========================

virsh позволяет переносить виртуальные машины с одного узла на другой. Для выполнения живой миграции просто нужно указать параметр --live. 

Команда переноса выглядит так::

  virsh migrate --live GuestName DestinationURL

Параметр --live не является обязательным.

.. The GuestName parameter represents the name of the guest which you want to migrate.
.. The DestinationURL parameter is the URL or hostname of the destination system. The destination system must run the same version of Fedora, be using the same hypervisor and have libvirt running.
.. Once the command is entered you will be prompted for the root password of the destination system.

Управление виртуальными сетями
==============================
В этой секции будет рассмотрены управляющие команды virsh. Например, команда просмотра списка виртуальных сетей выглядит так::

  virsh net-list

Пример вывода этой команды::

  # virsh net-list
  Name               State      Autostart
  -----------------------------------------
  default            active     yes      
  vnet1	             active     yes      
  vnet2	             active     yes

Просмотр информации для заданной виртуальной сети::

  virsh net-dumpxml NetworkName

Пример вывода этой команды (в формате XML)::

  # virsh net-dumpxml vnet1
  <network>
    <name>vnet1</name>
    <uuid>98361b46-1581-acb7-1643-85a412626e70</uuid>
    <forward dev='eth0'/>
    <bridge name='vnet0' stp='on' forwardDelay='0' />
    <ip address='192.168.100.1' netmask='255.255.255.0'>
      <dhcp>
        <range start='192.168.100.128' end='192.168.100.254' />
      </dhcp>
    </ip>
  </network>

Другие команды управления виртуальными сетями:

============================== ====
virsh net-autostart <имя_сети> перевести сеть в режим автоматической активизации при запуске сервиса ``libvirt``
virsh net-create <файл_XML>    создание и запуск новой сети на основе существующего XML-файла
virsh net-define <файл_XML>    создание (без активизации) новой сети на основе файла конфигурации
virsh net-destroy <имя_сети>   деактивизация заданной сети
virsh net-name <UUID_сети>     определение имени сети по идентификатору
virsh net-uuid <имя_сети>      преобразование заданного имени в идентификатор UUID
virsh net-start <имя_сети>     активизация неактивной сети
virsh net-undefine <имя_сети>  удаление неактивной сети
============================== ====

Для замены виртуальной сети ее нужно сначала удалить::

  virsh net-destroy <имя_сети>
  virsh net-undefine <имя_сети>

а затем определить заново::

  virsh net-define <файл_XML>
  virsh net-start <имя_сети>
  net-autostart <имя_сети>

Виртуальная сеть типа bridge
----------------------------

::

                    +--ethA
                    |   ...
  host ethY-----brY---ethN vm
                    |   ...
                    +--ethX

Позволяет использовать IP-адрес из подсети host.

::

  <network>
        <name>host-bridge</name>
        <forward mode="bridge"/>
        <bridge name="br0"/>
  </network>

.. note::
  Сетевой мост br0 должен существовать.

.. note::
  В VirtualBox не передает пакеты на host при использовании "Сетевого моста". NAT работает нормально.

Виртуальная сеть типа route
---------------------------

::

                    +--ethA
                    |   ...
  host ethY---virbrY---ethN vm
                    |   ...
                    +--ethX

XML-файл::

  <network>
    <name>default</name>
    <forward mode='route' dev='eth0' />
    <bridge name='virbr0' />
    <mac address='52:54:00:37:9B:28'/>
    <domain name='example.com' />
	<dns>
	  <host>
	    <hostname>vm00</hostname>
	  </host>
	</dns>
	<ip address='192.168.122.1' netmask='255.255.255.0'>
      <dhcp>
        <bootp file='pxelinux.0' />
	    <range start='192.168.122.100' end='192.168.122.254' />
        <host mac='xx:xx:xx:xx' name='vm00' ip='192.168.122.10' />
	  </dhcp>
    </ip>
  </network>

Изменения в iptables::

  *mangle
  :PREROUTING ACCEPT [32:5042]
  :INPUT ACCEPT [25:4454]
  :FORWARD ACCEPT [12:2228]
  :OUTPUT ACCEPT [22:2764]
  :POSTROUTING ACCEPT [34:4992]
  -A POSTROUTING -o virbr0 -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill 
  COMMIT
  
  *filter
  
  ...
  
  :OUTPUT ACCEPT [22:2764]
  -A INPUT -i virbr0 -p udp -m udp --dport 53 -j ACCEPT 
  -A INPUT -i virbr0 -p tcp -m tcp --dport 53 -j ACCEPT 
  -A INPUT -i virbr0 -p udp -m udp --dport 67 -j ACCEPT 
  -A INPUT -i virbr0 -p tcp -m tcp --dport 67 -j ACCEPT 
  
  ...
  
  -A FORWARD -d 192.168.122.0/24 -i eth0 -o virbr0 -j ACCEPT 
  -A FORWARD -s 192.168.122.0/24 -i virbr0 -o eth0 -j ACCEPT 
  -A FORWARD -i virbr0 -o virbr0 -j ACCEPT 
  -A FORWARD -o virbr0 -j REJECT --reject-with icmp-port-unreachable 
  -A FORWARD -i virbr0 -j REJECT --reject-with icmp-port-unreachable 
  
  ...

.. note::
  Для доступа в интернет из контейнера необходимо добавить в таблицу `nat` правило::
  
    -A POSTROUTING -s 192.168.122.0/24 -o eth0 -j SNAT --to-source 10.0.2.15
  

Виртуальная сеть типа nat
-------------------------

XML-файл::

  <network>
    <name>nat</name>
    <forward mode='nat'>
      <nat>
	    <port start='1024' end='65535' />
	  </nat>
    </forward>
	<bridge name='virbr0' />
    <mac address='52:54:00:37:9B:29'/>
    <ip address='192.168.123.1' netmask='255.255.255.0'>
      <dhcp>
        <range start='192.168.123.100' end='192.168.123.254' />
      </dhcp>
    </ip>
  </network>

Изменения в iptables::

  *nat
  :PREROUTING ACCEPT [0:0]
  :INPUT ACCEPT [0:0]
  :OUTPUT ACCEPT [6:432]
  :POSTROUTING ACCEPT [6:432]
  -A POSTROUTING -s 192.168.123.0/24 ! -d 192.168.123.0/24 -p tcp -j MASQUERADE --to-ports 1024-65535 
  -A POSTROUTING -s 192.168.123.0/24 ! -d 192.168.123.0/24 -p udp -j MASQUERADE --to-ports 1024-65535 
  -A POSTROUTING -s 192.168.123.0/24 ! -d 192.168.123.0/24 -j MASQUERADE 
  COMMIT
  
  *mangle
  :PREROUTING ACCEPT [6:482]
  :INPUT ACCEPT [6:482]
  :FORWARD ACCEPT [0:0]
  :OUTPUT ACCEPT [6:432]
  :POSTROUTING ACCEPT [6:432]
  -A POSTROUTING -o virbr0 -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill 
  COMMIT
  
  *filter
  
  ...
  
  :OUTPUT ACCEPT [6:432]
  -A INPUT -i virbr0 -p udp -m udp --dport 53 -j ACCEPT 
  -A INPUT -i virbr0 -p tcp -m tcp --dport 53 -j ACCEPT 
  -A INPUT -i virbr0 -p udp -m udp --dport 67 -j ACCEPT 
  -A INPUT -i virbr0 -p tcp -m tcp --dport 67 -j ACCEPT 
  
  ...
  
  -A FORWARD -d 192.168.123.0/24 -o virbr0 -m state --state RELATED,ESTABLISHED -j ACCEPT 
  -A FORWARD -s 192.168.123.0/24 -i virbr0 -j ACCEPT 
  -A FORWARD -i virbr0 -o virbr0 -j ACCEPT 
  -A FORWARD -o virbr0 -j REJECT --reject-with icmp-port-unreachable 
  -A FORWARD -i virbr0 -j REJECT --reject-with icmp-port-unreachable 

Spice
=====
============ ====
Spice-сервер обеспечивает удаленный доступ к экрану и не только. Поддерживает виртуальную видеокарту QXL
Spice-клиент реализует удаленный доступ
Spice-агент  автоматизирует настройку связи виртуальной и реальной машины, ставиться на виртуальной машине
============ ====