.. scsi-target-utils
   ^^^^^^^^^^^^^^^^^

.. note::
   В CentOS 7 необходимо установить репозиториий ``epel``

Если не установлена утилита scsi-target-utils, установить::

  yum install scsi-target-utils

Запустить::

  /etc/init.d/tgtd start

Проверить уровни запуска::

  chkconfig tgtd --list

Если выключена, включить::

  chkconfig tgtd on

.. Отредактировать файл::
.. 
..   /etc/iscsi/initiatorname.iscsi
.. 
.. если не устраивает идентификатор устройства. С помощью отредактированного файла установить переменную окружения::
.. 
..   source /etc/iscsi/initiatorname.iscsi
..   echo $InitiatorName
.. 
.. .. note::
..   После перезагрузки $InitiatorName не сохраняется

Создать хранилище (target)::

  tgtadm --lld iscsi --op new --mode target --tid 1 -T [идентификатор хранилища]

.. note::
   [идентификатор хранилища] принято задавать в виде::
   
     iqn.[ГГГГ]-[ММ].[имя_домена_сети_в_обратном_порядке]:[имя_хранилища]

Посмотреть состояние::

  tgtadm --lld iscsi --op show --mode target

Добавить накопитель::

  tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b [устройство|файл]

Посмотреть состояние::

  tgtadm --lld iscsi --op show --mode target

.. Экспортировать устройство::
.. 
..   tgtadm --lld iscsi --op bind --mode target --tid 1 -I [адрес|ALL]
.. 
.. ALL экспортирует устройства (LUN) хранилища для любого клиента.
.. Для создания списка допустимых IP-адресов нужно повторить команду для всех допустимых IP-адресов.
.. 

Добавить в файл::

  /etc/tgt/targets.conf

описание target вида::

  <target [идентификатор хранилища]>
  backing-store [устройство\|файл]
  </target>

перезапустить службу::

  service tgtd restart

.. note::
  iptables: открыть порта 3260

Проверить ACL (последние строчки в листинге)::

  tgtadm --lld iscsi --op show --mode target
  
  Target 1: iqn.2016-04.local.lnp.n7710-g:iscsi.disk.raid0
    System information:
        Driver: iscsi
        State: ready
    LUN information:
        LUN: 0
            Type: controller
            SCSI ID: IET     00010000
            SCSI SN: beaf10
            Size: 0 MB, Block size: 1
            Online: Yes
            Removable media: No
            Prevent removal: No
            Readonly: No
            Backing store type: null
            Backing store path: None
            Backing store flags: 
        LUN: 1
            Type: disk
            SCSI ID: IET     00010001
            SCSI SN: beaf11
            Size: 4295 MB, Block size: 512
            Online: Yes
            Removable media: No
            Prevent removal: No
            Readonly: No
            Backing store type: rdwr
            Backing store path: /dev/sdb
            Backing store flags: 
    Account information:
    ACL information:
        ALL
  

