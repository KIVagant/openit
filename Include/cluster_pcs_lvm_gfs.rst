.. Кластер высокой доступности (HA-cluster) на базе Pacemaker
   ==========================================================

`Источник <http://jensd.be/?p=156>`__

Создание кластера
-----------------
Для кластера нужны:

- сервисы (webserver, mailserver, file-server,…)
- менеджер ресурсов (Pacemaker)
- система связи между узлами кластера (Corosync или Heartbeat)

Рассматрим построение кластера из следующих компонент:

- HTTP сервера Apache (сервиса), 
- Pacemaker (менеджер ресурсов), 
- Corosync (система связи между узлами). 

.. note::
   В ``CentOS 7`` ``Heartbeat`` не поддерживается.

- PCS как система управления кластером.

Рассматриваемый кластер не предназначен для промышленного использования. 
С его помощью будут изучаться вопросы построения простого HTTP сервера. 
HTTP сервер будет обрабатывать запросы поступающие на виртуальный адрес кластера. 
В зависимости от того, какой узел кластера подключен к виртуальному адресу будет выводиться та или другая статическая страничка HTML.

.. Optionally: file synchronization which will keep filesystems equal at all cluster nodes (with DRDB or GlusterFS)
.. Optionally: Cluster manager to easily manange the cluster settings on all nodes (like PCS)
.. The example is based on CentOS 7 but should work without modifications on basically all el6 and el7 platforms and with some minor modifications on other Linux distributions as well.

Подготовка
----------

Для построения кластера нужно провести следующие предварительные настройки:

- обеспечить ping по короткому имени всех узлов кластера;
- открыть UDP-ports 5404 и 5405 для Corosync;
- открыть TCP-port 2224 для утилиты ``pcs``;
- разрешить IGMP-traffic

::

  [root@nodeX ~]# iptables -I INPUT -p igmp -j ACCEPT

- разрешить multicast-traffic

::

  [root@nodeX ~]# iptables -I INPUT -m addrtype --dst-type MULTICAST -j ACCEPT

.. When testing the cluster, you could temporarily disable the firewall to be sure that blocked ports aren’t causing unexpected problems.

Установка
---------

::

  [root@nodeX ~]# yum install corosync pcs pacemaker cman

.. note::
   В ``CentOS 7`` пакета ``cman`` нет.

.. To manage the cluster nodes, we will use PCS. This allows us to have a single interface to manage all cluster nodes. 
.. By installing the necessary packages, Yum also created a user, hacluster, 
.. which can be used together with PCS to do the configuration of the cluster nodes. 

Для управления кластером будет использоваться утилита ``pcs``. 
При установке ``pcs`` создается пользователь ``hacluster``.
Для использования ``pcs`` нужно:

- задать пароль пользователю ``hacluster`` (создается при установке ``pacemaker``):

::

  [root@nodeX ~]# passwd hacluster
  Changing password for user hacluster.
  New password:
  Retype new password:
  passwd: all authentication tokens updated successfully.

- проверить наличие каталога ``/etc/cluster``, если отсутствует - создать и установить контекст ``SELinux``:

::

  root@nodeX ~]# mkdir /etc/cluster
  root@nodeX ~]# restorecon -R -v /etc/cluster

- запустить сервис

**CentOS 7**

::

  [root@nodeX ~]# systemctl start pcsd

**CentOS 6**

::

  [root@nodeX ~]# service pcsd start

- авторизоваться с помощью пользователя ``hacluster``

::

  [root@node1 ~]# pcs cluster auth node1 node2
  Username: hacluster
  Password:
  node1: Authorized
  node2: Authorized

.. note::
   Операция выполняется "не быстро".

После авторизации можно управлять кластером с одного из узлов.

Создание кластера
-----------------

::

  [root@node1 ~]# pcs cluster setup --name <имя_кластера> node1 node2
  Shutting down pacemaker/corosync services...
  Redirecting to /bin/systemctl stop  pacemaker.service
  Redirecting to /bin/systemctl stop  corosync.service
  Killing any remaining services...
  Removing all cluster configuration files...
  node1: Succeeded
  node2: Succeeded

Данная команда создает конфигурационный файл ``/etc/corosync.conf`` для кластера с именем ``<имя_кластера>``::

  totem {
  version: 2
  secauth: off
  cluster_name: <имя_кластера>
  transport: udpu
  }

  nodelist {
    node {
          ring0_addr: node1
          nodeid: 1
         }
    node {
          ring0_addr: node2
          nodeid: 2
         }
  }

  quorum {
  provider: corosync_votequorum
  two_node: 1
  }

  logging {
  to_syslog: yes
  }

.. note::
   Если выводится сообщение об ошибке ``Error connection to node<X> (HTTP error: 500)``, то создать на обеих узлах каталог ``/etc/cluster``.

.. The syntax in that file is quite readable in case you would like to automate/script this.

Запуск кластера
---------------

Настройка автоматического включения кластера при загрузке::

  pcs cluster enable --all
  node1: Cluster Enabled
  node2: Cluster Enabled

.. After creating the cluster and adding nodes to it, we can start it. 
.. The cluster won’t do a lot yet since we didn’t configure any resources.

Если это не сделать нужно будет включать кластер на узлах вручную после каждой перезагрузки::

  [root@node1 ~]# pcs cluster start --all
  node2: Starting Cluster...
  node1: Starting Cluster...

.. note::
   Длительный процесс, около минуты.

.. You could also start the pacemaker and corosync services on both nodes (as will happen at boot time) to accomplish this.

Проверка состояния кластера::

  [root@node1 ~]# pcs status cluster
  Cluster Status:
   Last updated: Sun Sep 20 12:03:07 2015
   Last change: Sun Sep 20 12:03:04 2015
   Current DC: NONE
   2 Nodes configured
   0 Resources configured

Проверка состояния узлов::

  [root@node1 ~]# pcs status nodes
  Pacemaker Nodes:
  Online: node1 node2
  Standby:
  Offline:
 
Проверка состояния синхронизации узлов кластера:: 

  [root@node1 ~]# corosync-cmapctl | grep members
  runtime.totem.pg.mrp.srp.members.1.config_version (u64) = 0
  runtime.totem.pg.mrp.srp.members.1.ip (str) = r(0) ip(192.168.202.101)
  runtime.totem.pg.mrp.srp.members.1.join_count (u32) = 1
  runtime.totem.pg.mrp.srp.members.1.status (str) = joined
  runtime.totem.pg.mrp.srp.members.2.config_version (u64) = 0
  runtime.totem.pg.mrp.srp.members.2.ip (str) = r(0) ip(192.168.202.102)
  runtime.totem.pg.mrp.srp.members.2.join_count (u32) = 1
  runtime.totem.pg.mrp.srp.members.2.status (str) = joined
  
  [root@node1 ~]# pcs status corosync

  Membership information
  ----------------------
      Nodeid      Votes Name
           1          1 node1 (local)
           2          1 node2

Настройка параметров кластера
-----------------------------

Определение основных параметров кластера::

  [root@node1 ~]# pcs property
  Cluster Properties:
  cluster-infrastructure: corosync
  dc-version: 1.1.10-32.el7_0-368c726

Настройки кластера можно проверить с помощью специальной утилиты ``crm_verify``. Опция ``-L`` позволяет осуществить диагностики всех работающих узлов кластера (без этой опции можно проверять только локальные файлы настроек текущего узла)::

  [root@node1 ~]# crm_verify -L
  Errors found during check: config not valid
    -V may provide more details

В настройках кластера обнаружена ошибка, детализироать которую предлагается с помощью опции ``-V``::

  [root@node1 ~]# crm_verify -L -V
  error: unpack_resources: Resource start-up disabled since no STONITH resources have been defin
  error: unpack_resources: Either configure some or disable STONITH with the stonith-enabled opt
  error: unpack_resources: NOTE: Clusters with shared data need STONITH to ensure data integrity
  Errors found during check: config not valid

Данное сообщение об ошибке говорит о том, что не работае механизм ``STONITH`` (Shoot-The-Other-Node-In-The-Head).

.. The above message tells us that there still is an error regarding STONITH (Shoot The Other Node In The Head), 
.. which is a mechanism to ensure that you don’t end up with two nodes that both think they are active and claim to be the service and virtual IP owner, 
.. also called a split brain situation. 

**Механизм ``STONITH``**

Позволяет, если необходимо, выключать/включать/перезагружать узлы кластера. Обычно этот механизм реализуется с помощью специальных сетевых «пилотов» с удаленным управлением или через специальные платы управления сервером. 
В ``pacemaker`` устройства ``STONITH`` реализованы в виде кластерных ресурсов.

.. Иногда бывает необходимо физически выключить(обесточить) узел для предотвращения порчи разделяемых данных, либо для завершения каких-либо процедур восстановления. Для этого у pacemaker есть STONITHd.

Для простого кластера механизм ``STONITH`` можно отключить::

  [root@node1 ~]# pcs property set stonith-enabled=false

.. While configuring the behavior of the cluster, we can also configure the quorum settings. 
.. The quorum describes the minimum number of nodes in the cluster that need to be active in order for the cluster to be available. 
.. This can be handy in a situation where a lot of nodes provide simultaneous computing power. 
.. When the number of available nodes is too low, it’s better to stop the cluster rather than deliver a non-working service. By default, the quorum is considered too low if the total number of nodes is smaller than twice the number of active nodes. 
.. For a 2 node cluster that means that both nodes need to be available in order for the cluster to be available. In our case this would completely destroy the purpose of the cluster.

**Кворум**

Кворум задает минимальное число работающих узлов кластера, при котором кластер считается работоспособным.
По умолчанию, кворум считается недопустиммым, если число работающих узлов меньше половины от общего числа узлов.
В случае 2-х узлов анализ кворума не имеет смысла (кластер будет считаеться не работоспособным если хотябы один из узлов отключен).

Отключение кворума::

  [root@node1 ~]# pcs property set no-quorum-policy=ignore

Просмотреть установленные параметры можно так::

  [root@node1 ~]# pcs property
  ...
  no-quorum-policy: ignore
  stonith-enabled: false

Добавление тестового ресурса
----------------------------
::

  [root@node1 ~]# pcs resource create dummy_svc Dummy op monitor interval=120s


где, ``op monitor interval=120s`` при отказе ресурса обеспечивает вывод диагностического сообщения раз в 2 минуты и перенос ресурса на другой элемен кластера. 

**Эмуляция отказа тестового ресурса**
::

  [root@node1 ~]# crm_resource --resource my_first_svc --force-stop
	  

Управление кластером
--------------------

- определение состояния кластера

::

[root@nodeX ~]# pcs cluster status --all 

- запуск всех узлов кластера

::

  [root@nodeX ~]# pcs cluster start --all 

- остановка всех узлов кластера

::

  [root@nodeX ~]# pcs cluster stop --all 

- запуск текущего узла кластера

::

  [root@nodeX ~]# pcs cluster start 

- выключение узла

::

  [root@nodeX ~]# poweroff

Эксперимент по выключению одного из узлов
-----------------------------------------

Выключим узел ``node1`` :: 

  [root@node1 ~]# poweroff

Выполним на работающем узле ``node2``::

  [root@node2 ~]# pcs cluster status --all
  Cluster Status:
   Last updated: Sun Sep 20 13:25:37 2015
   Last change: Sun Sep 20 12:56:37 2015
   Stack: corosync
   Current DC: node2 (2) - partition with quorum
   Version: 1.1.12-a14efad
   2 Nodes configured
   0 Resources configured

  PCSD Status:

**Длительная пауза ...**

::

    node1: Offline
    node2: Online
  
Включим ``node1``, повторим предыдущую команду на узле, который не выключался (``node2``)::

  Cluster Status:
   Last updated: Sun Sep 20 13:32:17 2015
   Last change: Sun Sep 20 12:56:37 2015
   Stack: corosync
   Current DC: node2 (2) - partition with quorum
   Version: 1.1.12-a14efad
   2 Nodes configured
   0 Resources configured

  PCSD Status:
    node1: Online
    node2: Online

Выполним на включенном узле ``node1`` команду::

  [root@node1 ~]# pcs cluster status
  Error: cluster is not currently running on this node

Для восстановления работоспособности утилиты ``pcs`` на данном узле необходимо запустить кластер на этом узле::

  [root@nodeX ~]# pcs cluster start 
  Starting Cluster...

Тогда::

  [root@node1 ~]# pcs cluster status
  Cluster Status:
   Last updated: Sun Sep 20 13:37:21 2015
   Last change: Sun Sep 20 12:56:37 2015
   Stack: corosync
   Current DC: NONE
   2 Nodes configured
   0 Resources configured

  PCSD Status:
    node1: Online
    node2: Online

Виртуальный IP кластера
-----------------------

.. The next step is to actually let our cluster do something. 
.. We will add a virtual IP to our cluster. 
.. This virtual IP is the IP address that which will be contacted to reach the services (the webserver in our case). 
.. A virtual IP is a resource. 

Создание::

  [root@node1 ~]# pcs resource create virtual_ip ocf:heartbeat:IPaddr2 \
                  ip=<VIP> \
                  cidr_netmask=32 \
                  op monitor interval=30s
  [root@node1 ~]# pcs status resources
  virtual_ip (ocf::heartbeat:IPaddr2): Started

Здесь, ``<VIP>`` - виртуальный ``IP`` для узлов кластера или просто ``IP`` кластера

Проверка виртуального ``IP``::

  [root@node1 ~]$ ping -c1 <VIP>

.. note::
   В данный момент по ``VIP`` "пингуется" узел ``node1``.

Определение активного узла (узла, на который передаются пакеты с адресом равным адресу кластера)::

  [root@node1 ~]# pcs status|grep virtual_ip
  virtual_ip (ocf::heartbeat:IPaddr2): Started node1

Настройка HTTP сервера
----------------------

.. Once our virtual IP is up and running, we will install and configure the service which we want to make high-available on both nodes: Apache. 
.. To start, install Apache and configure a simple static webpage on both nodes that is different. 
.. This is just temporary to check the function of our cluster. 
.. Later the webpages on node 01 and node 02 should be synchronized in order to serve the same website regardless of which node is active.

Ниже растраивается простой HTTP сервер обрабатывающий запросы поступающие на виртуальный адрес кластера.
В зависимости от того, какой узел кластера подключен к виртуальному адресу кластера выводистя та или другая статическая страничка HTML.

.. Позже веб-страницы на узле 01 и узла 02 должны быть синхронизированы, чтобы служить тот же сайт, независимо от того, какой узел активен.

Установка::

  [root@nodeX ~]# yum install httpd
  ...
  Complete!

Открытие HTTP трафика::

  [root@nodeX ~]# iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
  [root@nodeX ~]# service iptables save
  iptables: Saving firewall rules to /etc/sysconfig/iptables:[ OK ]

.. In order for the cluster to check if Apache is still active and responding on the active node, we need to create a small test mechanism. For that, we will add a status-page that will be regularly queried. 
.. The page won’t be available to the outside in order to avoid getting the status of the wrong node.

На каждом узле кластера создается файла настройки::

  [root@nodeX ~]# vi /etc/httpd/conf.d/serverstatus.conf
  Listen 127.0.0.1:80
  <Location /server-status>
    SetHandler server-status
    Order deny,allow
    Deny from all
    Allow from 127.0.0.1
  </Location>

который выдает состояние HTTP сервера при обрашении к нему с помощью адресной строки вида::

  http://127.0.0.1/server-status

HTTP сервер отключается от реальног адреса путем коментирования всех строк с опцией ``Listen`` в основном файле настройки::

  [root@nodeX ~]# sed -i 's/Listen/#Listen/' /etc/httpd/conf/httpd.conf

Перезапуск HTTP сервиса::

  [root@nodeX ~]# systemctl restart httpd

Проверка состояния узла::

  [root@nodeX ~]# wget http://127.0.0.1/server-status

На каждом узле содаются тестовае странички::

  [root@node1 ~]# vi /var/www/html/index.html
  <html>
    <h1>node1</h1>
  </html>
  
  [root@node2 ~]# vi /var/www/html/index.html
  <html>
    <h1>node2</h1>
  </html>

Подключение HTTP сервера к кластеру
-----------------------------------

.. Now we will stop the webserver on both nodes. From now on, the cluster is responsible for starting and stopping it. First we need to enable Apache to listen to the outside world again (remember, we disabled the Listen-statement in the default configuration). Since we want our website to be served on the virtual IP, we will configure Apache to listen on that IP address.

Остановить HTTP сервер:

**CentOS 7**

::

  [root@nodeX ~]# systemctl stop httpd

**CentOS 6**

::

  [root@nodeX ~]# service httpd stop

На каждом узле настроить HTTP сервер на виртуальный адрес кластера::

  [root@nodeX ~]# echo "Listen <VIP>:80"|sudo tee --append /etc/httpd/conf/httpd.conf

Создать кластерный ресурс::

  [root@node1 ~]# pcs resource create webserver ocf:heartbeat:apache \
                               configfile=/etc/httpd/conf/httpd.conf \
                               statusurl="http://localhost/server-status" \
                               op monitor interval=1min


.. By default, the cluster will try to balance the resources over the cluster. 
.. That means that the virtual IP, which is a resource, will be started on a different node than the webserver-resource. 
.. Starting the webserver on a node that isn’t the owner of the virtual IP will cause it to fail since we configured Apache to listen on the virtual IP. 

По умолчанию, кластерный ресурс HTTP сервера будет пытаться распределять нагрузку между узлами.
В нашем случае это приведет к ошибке, поскольку HTTP сервисы на узлах настроены на единственный виртуальный адрес кластера.

Поэтому::

  [root@node1 ~]# pcs constraint colocation add webserver virtual_ip INFINITY

.. To avoid the situation where the webserver would start before the virtual IP is started or owned by a certain node, 
.. we need to add another constraint which determines the order of availability of both resources::

Для того, чтобы HTTP сервис запускался после активации виртуального адреса кластера::

  [root@node1 ~]# pcs constraint order virtual_ip then webserver
  Adding virtual_ip webserver (kind: Mandatory) (Options: first-action=start then-action=start)

.. When both the cluster nodes are not equally powered machines and you would like the resources to be available on the most powerful machine, you can add another constraint for location::

Если узлы кластера не эквивалентны по производительности или доступности можно увеличить приоритет одного из узлов::

  [root@node1 ~]# pcs constraint location webserver prefers node1=50

Просмотр текущих настроек::

  [root@node1 ~]# pcs constraint
  Location Constraints:
  Resource: webserver
  Enabled on: node1 (score:50)
  Ordering Constraints:
  start virtual_ip then start webserver
  Colocation Constraints:
  webserver with virtual_ip

Перезапуск кластера::

  [root@node1 ~]# pcs cluster stop --all && sudo pcs cluster start --all
  node2: Stopping Cluster...
  node01: Stopping Cluster...
  node02: Starting Cluster...
  node01: Starting Cluster...
  [root@node01 ~]# pcs status
  Cluster name: <имя_кластера>
  Last updated: Fri Aug 22 13:27:28 2014
  Last change: Fri Aug 22 13:25:17 2014 via cibadmin on node01
  Stack: corosync
  Current DC: node02 (2) - partition with quorum
  Version: 1.1.10-32.el7_0-368c726
  2 Nodes configured
  2 Resources configured
  Online: [ node01 node02 ]
  Full list of resources:
  virtual_ip (ocf::heartbeat:IPaddr2): Started node01
  webserver (ocf::heartbeat:apache): Started node01
  PCSD Status:
  node01: Online
  node02: Online
  Daemon Status:
  corosync: active/disabled
  pacemaker: active/disabled
  pcsd: active/disabled

.. note::
   Из приведенного листинга следует, что в данный момент активен узел ``node1``. 

Для проверки остановим узел ``node1`` и посмотрим состояние кластера::

  [root@node1 ~]# pcs cluster stop node1
  node1: Stopping Cluster...
  
  [root@node2 ~]# pcs status
  Cluster name: <имя_кластера>
  ...
  Online: [ node2 ]
  OFFLINE: [ node1 ]
  Full list of resources:
  virtual_ip (ocf::heartbeat:IPaddr2): Started node2
  webserver (ocf::heartbeat:apache): Started node2

.. note::
   Из приведенного листинга следует, что при отключении узла ``node1`` управление ``HTTP``сервисом переключается на ``node2``.

Для того, чтобы сервисы кластера автоматически запускались при перезагрузке нужно выполнить следующие команды на всех узлах кластера::

  [root@nodeX ~]# systemctl enable pcsd
  [root@nodeX ~]# systemctl enable corosync
  [root@nodeX ~]# systemctl enable pacemaker

Настройка GFS2
--------------

`Источник <http://www.tokiwinter.com/configuring-gfs2-on-centos-7/>`__

.. This article will briefly discuss how to configure a GFS2 shared filesystem across two nodes on CentOS 7. Rather than rehashing a lot of previous content, this article presumes that you have followed the steps in my previous article, in order to configure the initial cluster and storage, up to and including the configuration of the STONITH device – but no further. All other topology considerations, device paths/layouts, etc. are the same, and the cluster nodes are still node1 and node2. The cluster name is webcluster and the 8GB LUN is presented as /dev/disk/by-id/wwn-0x60014055f0cfae3d6254576932ddc1f7 upon which a single partition has been created: /dev/disk/by-id/wwn-0x60014055f0cfae3d6254576932ddc1f7-part1.

Установка необходимого ПО на обеих элементах кластера::

  [root@nodeX ~]# yum -y install gfs2-utils dlm
  [root@nodeX ~]# yum -y install lvm2-cluster

Включение кластерных блокировок LVM::

  [root@nodeX ~]# lvmconf --enable-cluster

Создается резервная копия ``/etc/lvm/lvm.conf`` файла ``/etc/lvm/lvm.lvmconfold``. 
Разница между исходным и новым файлом:

================ ====
locking_type = 3 locking_type = 1
use_lvmetad = 0  use_lvmetad = 1
================ ====

Перезагрузка::

  [root@nodeX ~]# systemctl reboot

Создание на кластере ресурса DLM::

  [root@node1 ~]# pcs resource create dlm ocf:pacemaker:controld \
      op monitor interval=30s on-fail=fence clone interleave=true ordered=true

.. note::
   Удалить ресурс можно командой::
     
     pcs resource delete dlm

Создание на кластере ресурса CLVMD::

  [root@node1 ~]# pcs resource create clvmd ocf:heartbeat:clvm \
      op monitor interval=30s on-fail=fence clone interleave=true ordered=true

.. note::
   Команда выполняется на одном из узлов

Настройка порядка запуска сервисов (``DLM`` сервис должен запускаться раньше ``CLVMD`` сервиса)::

  [root@node1 ~]# pcs constraint order start dlm-clone then clvmd-clone

Настройка ограничения обеспечивающего запуск сервиса ``CLVMD`` только вместе с сервисом ``DLM`` (по отдельности запускаться не будут)::  

  [root@node1 ~]# pcs constraint colocation add clvmd-clone with dlm-clone

Проверка состояния::

  [root@node1 ~]# pcs status resources
  Clone Set: dlm-clone [dlm]
       Started: [ node1 node2 ]
  Clone Set: clvmd-clone [clvmd]
       Started: [ node1 node2 ]

.. Set the no-quorum-policy of the cluster to freeze so that that when quorum is lost, the remaining partition will do nothing until quorum is regained – GFS2 requires quorum to operate.

Настройка, обеспечивающая блокироку изменений даннах при нарушении условий кворума::

  [root@node1 ~]# pcs property set no-quorum-policy=freeze

**Создание ресурса LVM**

::

  [root@node1 ~]# pvcreate <блочное_устройство>
  [root@node1 ~]# vgcreate -Ay -cy <имя_VG> <блочное_устройство>
  [root@node1 ~]# lvcreate -L <размер>G -n <имя_LV> <имя_VG>

.. note::
   Выполняется на одном из узлов.

**Создание файловой системы GFS**

::

  [root@node1 ~]# mkfs.gfs2 -p lock_dlm -t webcluster:testfs -j 2 /dev/<имя_VG>/<имя_LV>

Здесь:

== ====
-p тип кластерной блокировки
-t имя кластера и имя файловой системы
-j количество журналов (определяется количеством узлов имеющих право изменять данные на файловой системе)
== ====

.. note::
   Для надежности обычно количество журналов делают больше количества узлов.

.. We will not use /etc/fstab to specify the mount, rather we’ll use a Pacemaker-controlled resource::

Для монтирования ``LV`` будет использоваться ``pcs``, а не команда ``mount``::

  [root@node1 ~]# pcs resource create gfs2_res Filesystem device="/dev/<имя_VG>/<имя_LV>" \
                      directory="/mnt" fstype="gfs2" options="noatime,nodiratime" \
                      op monitor interval=10s on-fail=fence clone interleave=true

.. This is configured as a clone resource so it will run on both nodes at the same time. Confirm that the mount has succeeded on both nodes::

Данная команда клонирует ресурс на все узлы кластера, что можно проверить командой::

  [root@node1 ~]# pcs resource show
   Clone Set: dlm-clone [dlm]
       Started: [ node1 node2 ]
   Clone Set: clvmd-clone [clvmd]
       Started: [ node1 node2 ]
   Clone Set: gfs2_res-clone [gfs2_res]
       Started: [ node1 node2 ]

Следующая команда показывает доступность ``LV`` на любом узле::

  [root@nodeX ~]# mount | grep gfs2
  /dev/mapper/<имя_VG>-<имя_LV> on /mnt type gfs2 (rw,noatime,nodiratime,seclabel)

.. note::
  Опции ``noatime``, ``nodiratime`` и ``seclabel`` повышают производительность.
   ``noatime``, ``nodiratime`` - отключают запись времени доступа для файлов и каталогов.
   ``seclabel`` - отключает запись меток SELinux.

.. Next, create an ordering constraint so that the filesystem resource is started after the CLVMD resource, and a colocation constraint so that both start on the same node::

Далее, задается последовательность запуска сервиса (``gfs2`` запускается только после сервиса ``clvmd``):: 

  [root@node1 ~]# pcs constraint order start clvmd-clone then gfs2_res-clone
  Adding clvmd-clone gfs2_res-clone (kind: Mandatory) (Options: first-action=start then-action=start)

Задается необходимось запуска обеих сервисов на каждом узле::

  [root@node1 ~]# pcs constraint colocation add gfs2_res-clone with clvmd-clone
  
Проверка настроек::

  [root@node1 ~]# pcs constraint show
  Location Constraints:
  Ordering Constraints:
    start dlm-clone then start clvmd-clone
    start clvmd-clone then start gfs2_res-clone
  Colocation Constraints:
    clvmd-clone with dlm-clone
    gfs2_res-clone with clvmd-clone

Расширить файловую систему можно выполнив следующие каманды на любом из узлов:

- увеличить ``LV``

::

  [root@node1 ~]# lvextend -L+1G /dev/<имя_VG>/<имя_LV>
  Extending logical volume <имя_LV> to 2.00 GiB
  Logical volume <имя_LV> successfully resized
  
- расширить файловую систему

::

  [root@node1 ~]# gfs2_grow /dev/<имя_VG>/<имя_LV>
  FS: Mount point:          /mnt
  FS: Device:               /dev/mapper/<имя_VG>-<имя_LV>
  FS: Size:                 262142 (0x3fffe)
  FS: Resource group size:  65517 (0xffed)
  DEV: Length:               524288 (0x80000)
  The file system grew by 1024MB.
  gfs2_grow complete.

Определить размер::

  df -hT /mnt
  Filesystem                  Type  Size  Used Avail Use% Mounted on
  /dev/mapper/<имя_VG>-<имя_LV> gfs2  2.0G  259M  1.8G  13% /mnt
