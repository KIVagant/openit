.. Система загрузки UEFI
   =====================

В 1998 году фирма Intel приступила к разработке новой системы загрузки EFI для процессоров Itanium. 
В 2005 году корпорация Intel передала свою разработку специально созданному консорциуму UEFI Forum, главными участниками которого стали: 
Intel, 
AMD, 
Apple, 
IBM, 
Microsoft 
и ряд других. 
UEFI представляет собой специализированную операционную систему, предназначенную для загрузки других операционных систем. 
Физическое расположение UEFI может быть разнообразным - от микросхемы памяти или раздела на жестком диске до внешнего сетевого хранилища. 
В соответствии со спецификацией UEFI поддерживает файловую систему FAT32. 
С помощью UEFI процесс загрузки выглядит так. 
С раздела GPT с идентификатором EF00 и файловой системой FAT32, загружается в память файл ``\efi\boot\boot<архитектура>.efi`` (например, ``\efi\boot\bootx64.efi``), который должен выполнять все действия необходимые для загрузки ОС.

.. EFI
   gdisk -l <device
   gdisk
   ef000 - EFI File system
   Создать GPT диск 
   создать EFI раздел (>250M)
   отформатировать mkmsdosfs
   скопировать EFI каталог, например, с Fedora
   Оборудование должно поддерживать EFI 

