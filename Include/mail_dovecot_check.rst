.. Проверка работоспособности сервера
   ==================================

Проверка работоспособности POP сервера
--------------------------------------
Проверить работоспособность POP3 сервера можно командой::

  # telnet lsrv.example.com pop3
  Trying _IP_адрес_ ...
  Connected to lsrv.example.com (_IP_адрес_).
  Escape character is '^]'.
  +OK dovecot ready.

Вводим имя пользователя::

  user boss

Ответ сервера::

  +OK

Вводим пароль пользователя::

  pass 123

Ответ сервера::

  +OK Logged in.

Вводим команду просмотра доступных писем::

  list

Ответ сервера::

  +OK 0 messages:
  .

Подаем команду выйти::

  quit

Ответ сервера::

  +OK Logging out.
  Connection closed by foreign host.

Проверка работоспособности IMAP сервера
---------------------------------------
Проверить работоспособность IMAP сервера можно командой::

  telnet lsrv.example.com imap

Ответ сервера::

  Trying _IP_адрес_ ...
  Connected to imap.example.com(_IP_адрес_).
  Escape character is '^]'.
   * OK Dovecot ready.

Вводим имя пользователя и пароль::

  a1 LOGIN <ПОЛЬЗОВАТЕЛЬ> <ПАРОЛЬ>

Ответ сервера::

  a1 OK Logged in.

Вводим команду просмотра доступных папок::

  a2 LIST == "*"

Ответ сервера::

  * LIST (\HasNoChildren) "." "INBOX"
  a2 OK List completed.

Вводим команду просмотра доступных писем::

  a3 EXAMINE INBOX

Ответ сервера::

  * FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
  * OK [PERMANENTFLAGS ()] Read-only mailbox.
  * 1 EXISTS
  * 1 RECENT
  * OK [UNSEEN 1] First unseen.
  * OK [UIDVALIDITY 1257842737] UIDs valid
  * OK [UIDNEXT 2] Predicted next UID
  a3 OK [READ-ONLY] Select completed.

Вводим команду просмотра первого письма::

  a4 FETCH 1 BODY[]

Ответ сервера::

  * 1 FETCH (BODY[] {405}
  Return-Path: sender@example.com
  Received: from client.example.com ([_IP_адрес_])
          by mx1.example.com with ESMTP
          id <20040120203404.CCCC18555.mx1.example.com@client.example.com>
          for <recipient@example.com>; Tue, 20 Jan 2004 22:34:24 +0200
  From: sender@example.com
  Subject: Test message
  To: recipient@example.com
  Message-Id: <20040120203404.CCCC18555.mx1.example.com@client.example.com>
  This is a test message.
  )
  a4 OK Fetch completed.

Подаем команду выхода::

  a5 LOGOUT

Ответ сервера::

  * BYE Logging out
  a5 OK Logout completed.
