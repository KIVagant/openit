.. GRUB
   ----

Загрузчик GRUB позволяет изменять параметры загрузки ОС с помощью вызова:

* командного интерпретатора загрузки (клавиша ``c``).
* встроенного редактора меню (клавиша ``e``);

При редактировании пункта меню можно изменять команды и опции загрузки ядра, при использовании командного интерпретатора необходимо вводить инструкции загрузки ОС заново.
Функциями ядра можно управлять изменяя параметры загрузки. В случае загрузчика GRUB параметры загрузки перечисляются через пробел сразу за именем файла ядра. Полный перечень параметров и их описание можно найти в документации на исходный код ядра. 
Ниже приведены только некоторые, наиболее часто используемые из них:

===================== ==========================================================================
Параметр              Назначение
===================== ==========================================================================
root=/dev/<раздел>    Раздел, монтируемый ядром в качестве корневой файловой системы.
ro                    Монтирование корневой файловой системы только для чтения.
init=<программа>      Программа, запускаемая ядром для инициализации операционной системы. 
                      По умолчанию используется /sbin/init.
S или single          Загрузка операционной системы в монопольном (однопользовательском) режиме.
целое число от 1 до 5 Загрузка операционной системы в заданном режиме.
quiet                 Отключить вывод диагностических сообщений при загрузке ядра
rhgb                  Включить графическую заставку во время инициализации операционной системы.
===================== ==========================================================================

Редактор меню
~~~~~~~~~~~~~
Редактор меню вызывается нажатием клавиши ``e`` в основном меню загрузки и позволяет редактировать содержимое выбранной секции загрузки. Секция для загрузки GNU/Linux выглядит следующим образом:
::

  root (hd0,0)
  kernel /boot/vmlinuz<XX-XXXX> root=/dev/sda1 quiet rhgb
  initrd /boot/initrd<XX-XXXX>.img
  boot

где: <XX-XXXX> -- конкретная версия ядра может меняться в процессе обновления системы. 

Каждая строка приведенного пункта меню представляет собой команду.
::

  Команда root (<устройство>,<раздел>)

Задает устройство и раздел, на котором по умолчанию должны находиться файлы, необходимые для загрузки ядра операционной системы. 
Устройство указывается с помощью двухбуквенного обозначения типа (в примере -- hd) и номера устройства (в примере -- 0).

Тип устройства может быть одним из следующих: 

| hd -- накопитель на жестких дисках
| fd -- накопитель на гибком диске;
| cd – привод лазерного диска.

После запятой указывается номер раздела на устройстве (в примере -- 0). Нумерация разделов начинается с 0.
::

  Команда kernel <имя_файла> <параметры_ядра>

Загружает ядро ОС из указанного файла (в примере -- ``/boot/vmlinuz<XX-XXXX>``). После имени файла ядра обычно указываются параметры загрузки ядра (в примере -- ``root=/dev/sda1 quiet rhgb``).
::

  Команда initrd <имя_файла>

Загружает из указанного файла (в примере -- ``/boot/initrd<XX-XXXX>.img``) образ виртуального диска с модулями ядра, необходимыми для монтирования корневой файловой системы.
::

  Команда boot

Передает управление загруженному ядру. Использование этой команды в меню загрузки необязательно, поскольку загрузчик GRUB передает управление ядру автоматически после выполнения последней команды меню.

Редактор меню поддерживает команды, соответствующие следующим клавишам:

* ``o`` -- добавить команду после текущей;
* ``O`` -- добавить команду перед текущей;
* ``d`` -- удалить текущую команду;
* ``e`` -- редактировать текущую команду до нажатия ``Enter`` (сохранить изменения) или ``Esc`` (отказ от изменений);
* ``Esc`` -- выход из редактора с отказом от всех изменений;
* ``b`` -- загрузка операционной системы (выполнение команд текущей секции);
* ``c`` -- переход в режим командной строки.

Командный интерпретатор
^^^^^^^^^^^^^^^^^^^^^^^
После нажатия горячей клавиши ``c`` загрузчик переходит в режим командного интерпретатора. Для загрузки ОС в этом режиме необходимо выполнить команды аналогичные описанным выше. Ввод каждой команды должен завершаться нажатием клавиши ``Enter``. При наборе команд поддерживается механизм автодополнения команд и имен файлов, аналогичный командному интерпретатору ``bash``. Загрузка системы начинается после выполнения команды ``boot``.

Конфигурационный файл загрузчика GRUB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Описанные выше действия по изменению параметров загрузки действуют только один раз. После перезагрузки компьютера параметры загрузки восстанавливаются из конфигурационного файла::

  /boot/grub/grub.conf

Просмотр данного файла в загруженной по умолчанию системе позволяет узнать с какими параметрами было загружено ядро и операционная система.
