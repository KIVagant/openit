.. Кластерная файловая система GFS2 на базе LVM
   ============================================

.. note::
   Не подходит для CentOS 7, т.к. там нет ``cman``

.. При грубом отключении одной узлы GFS становится недоступной, после включении узлы доступ восстанавливается.

.. При нормальной остановке узлы все работает штатно на оставшемся узле 

| node0 - target (сервер) ISCSI с двумя разделами: один под даные, другой кворумный
| node1 - initiator (клиент) 1
| node2 - initiator (клиент) 2
|

.. На сервере iSCSI
   ================

   Создать раздел LVM (8e )::

     [root@node0 ~]# fdisk /dev/<устройство>
     Command (m for help): p
     Disk /dev/<устройство>: NN.N GB, NNNNNNNNNNNNN bytes
     ...
     Device            Boot Start End   Blocks   Id System
    /dev/<устройство>      N     NNNNN NNNNNNNN 8e Linux LVM
  
Подготовка узлов кластера
=========================

.. note::
   Далее команды, выполняемые на обеих элементах кластера будут начинаться с::

    [root@nodeX ~]# 
   
   где, X - 1 или 2

Установить необходимое ПО::

  [root@nodeX ~]# LANG=C yum groupinstall "High Availability" "Resilient Storage"

Установить поддержку GFS2, необходимую для одновременного изменения файлов на общей файловой системе с нескольких элементов кластера::

  [root@nodeX ~]# yum install lvm2-cluster cman gfs2-utils


Подключить iSCSI устройство::

  [root@nodeX ~]# iscsiadm -m discovery -t sendtargets -p node0:3260
  [root@nodeX ~]# cat /proc/partitions
  major minor  #blocks  name
     8        0    8388608 sda
     8        1    1048576 sda1
     8        2    7339008 sda2
     8       16    8388608 sdb
     8       17    7340032 sdb1
    
Установить и настроить сервер синхранизации::

  [root@nodeX ~]# yum -y install ntp
  [root@nodeX ~]# service ntpd start
  [root@nodeX ~]# ntpq -p
  [root@nodeX ~]# chkconfig ntpd on
  
Обеспечить распознование узлов по имени.

.. Next, for lab purposes, disable iptables and ip6tables, and switch SELinux into permissive mode::
.. 
..   # service iptables stop
..   # chkconfig iptables off
..   # service ip6tables stop
..   # chkconfig ip6tables off
..   # vi /etc/selinux/config
..   # grep '^SELINUX=' /etc/selinux/config
..   SELINUX=permissive
..   # setenforce permissive

Устанавливить пароль для пользователя ``ricci``, от имени которого работает одноименный сервис, обеспечивающий синхронизацию настроек узлов кластера::

  [root@nodeX ~]# passwd ricci
  
  Changing password for user ricci.
  New password:
  Retype new password:
  passwd: all authentication tokens updated successfully.

Запустить сервис ``ricci``::

  [root@nodeX ~]# service ricci start

  Starting system message bus:                               [  OK  ]
  Starting oddjobd:                                          [  OK  ]
  generating SSL certificates...  done
  Generating NSS database...  done
  Starting ricci:                                            [  OK  ]

и ``rgmanage``::

  [root@nodeX ~]# service rgmanager start
  Starting Cluster Service Manager:                          [  OK  ]

Включить эти сервисы::

  [root@nodeX ~]# chkconfig ricci on
  [root@nodeX ~]# chkconfig rgmanager on

Далее, для синхронизации файла конфигурации ``/etc/cluster/cluster.conf`` можно использовать команду::

  ccs_sync node1 node2

поэтому команды можно выполнять на одном из узлов.

.. Since a 2 node cluster cannot form a quorum on behalf of 2 votes please make sure to have second line configured as in the aboveexample. Cluster with 3 or more nodes do not need this special configuration option.

Создать файл конфигурации кластера вручную ::

  vi /etc/cluster/cluster.conf
  <cluster name="<имя_кластера>" config_version="1">
	<fence_daemon/>
	<clusternodes>
		<clusternode name="node1" nodeid="1" votes="1"/>
		<clusternode name="node2" nodeid="2" votes="1"/>
	</clusternodes>
	<cman expected_votes="1" two_node="1"/>
	<fencedevices/>
	<rm>
		<failoverdomains/>
		<resources/>
	</rm>
  </cluster>

или с помощью команд::

  ccs --createcluster <имя_кластера>
  ccs --addnode node1 --votes=1 --nodeid=1
  ccs --addnode node2 --votes=1 --nodeid=2
  ccs --setcman two_node=1 expected_votes=1

Cинхронизировать файла конфигурации ``/etc/cluster/cluster.conf`` командой::

  ccs_sync node1 node2

При первом использовании нужно ввести пароли пользователя ricci::

  You have not authenticated to the ricci daemon on node2
  Password: 
  You have not authenticated to the ricci daemon on node1
  Password: 

Запустить сервис управления кластером на обеих узлах::

  [root@nodeX ~]# /etc/init.d/cman start

Включить сервис управления кластером на обеих узлах::

  [root@nodeX ~]# chkconfig cman on

 Проверить состояние узлов одной из следующих команд::

  [root@nodeX ~]# cman_tool nodes
  Node  Sts   Inc   Joined               Name
     1   M      8   2016-04-04 00:09:02  node1
     2   M      4   2016-04-04 00:08:13  node2

  [root@nodeX ~]# clustat 
  Cluster Status for <имя_кластера> @ Mon Apr  4 00:15:43 2016
  Member Status: Quorate

   Member Name                                                     ID   Status
   ------ ----                                                     ---- ------
   node1                                                                 1 Online
   node2                                                                 2 Online, Local

Запустить сервис управления кластерным LVM::

  [root@nodeX ~]# /etc/init.d/clvmd start
  [root@nodeX ~]# chkconfig clvmd on
  
Заменить стандартный тип блокировок LVM2 (``stand-alone``) на кластерный (``cluster-wide``)::

  [root@nodeX ~]# lvmconf --enable-cluster
  [root@nodeX ~]#  vi /etc/lvm/lvm.conf
  fallback_to_clustered_locking = 0
  fallback_to_local_locking = 0
  
Рестартовать сервис кластерного LVM::

  [root@nodeX ~]# /etc/init.d/clvmd restart

Создать VG с одного из узлов::
 
  pvcreate /dev/<устройство>
  vgcreate -Ay -cy <имя_vg> /dev/<устройство>

.. note::
   Ключи  ``-Ay -cy`` важны, без них после перезагрузки iscsi-сервера портится target.

.. При создании группы томов нужно дополнительно указывать ключ ``-c``::

     vgcreate -c n vg1 /dev/sdd1 /dev/sde1

.. При просмотре кластерные группы будут видны с флагом ``c``::

     # vgs
     VG            #PV #LV #SN Attr   VSize  VFree
     VolGroup00      1   2   0 wz--n- 19.88G    0
     vg1             1   1   0 wz--nc 46.00G 8.00M

С одного из узлов создать том::

  lvcreate -l 100%FREE -n <имя_lvm> <имя_vg>

Проверить VG::

  vgs

  VG #PV #LV #SN Attr VSize VFree
  <имя_vg> 1 1 0 wz--nc NN.NNg NN.NNg

.. note:: 
   Кластерные группы отмечаются флагом ``c``.

Проверить LV::

  lvs

  LV        VG       Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  <имя_lvm> <имя_vg> -wi-ao---- NN.NNg

Создать GFS2 с одного из узлов::

  mkfs.gfs2 -p lock_dlm -t <имя_кластера>:share -j 3 /dev/<имя_vg>/<имя_lvm>

.. ====

Монтирование GFS2

::

  [root@nodeX ~]# mkdir /mnt/gfs
  [root@nodeX ~]# vi /etc/fstab
  /dev/<имя_vg>-<имя_lvm> /mnt/gfs gfs2 defaults 0 0
  
  [root@nodeX ~]# /etc/inint.d/gfs2 start
  [root@nodeX ~]# chkconfig gfs2 on
  
Кворумный диск
==============

Получившийся кластер состоит из двух узлов. Чтобы каждый из узлов работал независимо (продолжал изменять данные на диске, вне зависимости от работоспособности другого) нужно добавить кворумный диск.

Создание кворумного диска с меткой <q_метка> на разделе <q_раздел> iscsi диска::

  [root@node1 ~]#  mkqdisk -c /dev/<q_раздел> -l <q_метка>
  mkqdisk v0.5.1
  Writing new quorum disk label <q_метка> to /dev/<q_раздел>.
  WARNING: About to destroy all data on /dev/mydata1; proceed [N/y] ? y
  Initializing status block for node 1...

Проверка::

  [root@node1 ~]# mkqdisk -L
  mkqdisk v0.5.1
  /dev/<q_метка>:
  Magic: xxxxxxxx
  Label: <q_метка>
  Created: 
  Host: node1

.. Этого нет в 6.3
.. 
.. Теперь необходимо вручную на одном из узлов убрать флаг, говорящий о том, что кластер в особой двухнодовой конфигурации (luci .. ставит этот флаг автоматом), и увеличить число голосов на один. Кроме того, необходимо увеличить версию конфигурационного файла (/etc/cluster/cluster.conf)::
.. 
..   cluster alias="cluster0" config_version="3" name="cluster0"
..   ...
..   cman expected_votes="3" two\_node="0"/
.. 
.. Даем команду узлам кластера обновить конфигурацию::
.. 
..   ccs_tool update /etc/cluster/cluster.conf
..   Config file updated from version 2 to 3
..   Update complete.

Изменим файл настроек кластера /etc/cluster/cluster.conf::

  <?xml version="1.0"?>

::

  <cman expected_votes="3"/>

.. note::
   Делает узлы независящими друг от друга.

::

  <cluster name="<имя_кластера>" config_version="2">
    <clusternodes>
    <clusternode name="node1" nodeid="1" />
    <clusternode name="node2" nodeid="2" />
  </clusternodes>

::

  <rm>
    <resources>
      <lvm lv_name="<имя_lv>" name="<имя_ресурса>" vg_name="<имя_gv>"/>
    </resources>
  </rm>

::

  <quorumd label="метка_кворумного_раздела"/>

::

  </cluster>

Перезапустим узлы: сначала одну, затем другую, затем еще раз первую.

Fence
=====

Изменим файл настроек кластера /etc/cluster/cluster.conf::

  <?xml version="1.0"?>
  <cluster name=""<имя_кластера>" config_version="3">
  <cman expected_votes="3"/>
  <clusternodes>
    <clusternode name="node1" nodeid="1">

::

      <fence>
        <method name="human">
          <device name="last_resort" ipaddr="node1">
          </device>
        </method>
      </fence>

::

    </clusternode>
    <clusternode name="node2" nodeid="2">

::

      <fence>
        <method name="human">
          <device name="last_resort" ipaddr="node2">
          </device>
        </method>
      </fence>

::

    </clusternode>

::

    <fencedevices>
      <fencedevice name="last_resort" agent="fence_manual">
      </fencedevice>
    </fencedevices>

::

  </clusternodes>

::

  <rm>
    <resources>
      <lvm lv_name="<имя_lv>" name="<имя_ресурса>" vg_name="<имя_gv>"/>
    </resources>
  </rm>
  <quorumd label="метка_кворумного_раздела"/>
  </cluster>


Управляющая машина
==================

Устанавливаем группу пакетов::

  yum groupinstall "High Availability Management"

Запускаем сервис с WEB интерфейсом::

  [root@node0 ~]# chkconfig luci on
  [root@node0 ~]# service luci start

Web-интерфейс доступен по адресу::

  https://<имя или IP управляющей машины>:8084

Для входа:

| Имя пользователя: root
| Пароль: пароль root

Для управления элементами кластера с помощью ``luci`` на узлах должен быть:

- запущен сервис ``ricci``;
- установлен пароль для пользователя ``ricci``.

Управляющая машина может не входить в кластер, на ней можно не запускать ``cman`` и ``ricci``.
