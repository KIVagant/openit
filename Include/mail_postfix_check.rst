.. Проверка работоспособности сервера
   ==================================

Проверить работоспособность сервера можно с помощью команды ``telnet``
В Red Hat совместимых дистрибутивах утилита ``telnet`` не установливается по умолчанию, 
поэтому ее сначала нужно установить::

     yum install telnet

Диалог для отправки тестового письма выглядит примерно так::

  telnet lsrv.example.com smtp

Ответ сервера::

  Trying _IP_адрес_ ...
  Connected to lsrv.example.com (_IP_адрес_).
  Escape character is '^]'.
  220 lsrv.example.com ESMTP Postfix

Вводим имя компьютера с которого отправляется почта::

  ehlo lsrv.example.com

Ответ сервера::

  250-lsrv.example.com
  250-PIPELINING
  250-SIZE 10240000
  250-VRFY
  250-ETRN
  250 8BITMIME

Вводим адрес отправителя::

  mail from: master@example.com

Ответ сервера::

  250 Ok

Вводим адрес получателя::

  rcpt to: boss@example.com

Ответ сервера::

  250 Ok

Для перехода в режим ввода текста письма подаем команду::

  data

Ответ сервера::

  354 End data with <CR><LF>.<CR><LF>

Вводим текст письма::

  Почтальону Печкину

Для выхода из режима ввода текста письма нужно набрать точку в пустой строке и нажать <Enter>::

  .

Получаем сообщение о том, что письмо помещено в очередь для отправки::

  250 Ok: queued as ИДЕНТИФИКАТОР_ВСООБЩЕНИЯ_В_ОЧЕРЕДИ

Подаем команду выйти::

  quit

Ответ на наш ввод::

  221 Bye
  Connection closed by foreign host.

Проверка защищенного протокола SMTPS
------------------------------------

По умолчанию стандартный TCP порт для поддержки незащищенного SMTP-протокола - 25, 
для защищенного SMTPS-протокола - 465. 
Проверить 465 порт можно с помощью той же команды ``telnet``

::

  telnet lsrv.example.com 465
  Trying _IP_адрес_ ...
  Connected to lsrv.example.com.
  Escape character is '^]'.
  220 lsrv.example.com ESMTP Exim 4.80.1 Tue, 22 Jan 2013 22:28:14 +0100

Проверка TLS шифрования
-----------------------

::

  telnet lsrv.example.com 25
  Trying _IP_адрес_ ...
  Connected to lsrv.example.com.
  Escape character is '^]'.
  220 lsrv.example.com ESMTP Exim 4.80.1 Tue, 22 Jan 2013 22:39:55 +0100

  EHLO your-server.local.lan

  250-smtp.domain.dom Hello your-server.local.lan [ip-address]
  250-SIZE 52428800
  250-8BITMIME
  250-PIPELINING
  250-AUTH PLAIN LOGIN CRAM-MD5

  250-STARTTLS <<<  STARTTLS поддерживается

  250 HELP

  quit

  221 smtp.domain.dom closing connection
  Connection closed by foreign host.

Определение поддерживаемых методов аутентификации
-------------------------------------------------

::

  telnet lsrv.example.com 25
  Trying _IP_адрес_ ...
  Connected to lsrv.example.com.
  Escape character is '^]'.
  220 lsrv.example.com ESMTP Exim 4.80.1 Tue, 22 Jan 2013 22:39:55 +0100

  EHLO lsrv.example.com

  250-lsrv.example.com Hello lsrv.example.com [ip-address]
  250-SIZE 52428800
  250-8BITMIME
  250-PIPELINING

Далее идет перечень поддерживаемых методов аутентификации::

  250-AUTH PLAIN LOGIN CRAM-MD5
  250-STARTTLS
  250 HELP

::

  quit

  221 lsrv.example.com closing connection
  Connection closed by foreign host.
